#include "common.h"
#include "codelets_d.h"
#include "test.h"
#include "auxiliary.h"
#include <libgen.h>

int first;
void 
print_parameters(test_struct parameters)
{
  char *_matrix = basename(parameters->matrix_file);
  int i=0;
  while(_matrix[i++] != '\0')
    ;
  char matrix[i-4];
  memcpy(matrix, _matrix, (i-5) * sizeof(*matrix));
  matrix[i-5] = '\0';

  printf("------------------------------------------------------------------------------------------------------------\n");
  printf("------------------------------------------------------------------------------------------------------------\n");
  printf("matrix_file:\t\t%s \n", parameters->matrix_file);
  printf("matrix:\t\t\t%s \n", matrix);
  printf("max_iter:\t\t%d \n", parameters->max_iter);
  printf("nb_repeat:\t\t%d \n", parameters->nb_repeat);
  printf("n:\t\t\t%u \n", parameters->n);
  printf("nnz:\t\t\t%u \n", parameters->nnz);
  printf("use_reduction:\t\t%d \n", parameters->use_reduction);
  printf("use_pack:\t\t%d \n", parameters->use_pack);
  printf("explicit_prefetch:\t%d\n", parameters->explicit_prefetch);
  printf("permuting_instruction:\t%d\n", parameters->permuting_instruction);
  printf("two_dim:\t\t%d\n", parameters->two_dim);
  printf("------------------------------------------------------------------------------------------------------------\n");
  printf("------------------------------------------------------------------------------------------------------------\n\n");
}

void
print_stats(test_struct parameters, double timing, long nb_flops)
{
  printf("kernel %s: nchunks %u timing %f nb_flops %lu Gflops %f\n", 
	 parameters->kernel,
	 parameters->current_nchunks,
	 timing, 
	 nb_flops, 
	 (double) (nb_flops / 1000) / timing);
}



void
fatal_error(const char *func, char *msg)
{
  fprintf(stderr,"Fatal error in %s(): %s\n", func, msg);
  abort();
}

void
check_relative_error_print(double error, test_struct parameters)
{
  fprintf(stdout,"kernel %s: Checking the relative error\n", parameters->kernel);
  
  if (error > EPS_MACHINE)
    fprintf(stdout,"kernel %s: Warrning the solution is  suspicious ! (%e) \n", parameters->kernel, error);
  else 
    fprintf(stdout,"kernel %s: The solution is  correct ! (%e) \n", parameters->kernel, error);
}



void 
create_blocRowPtr(unsigned *rowPtr, unsigned m, 
		  unsigned nnz, unsigned nchunks, 
		  double ratio_CPU_GPU,
		  unsigned *blockRowPtr)
{
  unsigned cpu_portion, gpu_portion, first_blockRow_indice, portion;
  unsigned i, j;

  blockRowPtr[0] = 0;
  /* if (first > 0) { */
  /*   struct starpu_perfmodel model = dspmv_task_model; */
  /*   struct starpu_perfmodel_history_list *list = model.state->per_arch[0]->list; */
  /*   double total_avg_speed_gpu= 0.0; */
  /*   unsigned nb = 0; */
  /*   double speed_gpu; */
  /*   while ( list != NULL ) */
  /*     { */
  /*   	struct starpu_perfmodel_history_entry *entry = list->entry; */
  /* 	if ( entry->mean > 0 ) { */
  /* 	  total_avg_speed_gpu += entry->size /entry->mean  ; */
  /* 	  nb++; */
  /* 	} */
  /*  	list = list->next; */
  /*     } */
  /*   speed_gpu = total_avg_speed_gpu / nb; */
    
  /*   list = model.state->per_arch[3]->list; */
  /*   double total_avg_speed_cpu= 0.0; */
  /*   double speed_cpu; */
  /*   nb = 0; */
  /*   while ( list != NULL ) */
  /*     { */
  /*   	struct starpu_perfmodel_history_entry *entry = list->entry; */
  /* 	if ( entry->mean > 0 ) { */
  /* 	  total_avg_speed_cpu +=  entry->size /entry->mean  ; */
  /* 	  nb++; */
  /* 	} */
  /* 	list = list->next; */
  /*     } */
  /*   speed_cpu = total_avg_speed_cpu / nb; */
    
  /*   /\* printf("SUM SPEED CPU  = (%f)   SUM SPEED GPU = (%f) SPEED CPU = (%f)   SPEED GPU = (%f)\n", *\/ */
  /*   /\* 	   total_avg_speed_cpu, total_avg_speed_gpu, speed_cpu, speed_gpu); *\/ */

  /* unsigned chunk, nb_cpu_parts=0, nb_gpu_parts=0; */
  /* unsigned n_workers = starpu_worker_get_count(); */
  
  /* for( chunk = 0; chunk < nchunks; chunk++) { */
  /*   int worker_type = starpu_worker_get_type(chunk % n_workers); */
  /*   switch(worker_type) { */
  /*   case STARPU_CPU_WORKER: */
  /*     /\* printf("found cpu\n"); *\/ */
  /*     nb_cpu_parts++; */
  /*     break; */
  /*   case STARPU_CUDA_WORKER: */
  /*     /\* printf("found gpu\n"); *\/ */
  /*     nb_gpu_parts++; */
  /*     break; */
  /*   default: */
  /*     fatal_error(__FUNCTION__, "unsuported worker type"); */
  /*     break; */
  /*   } */
  /* } */
  /* /\* speed_cpu= 1; *\/ */
  /* /\* speed_gpu= 10; *\/ */
  /* double ratio_CPU_GPU = speed_cpu / speed_gpu; */
  /* cpu_portion = ( ratio_CPU_GPU * nnz)  / (nb_cpu_parts * ratio_CPU_GPU + nb_gpu_parts); */
  /* gpu_portion = nnz  / (nb_cpu_parts * ratio_CPU_GPU + nb_gpu_parts);  */
  /* /\* printf("blockrowptr\n"); *\/ */
  /* for(i = 1, j = 0; i < nchunks; i++) */
  /*   { */
  /*     int worker_type = starpu_worker_get_type((i-1) % n_workers); */
  /*     switch(worker_type) { */
  /*     case STARPU_CPU_WORKER: */
  /* 	portion = cpu_portion; */
  /* 	/\* printf("cpu gets a portion of (%u):\t\t", portion); *\/ */
  /* 	break; */
  /*     case STARPU_CUDA_WORKER: */
  /* 	portion = gpu_portion; */
  /* 	/\* printf("gpu gets a portion of %u :\t\t", portion); *\/ */
  /* 	break; */
  /*     default: */
  /* 	fatal_error(__FUNCTION__, "unsuported worker type"); */
  /* 	break; */
  /*     } */
  /*     first_blockRow_indice = rowPtr[j]; */
  /*     while(rowPtr[j++] - first_blockRow_indice < portion) */
  /* 	; */
  /*     if( rowPtr[j] - first_blockRow_indice  - portion  <=   portion - ( rowPtr[j-1] - first_blockRow_indice ) ) */
  /* 	  blockRowPtr[i] = j; */
  /*     else */
  /* 	blockRowPtr[i] = --j; */
  /*     /\* printf("blockrowptr[%u]=%u\n",i, blockRowPtr[i]); *\/ */
  /*   } */
  /* blockRowPtr[nchunks] = m; */
  /* /\* printf("blockrowptr[%u]=%u\n",nchunks, blockRowPtr[nchunks]); *\/ */
  /* } else{ */
    /* printf("doing first run\n"); */
    portion = nnz / nchunks;
    first = 1;
    for(i = 1, j = 0; i < nchunks; i++)
    {
      first_blockRow_indice = rowPtr[j];
      while(rowPtr[j++] - first_blockRow_indice < portion)
	;
      if( rowPtr[j] - first_blockRow_indice  - portion  <=   portion - ( rowPtr[j-1] - first_blockRow_indice ) )
	  blockRowPtr[i] = j;
      else
	blockRowPtr[i] = --j;
    }
  blockRowPtr[nchunks] = m;

  /* } */
}

void 
print_worker_stats()
{
#ifdef PRINT_WORKER_STATS 
  struct starpu_profiling_worker_info worker_info;
  unsigned n_workers = starpu_worker_get_count(), worker;
  double total_time = 0.0, all_executing_time = 0.0, all_sleeping_time = 0.0;

  for( worker = 0; worker < n_workers; worker++) {
    starpu_profiling_worker_get_info(worker, &worker_info);
    double worker_total_time = starpu_timing_timespec_to_us (&worker_info.total_time);
    double worker_executing_time = starpu_timing_timespec_to_us (&worker_info.executing_time);
    double worker_sleeping_time = starpu_timing_timespec_to_us  (&worker_info.sleeping_time);
    printf("worker %u total time (%lf) exec time (%lf) sleep time (%lf)\n", worker, worker_total_time, worker_executing_time, worker_sleeping_time);
    total_time += worker_total_time;
    all_executing_time += worker_executing_time;
    all_sleeping_time += worker_sleeping_time;
    
  }
  printf("total time (%lf) exec time (%lf) sleep time (%lf)\n", total_time, all_executing_time, all_sleeping_time);
#endif
}
