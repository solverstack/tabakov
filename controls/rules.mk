C_SOURCES +=  $(addprefix ${topsrcdir}/controls/,\
						auxiliary.c\
						data_helpers.c\
						codelet_helper.c\
						cublas_helper.c\
						cusparse_helper.c\
						mmio.c\
						csr_load_matrix.c\
						sparsevect_interface.c\
						sparsevect_filters.c)