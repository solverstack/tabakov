#include "common.h"
#include "cusparse_helper.h"


#ifdef STARPU_USE_CUDA
static cusparseHandle_t handles[STARPU_NMAXWORKERS];
static cusparseMatDescr_t descrA;
#endif

static void init_cusparse_func(void *args __attribute__((unused)))
{ 
#ifdef STARPU_USE_CUDA
  cusparseStatus_t status;  
  status = cusparseCreate(&handles[starpu_worker_get_id()]); 
  if (status != CUSPARSE_STATUS_SUCCESS)
    fprintf(stderr,"CUSPARSE Library initialization failed\n");
  cusparseSetStream(handles[starpu_worker_get_id()], starpu_cuda_get_local_stream());

  status = cusparseCreateMatDescr(&descrA);
  if (status != CUSPARSE_STATUS_SUCCESS)
    fprintf(stderr,"Matrix descriptor initialization failed\n");
  cusparseSetMatType(descrA,CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatIndexBase(descrA,CUSPARSE_INDEX_BASE_ZERO);  
  cusparseSetPointerMode(handles[starpu_worker_get_id()], CUSPARSE_POINTER_MODE_DEVICE);
#endif
}

static void shutdown_cusparse_func(void *args __attribute__((unused)))
{
#ifdef STARPU_USE_CUDA
  cusparseDestroy(handles[starpu_worker_get_id()]);
#endif
}


void starpu_helper_cusparse_init(void)
{
  starpu_execute_on_each_worker(init_cusparse_func, NULL, STARPU_CUDA);
}


void starpu_helper_cusparse_shutdown(void)
{
  starpu_execute_on_each_worker(shutdown_cusparse_func, NULL, STARPU_CUDA);
#ifdef STARPU_USE_CUDA
  cusparseDestroyMatDescr(descrA);
#endif
}

cusparseHandle_t cusparse_get_handle()
{
#ifdef STARPU_USE_CUDA
  return handles[starpu_worker_get_id()];
#else 
  return NULL;
#endif
}

cusparseMatDescr_t cusparse_get_mat_descr(void)
{
#ifdef STARPU_USE_CUDA
  return descrA; 
#else 
  return NULL;
#endif
}
