#include "common.h"
#include "codelets_d.h"

void 
update_task(struct starpu_task *task, struct  codelet_options *options)
{
  if (options->execute_on_worker) {
    task->execute_on_a_specific_worker = 1;
    task->workerid = options->worker_id;
  } 

  if ( options->tag ) {
    task->use_tag = 1;
    task->tag_id = options->tag;
    if ( options->depnends_on )
      starpu_tag_declare_deps_array(options->tag, options->n_dependencies, options->depnends_on);
  }

  if (options->callback_func) {
    task->callback_func = options->callback_func;
    task->callback_arg  = options->callback_arg;
  }
}



