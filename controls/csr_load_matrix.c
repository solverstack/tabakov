/* 
*   Matrix Market I/O example program
*
*   Read a real (non-complex) sparse matrix from a Matrix Market (v. 2.0) file
*   and store it in CSR format
*   (See http://math.nist.gov/MatrixMarket for details.)
*
*   Usage:  a.out [filename] > output
*
*       
*   NOTES:
*
*   1) Matrix Market files are always 1-based, i.e. the index of the first
*      element of a matrix is (1,1), not (0,0) as in C.  ADJUST THESE
*      OFFSETS ACCORDINGLY offsets accordingly when reading and writing 
*      to files.
*
*   2) ANSI C requires one to use the "l" format modifier when reading
*      double precision floating point numbers in scanf() and
*      its variants.  For example, use "%lf", "%lg", or "%le"
*      when reading doubles, otherwise errors will occur.
*/

#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "data_helpers.h"
#include "mmio.h"

struct read_value{
  double val; 
  unsigned column; 
}; 


int 
compare_read_array(const void *a, const void *b)
{
  struct read_value *a1 = (struct read_value *) a; 
  struct read_value *b1 = (struct read_value *) b; 
  int c = (int) a1->column; 
  int d = (int) b1->column; 
  return  c - d; 
}

void
csr_load_sizes(char *file, unsigned  *m, unsigned *n, unsigned *nnz)
{
  MM_typecode matcode;
  FILE *f;
  int ret_code;

  if ((f = fopen(file, "r")) == NULL) {
    printf("Could not open file.\n");
    exit(1);
  }

  if (mm_read_banner(f, &matcode) != 0)
    {
      printf("Could not process Matrix Market banner.\n");
      exit(1);
    }

  if (!(mm_is_real(matcode) && mm_is_matrix(matcode) &&
        mm_is_sparse(matcode)))
    {
      printf("Sorry, this application does not support ");
      printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
      exit(1);
    }

  if ((ret_code = mm_read_mtx_crd_size(f, m, n, nnz)) !=0)
    exit(1);
  
  fclose(f);
}

void 
csr_load_matrix(char *file, double **val, unsigned **colInd, unsigned *NNZ, 
		unsigned **rowPtr, unsigned  *m, unsigned *n)
  
{
  MM_typecode matcode;
  FILE *f;
  unsigned *RowPtr, *ColInd; 
  int ret_code;
  unsigned M, N, i, nnz;
  double  *vals;
  unsigned symetric = 0;
 
  if ((f = fopen(file, "r")) == NULL) {
    printf("Could not open file.\n");
    exit(1);
  }
  
  if (mm_read_banner(f, &matcode) != 0)
    {
      printf("Could not process Matrix Market banner.\n");
      exit(1);
    }
  
  if (!(mm_is_real(matcode) && mm_is_matrix(matcode) && 
	mm_is_sparse(matcode)))
    {
      printf("Sorry, this application does not support ");
      printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
      exit(1);
    }
  
  if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nnz)) !=0)
    exit(1);

  unsigned old_nnz = nnz, n_diag_elem = 0; 
  if (mm_is_symmetric(matcode))
    {
      if (M == N)
        {
          symetric = 1;
          nnz+= nnz;
        }
      else
        {
          fprintf(stderr,"The matrix is declared symetric and isn't square. Not possible!!!\n");
          exit(-1);
        }
    }
#ifdef DEBUG_JA
  RowPtr = malloc((N+1) * sizeof(*RowPtr));
#else

  manage_data(0,
	      MALLOC_DATA ,&RowPtr, (size_t) (N+1)*sizeof(*RowPtr), 
	      END_OF_VAR_LIST);
#endif
  for( i = 0; i <= N; i++)
    RowPtr[i] = 0;
  unsigned line, column;  
  double val_elem; 
  long file_position = ftell(f); 

  for( i=0; i < old_nnz; i++) {
      fscanf(f, "%u %u %lg\n", &line, &column, &val_elem);
      RowPtr[line]++; 
      if(symetric) {
	if( line == column)
	  n_diag_elem++; 
	else 
	  RowPtr[column]++;
      }
  }
  
  for( i = 0; i < N; i++)
    RowPtr[i+1]+=RowPtr[i]; 

  nnz -= n_diag_elem; 

  fseek(f, file_position, SEEK_SET); 

  struct read_value *read_array = malloc (nnz * sizeof(*read_array)); 
  unsigned *indxLocalRow = calloc(N, sizeof(*indxLocalRow)); 

  for( i = 0; i < old_nnz; i++) {
      fscanf(f, "%u %u %lg\n", &line, &column, &val_elem);
      /* mtx files are stored in one-based index */
      line--; column--; 
      read_array[RowPtr[line] + indxLocalRow[line]].val = val_elem; 
      read_array[RowPtr[line] + indxLocalRow[line]].column = column; 
      indxLocalRow[line]++; 
      if (line != column && symetric) {
	read_array[RowPtr[column] + indxLocalRow[column]].val = val_elem; 
	read_array[RowPtr[column] + indxLocalRow[column]].column = line; 	
	indxLocalRow[column]++;
      }
  }
  
  for(i = 0; i < N; i++) {
    unsigned n_elem = RowPtr[i+1] - RowPtr[i]; 
    if (n_elem > 1)
      qsort((void *) &read_array[RowPtr[i]], n_elem, sizeof(*read_array), compare_read_array); 
  }
  
#ifdef DEBUG_JA
  ColInd = malloc(nnz * sizeof(*ColInd)); 
  vals = malloc(nnz*sizeof(*vals)); 
#else
  manage_data(0,
	      MALLOC_DATA ,&ColInd, (size_t) nnz*sizeof(*ColInd), 
	      MALLOC_DATA ,&vals, (size_t) nnz*sizeof(*vals), 
	      END_OF_VAR_LIST);
#endif
  for(i = 0; i < nnz; i++) {
    ColInd[i] = read_array[i].column; 
    vals[i] = read_array[i].val;
  }

  free(read_array);
  free(indxLocalRow);
  *n = N; 
  *m = M; 
  *NNZ = nnz; 
  *val = vals; 
  *rowPtr = RowPtr;
  *colInd = ColInd; 
  fclose(f);
}
