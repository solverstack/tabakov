#include "common.h"
#include "cublas_helper.h"
#include "enum.h"
#ifdef STARPU_USE_CUDA

static cublasHandle_t handles[STARPU_NMAXWORKERS];
static double *tmp_memory[STARPU_NMAXWORKERS];
#endif

static void init_cublas_func(void *args __attribute__((unused)))
{
#ifdef STARPU_USE_CUDA
  cublasStatus_t status;
  unsigned  worker = starpu_worker_get_id();
  unsigned  node = starpu_worker_get_memory_node(worker); 

  status = cublasCreate_v2(&handles[worker]);
  if (status != CUBLAS_STATUS_SUCCESS)
    fprintf(stderr,"CUBLAS Library initialization failed\n");

  cublasSetStream_v2(handles[worker], starpu_cuda_get_local_stream());
  tmp_memory[worker] = (double *) starpu_malloc_on_node(node, 3 * sizeof(double));
  double tmp[3];
  tmp[ZERO_VALUE_INDICE] = 0.0;
  tmp[ONE_VALUE_INDICE]  = 1.0; 
  tmp[TMP_VALUE_INDICE] = 0.0;

  cudaMemcpy(tmp_memory[worker], tmp, 3 * sizeof(double), cudaMemcpyHostToDevice);
  cudaStreamSynchronize(starpu_cuda_get_local_stream()); 

  cublasSetPointerMode_v2(handles[worker], CUBLAS_POINTER_MODE_DEVICE);
#endif
}

void cublas_helper_init(void)
{
  starpu_execute_on_each_worker(init_cublas_func, NULL, STARPU_CUDA);
}

cublasHandle_t cublas_get_handle()
{
#ifdef STARPU_USE_CUDA
  return handles[starpu_worker_get_id()];
#else 
  return NULL; 
#endif
}

double *cublas_get_zero_value()
{
#ifdef STARPU_USE_CUDA
  return &tmp_memory[starpu_worker_get_id()][ZERO_VALUE_INDICE];
#else 
  return NULL; 
#endif
}

double *cublas_get_one_value()
{
#ifdef STARPU_USE_CUDA
  return &tmp_memory[starpu_worker_get_id()][ONE_VALUE_INDICE];
#else 
  return NULL; 
#endif
}

double *cublas_get_tmp_value()
{
#ifdef STARPU_USE_CUDA
  return &tmp_memory[starpu_worker_get_id()][TMP_VALUE_INDICE];
#else 
  return NULL; 
#endif
}

static void shutdown_cublas_func(void *args __attribute__((unused)))
{
#ifdef STARPU_USE_CUDA
  cublasDestroy_v2(handles[starpu_worker_get_id()]);
  starpu_free_on_node(starpu_worker_get_memory_node(starpu_worker_get_id()),
		      (uintptr_t) tmp_memory[starpu_worker_get_id()],
		      2*sizeof(double));
#endif
}

void cublas_helper_shutdown(void)
{
  starpu_execute_on_each_worker(shutdown_cublas_func, NULL, STARPU_CUDA);
}
