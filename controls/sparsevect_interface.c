#include <assert.h>
#include "sparsevect_interface.h"
#include "my_blas.h"

static int sparsevect_copy_any_to_any(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, void *async_data);
static int sparsevect_can_copy(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, unsigned handling_node);
extern void dpack_vect(double *dst_vect, double *src_vect, unsigned *ind, unsigned n_sparse, cudaStream_t stream);
extern void dunpack_vect(double *dst_vect, double *src_vect, unsigned *ind, unsigned n_sparse, cudaStream_t stream);
extern void dpack_vect_cpu(double *dst_vect, double *src_vect, unsigned *ind, unsigned n);
extern void dunpack_vect_cpu(double *dst_vect, double *src_vect, unsigned *ind, unsigned n);

static const struct starpu_data_copy_methods sparsevect_copy_data_methods_s =
{
        .can_copy = sparsevect_can_copy,
	.any_to_any = sparsevect_copy_any_to_any,
};

static void register_sparsevect_handle(starpu_data_handle_t handle, unsigned home_node, void *data_interface);
static starpu_ssize_t allocate_sparsevect_buffer_on_node(void *data_interface_, unsigned dst_node);
static void *sparsevect_handle_to_pointer(starpu_data_handle_t data_handle, unsigned node);
static void free_sparsevect_buffer_on_node(void *data_interface, unsigned node);
static size_t sparsevect_interface_get_size(starpu_data_handle_t handle);
static uint32_t footprint_sparsevect_interface_crc32(starpu_data_handle_t handle);
static int sparsevect_compare(void *data_interface_a, void *data_interface_b);
static void display_sparsevect_interface(starpu_data_handle_t handle, FILE *f);

static struct starpu_data_interface_ops interface_sparsevect_ops =
{
	.register_data_handle = register_sparsevect_handle,
	.allocate_data_on_node = allocate_sparsevect_buffer_on_node,
	.handle_to_pointer = sparsevect_handle_to_pointer,
	.free_data_on_node = free_sparsevect_buffer_on_node,
	.copy_methods = &sparsevect_copy_data_methods_s,
	.get_size = sparsevect_interface_get_size,
	.footprint = footprint_sparsevect_interface_crc32,
	.compare = sparsevect_compare,
	.interface_size = sizeof(struct sparsevect_interface),
	.display = display_sparsevect_interface,
};

static void *sparsevect_handle_to_pointer(starpu_data_handle_t handle, unsigned node)
{
	STARPU_ASSERT(starpu_data_test_if_allocated_on_node(handle, node));

	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *)
		starpu_data_get_interface_on_node(handle, node);

	return (void*) sparsevect_interface->ptr;
}

static void register_sparsevect_handle(starpu_data_handle_t handle, unsigned home_node, void *data_interface)
{
	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *) data_interface;

	unsigned node;
	for (node = 0; node < STARPU_MAXNODES; node++)
	{
		struct sparsevect_interface *local_interface = (struct sparsevect_interface *)
			starpu_data_get_interface_on_node(handle, node);

		if (node == home_node)
		{
			local_interface->ptr = sparsevect_interface->ptr;
                        local_interface->dev_handle = sparsevect_interface->dev_handle;
                        local_interface->offset = sparsevect_interface->offset;
                        local_interface->send_buffers = sparsevect_interface->send_buffers;
                        local_interface->recive_buffers = sparsevect_interface->recive_buffers;
                        local_interface->send_indices = sparsevect_interface->send_indices;
                        local_interface->recive_indices = sparsevect_interface->recive_indices;
		}
		else
		{
			local_interface->ptr = 0;
                        local_interface->dev_handle = 0;
                        local_interface->offset = 0;
                        local_interface->send_buffers = NULL;
                        local_interface->recive_buffers = NULL;
                        local_interface->send_indices = NULL;
                        local_interface->recive_indices = NULL;
		}

		local_interface->nx            = sparsevect_interface->nx;
		local_interface->elemsize      = sparsevect_interface->elemsize;
		local_interface->father        = sparsevect_interface->father;
		local_interface->id            = 0;
		local_interface->memory_mutex  = NULL;
		local_interface->memory_state  = sparsevect_interface->memory_state;
		local_interface->need_packing  = sparsevect_interface->need_packing;
		local_interface->indices_sizes = sparsevect_interface->indices_sizes;
	}
}


/* declare a new data with the sparsevect interface */
void sparsevect_data_register(starpu_data_handle_t *handle, unsigned home_node,
				     uintptr_t ptr, uint32_t nx, size_t elemsize)
{
	struct sparsevect_interface sparsevect =
	{
		.ptr = ptr,
		.nx = nx,
		.elemsize = elemsize,
                .dev_handle = ptr,
                .offset = 0,
		.father = 1,
		.id = 0,
	};

	starpu_data_register(handle, home_node, &sparsevect, &interface_sparsevect_ops);
}

void sparsevect_ptr_register(starpu_data_handle_t handle, unsigned node, uintptr_t ptr,
				    uintptr_t dev_handle, size_t offset)
{
	struct sparsevect_interface *sparsevect_interface = starpu_data_get_interface_on_node(handle, node);
	starpu_data_ptr_register(handle, node);
	sparsevect_interface->ptr = ptr;
	sparsevect_interface->dev_handle = dev_handle;
	sparsevect_interface->offset = offset;
}

static uint32_t footprint_sparsevect_interface_crc32(starpu_data_handle_t handle)
{
	return starpu_hash_crc32c_be(sparsevect_get_nx(handle), 0);
}

static int sparsevect_compare(void *data_interface_a, void *data_interface_b)
{
	struct sparsevect_interface *sparsevect_a = (struct sparsevect_interface *) data_interface_a;
	struct sparsevect_interface *sparsevect_b = (struct sparsevect_interface *) data_interface_b;

	/* Two sparsevects are considered compatible if they have the same size */
	return ((sparsevect_a->nx == sparsevect_b->nx)
			&& (sparsevect_a->elemsize == sparsevect_b->elemsize));
}

static void display_sparsevect_interface(starpu_data_handle_t handle, FILE *f)
{
	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *)
		starpu_data_get_interface_on_node(handle, 0);

	fprintf(f, "%u\t", sparsevect_interface->nx);
}

static size_t sparsevect_interface_get_size(starpu_data_handle_t handle)
{
	size_t size;
	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *)
		starpu_data_get_interface_on_node(handle, 0);

	size = sparsevect_interface->nx*sparsevect_interface->elemsize;

	return size;
}

/* offer an access to the data parameters */
uint32_t sparsevect_get_nx(starpu_data_handle_t handle)
{
	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *)
		starpu_data_get_interface_on_node(handle, 0);

	return sparsevect_interface->nx;
}

uintptr_t sparsevect_get_local_ptr(starpu_data_handle_t handle)
{
	unsigned node;
	node = starpu_worker_get_memory_node(starpu_worker_get_id());
	STARPU_ASSERT(starpu_data_test_if_allocated_on_node(handle, node));
	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *)
		starpu_data_get_interface_on_node(handle, 0);
	return sparsevect_interface->ptr;
}

size_t sparsevect_get_elemsize(starpu_data_handle_t handle)
{
	struct sparsevect_interface *sparsevect_interface = (struct sparsevect_interface *)
		starpu_data_get_interface_on_node(handle, 0);
	return sparsevect_interface->elemsize;
}

/* returns the size of the allocated area */
static starpu_ssize_t allocate_sparsevect_buffer_on_node(void *data_interface_, unsigned dst_node)
{
        assert(0);
	return 0;
}

static void free_sparsevect_buffer_on_node(void *data_interface, unsigned node)
{
        assert(0);
}

static int sparsevect_can_copy(void *src_interface, unsigned src_node,
			       void *dst_interface, unsigned dst_node,
			       unsigned handling_node)
{
  struct sparsevect_interface *src_sparsevect = src_interface;

  if (src_sparsevect->father == 1) 
    return 1; 
#ifdef  SPEAKING_LIKE_GOSSIP_GIRL
  if ( src_sparsevect->memory_state[src_node] == SPARSEVECT_MEMORY_OWE 
       &&  handling_node == src_node) {
    printf("for chunk %u acceptabale: sending data from %u to %u with handling node %u and  %u  is ",
	   src_sparsevect->id, src_node, dst_node, handling_node, src_node);
    
    if ( src_sparsevect->memory_state[src_node]  == SPARSEVECT_MEMORY_OWE ) 
      printf("the owner\n");
    else 
      printf("not the owner\n");
  }
#endif    

  if ( src_sparsevect->memory_state[src_node] == SPARSEVECT_MEMORY_OWE  ){
    if ( src_sparsevect->need_packing[src_sparsevect->id][dst_node] ==  SPARSEVECT_PACK )
      return  handling_node == src_node;
    else
      return 1;
  } else
    return 0;

}

static int sparsevect_copy_any_to_any(void *src_interface, unsigned src_node,
				      void *dst_interface, unsigned dst_node,
				      void *async_data)
{
	struct sparsevect_interface *src_sparsevect = src_interface;
	struct sparsevect_interface *dst_sparsevect = dst_interface;
	uint32_t **need_packing = src_sparsevect->need_packing;
	uint32_t vect_id = src_sparsevect->id;
	int ret = 0, transfered_data; 

	if (src_sparsevect->father) {
	  ret = starpu_interface_copy(src_sparsevect->dev_handle, src_sparsevect->offset, src_node,
				      dst_sparsevect->dev_handle, dst_sparsevect->offset, dst_node,
				      src_sparsevect->nx*src_sparsevect->elemsize, async_data);
	  transfered_data = src_sparsevect->nx*src_sparsevect->elemsize;
	  return ret;
	}
	
	/* no transfer is needed */
	if ( need_packing[vect_id][dst_node] == SPARSEVECT_NONE) 
	  transfered_data = 0;

	if ( need_packing[vect_id][dst_node] == SPARSEVECT_FULL)   {
	  ret = starpu_interface_copy(src_sparsevect->dev_handle, src_sparsevect->offset, src_node,
				      dst_sparsevect->dev_handle, src_sparsevect->offset, dst_node,
				      src_sparsevect->nx*src_sparsevect->elemsize, async_data);
	  transfered_data = src_sparsevect->nx*src_sparsevect->elemsize;
	}

	if ( need_packing[vect_id][dst_node] == SPARSEVECT_CONTINUOUS)   {
	  size_t offset = src_sparsevect->offset + src_sparsevect->first_sparse_indice[dst_node] * src_sparsevect->elemsize;
	  size_t size   = src_sparsevect->indices_sizes[vect_id][dst_node] * src_sparsevect->elemsize;  

	  ret = starpu_interface_copy(src_sparsevect->dev_handle, offset, src_node,
                                      dst_sparsevect->dev_handle, offset, dst_node,
                                      size, async_data);
          transfered_data = size;
	}
	if ( need_packing[vect_id][dst_node] == SPARSEVECT_PACK)  {
	  uintptr_t send_buffer    = src_sparsevect->send_buffers[dst_node];
	  uintptr_t src_vector_ptr = src_sparsevect->ptr;
	  uintptr_t recive_buffer  = dst_sparsevect->recive_buffers[dst_node];
	  uint32_t nb_elem         = src_sparsevect->indices_sizes[vect_id][dst_node];
	  uint32_t *indices         = src_sparsevect->send_indices[vect_id][dst_node];
	  
	  if ( starpu_node_get_kind(src_node) == STARPU_CUDA_RAM
	       && starpu_node_get_kind(dst_node) == STARPU_CUDA_RAM )
	    dpack_vect((double *)send_buffer, (double *)src_vector_ptr, indices, nb_elem,
		       async_data ? starpu_cuda_get_peer_transfer_stream(src_node, dst_node) : NULL);

#ifdef  SPEAKING_LIKE_GOSSIP_GIRL
	  struct cudaPointerAttributes send_buff_attr, recive_buff_attr, ptr_attr, indices_attr;
	  unsigned my_node = starpu_worker_get_memory_node(starpu_worker_get_id());
	  cudaPointerGetAttributes(&send_buff_attr,   (void*)send_buffer);
	  cudaPointerGetAttributes(&recive_buff_attr, (void*)recive_buffer);
	  cudaPointerGetAttributes(&ptr_attr,         (void*)src_vector_ptr);
	  cudaPointerGetAttributes(&indices_attr,         (void*)indices);

	  printf("SENDING CHUNK %u FROM NODE %d TO NODE %d WITH HANDLING NODE %u:\n",
		 src_sparsevect->id, src_node, dst_node, my_node);
	  if(send_buff_attr.memoryType == cudaMemoryTypeDevice) {
	    printf("send_buffer (%p) is a device ptr on the %d GPU\n",
		   (void*)send_buffer,
		   send_buff_attr.device);
	  }else {
	    printf("send_buffer (%p)) is a host memory \n", (void*)send_buffer);
	  }
	  if(recive_buff_attr.memoryType == cudaMemoryTypeDevice) {
	    printf("recive_buffer (%p) is a device ptr on the %d GPU\n",
		   (void*)recive_buffer,
		   recive_buff_attr.device);
	  }else {
	    printf("recive_buffer (%p) is a host memory ptr!!\n", (void*)recive_buffer);
	  }
	  if(ptr_attr.memoryType == cudaMemoryTypeDevice) {
	    printf("src_vector_ptr (%p) is a device ptr on the %d GPU\n",
		   (void*)src_vector_ptr,
		   ptr_attr.device);
	  }else {
	    printf("src_vector_ptr (%p) is a host memory ptr!!\n", (void*)src_vector_ptr);
	  }
	  if(indices_attr.memoryType == cudaMemoryTypeDevice) {
	    printf("indices (%p) is a device ptr on the %d GPU\n",
		   (void*)indices,
		   indices_attr.device);
	  }else {
	    printf("indices (%p) is a host memory !\n", (void*)indices);
	  }
	  printf("\n");
#endif 

	  ret = starpu_interface_copy(send_buffer, 0, src_node,
				      recive_buffer, 0, dst_node,
				      nb_elem*src_sparsevect->elemsize, async_data);

	  transfered_data = nb_elem*src_sparsevect->elemsize;
	}

 	return ret;
}


void
update_memory_state(void *arg)
{
  struct sparsevect_interface *interface =
    (struct sparsevect_interface *) arg;
  pthread_mutex_t *memory_mutex = interface->memory_mutex;
  uint32_t *memory_state   = interface->memory_state;
  unsigned nb_nodes = starpu_memory_nodes_get_count(), node;
  unsigned my_node  = starpu_worker_get_memory_node(starpu_worker_get_id());
  uint32_t vect_id = interface->id;
  uint32_t **need_pack     = interface->need_packing;

  pthread_mutex_lock(memory_mutex);
  for(node = 0; node < nb_nodes; node++) {
    if(node == my_node) 
      memory_state[node] = SPARSEVECT_MEMORY_OWE;
    else
      memory_state[node] = SPARSEVECT_MEMORY_INVALID;

    if (need_pack[vect_id][node] == SPARSEVECT_PACK 
	&&  starpu_node_get_kind(my_node) == STARPU_CPU_RAM) {
      
      uintptr_t send_buffer    = interface->send_buffers[node];
      uintptr_t src_vector_ptr = interface->ptr;
      uint32_t nb_elem         = interface->indices_sizes[vect_id][node];
      uint32_t *indices        = interface->send_indices[vect_id][node];
      
      dpack_vect_cpu((double *)send_buffer, (double *)src_vector_ptr, indices, nb_elem);
    }
  }
  pthread_mutex_unlock(memory_mutex);
}

uintptr_t 
SPARSEVECT_GET_PTR(void *arg){
  struct sparsevect_interface *interface = (struct sparsevect_interface *) arg;
  pthread_mutex_t *memory_mutex = interface->memory_mutex;
  uintptr_t ptr = interface->ptr;
  uint32_t  id  = interface->id;
  uint32_t *memory_state   = interface->memory_state;
  uint32_t **need_pack     = interface->need_packing;
  uint32_t **indices_sizes = interface->indices_sizes;
  uint32_t ***indices      = interface->recive_indices;
  unsigned nb_nodes        = starpu_memory_nodes_get_count();
  unsigned  my_node        = starpu_worker_get_memory_node(starpu_worker_get_id());
  uint32_t src_node;
  double *packed_vect;

  if (need_pack[id][my_node] != SPARSEVECT_PACK) 
    return ptr;

  pthread_mutex_lock(memory_mutex);

  if(memory_state[my_node] == SPARSEVECT_MEMORY_VALID 
     || memory_state[my_node] == SPARSEVECT_MEMORY_OWE)  {
    pthread_mutex_unlock(memory_mutex);
    return ptr;
  }

  for(src_node = 0; src_node < nb_nodes; src_node++) 
    if(memory_state[src_node] == SPARSEVECT_MEMORY_OWE) 
      break;

  packed_vect      = (double *) interface->recive_buffers[my_node];

  /* if(memory_state[my_node] == SPARSEVECT_MEMORY_VALID */
  /*    || memory_state[my_node] == SPARSEVECT_MEMORY_OWE) { */
  /*   pthread_mutex_unlock(memory_mutex); */
  /*   return ptr; */
  /* } */
  
  if( starpu_node_get_kind(my_node) == STARPU_CUDA_RAM) {
    dunpack_vect((double *)ptr, packed_vect, indices[id][my_node],
    		 indices_sizes[id][my_node], starpu_cuda_get_local_stream());
#ifndef STARPU_TRUNK_VERSION
    cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif
  } else if(starpu_node_get_kind(my_node) == STARPU_CPU_RAM)
    dunpack_vect_cpu((double *)ptr, packed_vect, 
		     indices[id][my_node], indices_sizes[id][my_node]);
  else
    assert(0);

  memory_state[my_node] = SPARSEVECT_MEMORY_VALID;
  pthread_mutex_unlock(memory_mutex);
  return ptr;
}
