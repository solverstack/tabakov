#include "common.h"
#include "auxiliary.h"
#include "data_helpers.h" 
#include "sparsevect_interface.h"

#if defined  SPEAKING_LIKE_GOSSIP_GIRL || defined WRITE_SPARSEVECTS
static void
print_need_pack_line(uint32_t *line, uint32_t n)
{
  uint32_t i;
  for(i = 0; i < n; i++)
    {
      switch (line[i]) {
      case SPARSEVECT_FULL:
	fprintf(stdout,"FULL\t");
	break;
      case SPARSEVECT_PACK:
	fprintf(stdout,"PACK\t");
	break;
      case SPARSEVECT_NONE:
	fprintf(stdout,"NONE\t");
	break;
      case SPARSEVECT_CONTINUOUS:
	fprintf(stdout,"CONTI\t");
	break;
      }
    }
  fprintf(stdout,"\n");
}
#endif

static void
vector_register_on_all_workers(starpu_data_handle_t *handle_ref, 
			       void  ***vects_ref,
			       unsigned n,
			       size_t size);

static void
sparsevect_register_on_all_workers(starpu_data_handle_t *handle_ref, 
				   void ***vects_ref,
				   unsigned n,
				   size_t size);

static void
free_on_all_nodes(void **data, unsigned n);

void
csr_col_index_filter(void *father_interface,
                     void *child_interface,
                     struct starpu_data_filter *f,
                     unsigned id,
                     unsigned nchunks);


void
manage_data(int unused, ...)
{
  va_list ap; 
  va_start(ap, unused);
  int type = va_arg(ap, int);
  while ( type != END_OF_VAR_LIST )  
    {
      void **data_ref;
      void *data_cpy;
      void *vect;
      void **vects_cpy, ***vects_ref;
      size_t size;
      int async;
      unsigned n, nnz, nchunks, chunk, node;
      unsigned **distribution, *colInd, *rowPtr, *blockRowPtr;
      starpu_data_handle_t *handle_ref;
      starpu_data_handle_t  handle_copy;
      struct csr_2D_struct *matrix_2D;
      struct starpu_data_filter filter = { };
      
      switch (type) {
	
      case MALLOC_DATA :
	data_ref   = va_arg(ap, void**);
	size       = va_arg(ap, size_t);
	starpu_malloc((void **)data_ref, size); 
	break;

      case VECTOR_REGISTER:
	handle_ref  = va_arg(ap, starpu_data_handle_t*);
	node         = va_arg(ap, unsigned);
	vect         = va_arg(ap, void*);
	n            = va_arg(ap, unsigned);
	size         = va_arg(ap, size_t);
	starpu_vector_data_register(handle_ref, node, (uintptr_t)vect, n, size);
	break;

      case VECTOR_REGISTER_ON_ALL:
	handle_ref   = va_arg(ap, starpu_data_handle_t*);
	vects_ref    = va_arg(ap, void ***);
	n            = va_arg(ap, unsigned);
	size         = va_arg(ap, size_t);
	vector_register_on_all_workers(handle_ref, vects_ref,  n, size);
	break;

      case SPARSEVECT_REGISTER_ON_ALL:
	handle_ref   = va_arg(ap, starpu_data_handle_t*);
	vects_ref    = va_arg(ap, void ***);
	n            = va_arg(ap, unsigned);
	size         = va_arg(ap, size_t);
	sparsevect_register_on_all_workers(handle_ref, vects_ref,  n, size);
	break;

      case CSR_REGISTER:
	handle_ref  = va_arg(ap, starpu_data_handle_t*);
	node         = va_arg(ap, unsigned);
	n            = va_arg(ap, unsigned);
	nnz          = va_arg(ap, unsigned);
	vect         = va_arg(ap, void*);
	colInd       = va_arg(ap, unsigned*);
	rowPtr       = va_arg(ap, unsigned*);
	size         = va_arg(ap, size_t);
	starpu_csr_data_register(handle_ref, node, nnz, n, (uintptr_t)vect, colInd, rowPtr, rowPtr[0], size);
	break;

      case VECTOR_BLOCK_PARTITION:
	handle_copy        = va_arg(ap, starpu_data_handle_t);
	nchunks            = va_arg(ap, unsigned);
	filter.filter_func = starpu_vector_filter_block;
	filter.nchildren   = nchunks;
	starpu_data_partition(handle_copy, &filter);
	break;

      case VECTOR_INDEX_PARTITION:
	handle_copy           = va_arg(ap, starpu_data_handle_t);
	blockRowPtr           = va_arg(ap, unsigned*);
	nchunks               = va_arg(ap, unsigned);
	filter.filter_func    = indx_filter_func_vector;
	filter.nchildren      = nchunks;
	filter.filter_arg_ptr = blockRowPtr;
	starpu_data_partition(handle_copy, &filter);
	break;

      case CSR_INDEX_PARTITION:
	handle_copy           = va_arg(ap, starpu_data_handle_t);
	blockRowPtr           = va_arg(ap, unsigned*);
	nchunks               = va_arg(ap, unsigned);
	filter.filter_func    = csr_index_filter;
	filter.nchildren      = nchunks;
	filter.filter_arg_ptr = blockRowPtr;
	starpu_data_partition(handle_copy, &filter);
	break;

      case CSR_2D_INDEX_PARTITION:
	handle_copy           = va_arg(ap, starpu_data_handle_t);
	matrix_2D             = va_arg(ap, struct csr_2D_struct*);
	blockRowPtr           = va_arg(ap, unsigned*);
	nchunks               = va_arg(ap, unsigned);
	filter.filter_func    = csr_index_filter;
	filter.nchildren      = nchunks;
	filter.filter_arg_ptr = blockRowPtr;
	starpu_data_partition(handle_copy, &filter);
	filter.filter_func    = csr_col_index_filter;
	for(chunk = 0; chunk < nchunks; chunk++) {
	  filter.filter_arg_ptr = &matrix_2D[chunk];
	  starpu_data_partition(starpu_data_get_sub_data(handle_copy, 1, chunk), &filter);
	}
	break;
	
      case CYCLIC_PREFETCH:	
	handle_copy = va_arg(ap, starpu_data_handle_t);
	async       = va_arg(ap, int);
	cyclic_prefetch(handle_copy, async);
	break;

      case CYCLIC_PREFETCH_2D:	
	handle_copy  = va_arg(ap, starpu_data_handle_t);
	distribution = va_arg(ap, unsigned**);
	async       = va_arg(ap, int);
	cyclic_prefetch_2D(handle_copy, distribution, async);
	break;

      case PREFETCH_ON_ALL_WORKERS:
	handle_copy = va_arg(ap, starpu_data_handle_t);
	async       = va_arg(ap, int);
	prefetch_on_all_workers(handle_copy, async);
	break;

      case PREFETCH_CHILDREN_ON_ALL_WORKERS:
	handle_copy = va_arg(ap, starpu_data_handle_t);
	async       = va_arg(ap, int);
	prefetch_children_on_all_workers(handle_copy, async);
	break;

      case UNPARTITION:
	handle_copy  = va_arg(ap, starpu_data_handle_t);
	node         = va_arg(ap, unsigned);
	starpu_data_unpartition(handle_copy, node);
	break;

      case UNPARTITION_2D:
	handle_copy  = va_arg(ap, starpu_data_handle_t);
	nchunks      = va_arg(ap, unsigned);
	node         = va_arg(ap, unsigned);
	for (chunk = 0; chunk < nchunks; chunk++) 
	  starpu_data_unpartition(starpu_data_get_sub_data(handle_copy, 1, chunk), node);
	starpu_data_unpartition(handle_copy, node);
	break;

      case UNREGISTER:
	handle_copy = va_arg(ap, starpu_data_handle_t);
	starpu_data_unregister(handle_copy);
	break;
	
      case FREE_DATA:
        data_cpy     = va_arg(ap, void*);
	starpu_free(data_cpy);
	break;

      case FREE_DATA_ON_ALL:
	vects_cpy     = va_arg(ap, void**);
	n             = va_arg(ap, unsigned);
	free_on_all_nodes(vects_cpy ,n);
	break;

      default:
	fatal_error(__FUNCTION__, "unknown data menagement request");	
	break;
      }
      type = va_arg(ap, int);
    }
  va_end(ap);
}


void
csr_col_index_filter(void *father_interface,
                     void *child_interface,
                     struct starpu_data_filter *f,
                     unsigned id,
                     unsigned nchunks)
{
  struct starpu_csr_interface *csr_father = father_interface;
  struct starpu_csr_interface *csr_child = child_interface;
  struct csr_2D_struct
 *arg = (struct csr_2D_struct *) f->filter_arg_ptr;
  
  csr_child->nnz = arg->block_nnz[id];
  csr_child->nrow =  arg->nrows;
  csr_child->firstentry = 0;
  csr_child->elemsize = csr_father->elemsize;
  
  if (csr_father->nzval) {
    csr_child->rowptr = arg->RowPtrs[id];
    csr_child->colind = arg->ColInds[id];
    csr_child->nzval = (uintptr_t) arg->Vals[id];
  }
}




void 
allocate_data_on_all_nodes(void ***vects_ref,
			   unsigned n,
			   size_t size) 
{
  void **_vects_ref;
  unsigned nb_memory_nodes = starpu_memory_nodes_get_count(), node;

  _vects_ref = calloc(nb_memory_nodes, sizeof(*_vects_ref));
  
  starpu_malloc((void **)_vects_ref, n * size);
  init_dvect(*_vects_ref, 0, n, 3.0);

  for( node = 1;  node < nb_memory_nodes; node++) 
    _vects_ref[node] = (void *) starpu_malloc_on_node(node, n * size);

  *vects_ref =  _vects_ref;
}

static 
void sparsevect_register_on_all_workers(starpu_data_handle_t *handle_ref, 
					void ***vects_ref,
					unsigned n,
					size_t size)
{
  void **_vects_ref;
  unsigned nb_memory_nodes = starpu_memory_nodes_get_count(), node;
  
  allocate_data_on_all_nodes(&_vects_ref, n, size);
  
  sparsevect_data_register(handle_ref, 0, (uintptr_t)*_vects_ref, (uint32_t)n, size);
 
  for( node = 1;  node < nb_memory_nodes; node++) 
    sparsevect_ptr_register(*handle_ref,  node, (uintptr_t)_vects_ref[node],
			    (uintptr_t)_vects_ref[node], 0);

  *vects_ref =  _vects_ref;
}


static 
void vector_register_on_all_workers(starpu_data_handle_t *handle_ref, 
				    void ***vects_ref,
				    unsigned n,
				    size_t size)
{
  void **_vects_ref;
  unsigned nb_memory_nodes = starpu_memory_nodes_get_count(), node;
  
  allocate_data_on_all_nodes(&_vects_ref, n, size);

  starpu_vector_data_register(handle_ref, 0, (uintptr_t)*_vects_ref, (uint32_t)n, size);
 
  for( node = 1;  node < nb_memory_nodes; node++) 
    starpu_vector_ptr_register(*handle_ref,  node, (uintptr_t)_vects_ref[node],
			       (uintptr_t)_vects_ref[node], 0);

  *vects_ref =  _vects_ref;
}

static void
free_on_all_nodes(void **data, unsigned n)
{
  unsigned nb_memory_nodes = starpu_memory_nodes_get_count(), node;
  
  for( node = 1;  node < nb_memory_nodes; node++) 
    starpu_free_on_node(node, (uintptr_t)data[node], n);
  
  starpu_free(*data);
  
  free(data);
}


void 
indx_filter_func_vector(void *father_interface,
			void *child_interface,
			struct starpu_data_filter *f,
			unsigned id,
			unsigned nchunks)
{
  struct starpu_vector_interface *vector_father = father_interface;
  struct starpu_vector_interface *vector_child = child_interface;
  
  uint32_t *blockRowPtr = (uint32_t *) f->filter_arg_ptr;
  uint32_t nx = vector_father->nx;
  size_t elemsize = vector_father->elemsize;

  STARPU_ASSERT(nchunks <= nx);
  uint32_t chunk_size = blockRowPtr[id+1] - blockRowPtr[id];
  size_t offset = blockRowPtr[id] * elemsize;
  
  vector_child->nx = chunk_size;
  vector_child->elemsize = elemsize;

  if (vector_father->ptr) {
    vector_child->ptr = vector_father->ptr + offset;
    vector_child->dev_handle = vector_father->dev_handle;
    vector_child->offset = vector_father->offset + offset;
  }
}


void
csr_index_filter(void *father_interface,
                 void *child_interface,
                 struct starpu_data_filter *f,
                 unsigned id,
                 unsigned nchunks)
{
  struct starpu_csr_interface *csr_father = father_interface;
  struct starpu_csr_interface *csr_child = child_interface;
  uint32_t i, rowptr_indx;
  size_t elemsize = csr_father->elemsize;
  uint32_t *blockRowPtr = (uint32_t *) f->filter_arg_ptr;
  uint32_t *csrRowPtr = (uint32_t *) csr_father->rowptr;
  uint32_t first_row = blockRowPtr[id];
  uint32_t child_firstentry = csrRowPtr[blockRowPtr[id]];

  csr_child->nnz = csrRowPtr[blockRowPtr[id+1]] - child_firstentry;
  csr_child->nrow =  blockRowPtr[id+1] - first_row;
  csr_child->firstentry = child_firstentry;
  csr_child->elemsize = elemsize;

  if (csr_father->nzval) {
    starpu_malloc((void **)&csr_child->rowptr, (csr_child->nrow + 1) * sizeof(uint32_t));
    for(i=0, rowptr_indx = first_row; i<csr_child->nrow + 1; i++,rowptr_indx++)
      csr_child->rowptr[i] = csrRowPtr[rowptr_indx] - csrRowPtr[first_row];
    csr_child->colind = &csr_father->colind[child_firstentry];
    csr_child->nzval = csr_father->nzval + child_firstentry * elemsize;
  }
}



void
init_dvect(double *v, 
	   int start, 
	   int end, 
	   double val)
{
  int i; 
  for(i = start; i < end; i++)
    v[i] = val;
}


void
check_vect_all_same(double *vect, int n, char *name)
{
  int i = 1, all_same = 1;
  for( ; i < n; i++)
    if (vect[i] != vect[0] ) {
      printf("error on %s[%d] = %lf and %s[0] = %lf\n", name, i, vect[i], name, vect[0]);
        all_same = 0;
    }
  if ( all_same )
    printf("%s[*] = %lf\n", name, vect[0]);
}

void 
prefetch_on_all_needed_workers(starpu_data_handle_t handle, unsigned nchunks, int async)
{
  unsigned n_workers = starpu_worker_get_count();
  if (n_workers > nchunks)
    n_workers = nchunks;
  unsigned worker;
  for( worker = 0; worker < n_workers; worker++) 
    starpu_data_prefetch_on_node(handle, starpu_worker_get_memory_node(worker), async);
}


void 
prefetch_on_all_workers(starpu_data_handle_t handle, int async)
{
  unsigned n_workers = starpu_worker_get_count();
  unsigned worker;
  for( worker = 0; worker < n_workers; worker++) 
    starpu_data_prefetch_on_node(handle, starpu_worker_get_memory_node(worker), async);
}

void 
prefetch_children_on_all_workers(starpu_data_handle_t father, int async)
{
  unsigned n_workers = starpu_worker_get_count(), worker;
  unsigned nchunks = starpu_data_get_nb_children(father), chunk;
  
  for(chunk = 0; chunk < nchunks; chunk++)
    for( worker = 0; worker < n_workers; worker++) 
      starpu_data_prefetch_on_node(starpu_data_get_sub_data(father, 1, chunk), 
				   starpu_worker_get_memory_node(worker), async);
}

void 
init_cyclic_worker_list(unsigned *worker_list, unsigned n_workers, unsigned n_tasks)
{
  unsigned i; 
  for ( i=0; i < n_tasks; i++)
    worker_list[i] = i % n_workers;
}

void 
cyclic_prefetch(starpu_data_handle_t father, int async)
{
  unsigned chunk;
  unsigned nchunks = starpu_data_get_nb_children(father);
  unsigned n_workers = starpu_worker_get_count();
  
  for( chunk = 0; chunk < nchunks; chunk++) {
    unsigned mem_node = starpu_worker_get_memory_node(chunk % n_workers);

    starpu_data_prefetch_on_node(starpu_data_get_sub_data(father, 1, chunk),
				 mem_node, async);
  }
}


void 
cyclic_prefetch_2D(starpu_data_handle_t father, 
		   unsigned **distribution, int async)
{
  unsigned i, j;
  unsigned nchunks = starpu_data_get_nb_children(father);
  unsigned n_workers = starpu_worker_get_count();
  
  for( i = 0; i < nchunks; i++) {
    unsigned mem_node = starpu_worker_get_memory_node(i % n_workers);
    for( j = 0; j < nchunks; j++) { 
      if(distribution[i][j] != SPARSEVECT_NONE ) 
	starpu_data_prefetch_on_node(starpu_data_get_sub_data(father, 2, i, j),
				     mem_node, async);
    }
  }
}

void 
gpu_print_double(double *val, char *name)
{
  double cpu_val;
  cudaMemcpy(&cpu_val, val, sizeof(*val), cudaMemcpyDeviceToHost);
  printf("%s = %f on worker %u\n", name, cpu_val, starpu_worker_get_id());
}


void
dgpu_write_vect(double *vect, unsigned n, char *name, unsigned worker)
{
  int i;
  FILE *file;
  double *cpu_copy = malloc(n*sizeof(*cpu_copy));
  char file_name[1000]; 
  sprintf(file_name,"%s_%u", name, worker);
  file = fopen(file_name, "w");
  cudaMemcpy(cpu_copy, vect, n*sizeof(*vect),  cudaMemcpyDeviceToHost);
  for(i = 0 ; i < n; i++)
    fprintf(file,"%f\n", cpu_copy[i]);
  free(cpu_copy);
  fclose(file);
}

void
ugpu_write_vect(unsigned *vect, unsigned n, char *name, unsigned worker)
{
  int i;
  FILE *file;
  unsigned *cpu_copy = malloc(n*sizeof(*cpu_copy));
  char file_name[1000]; 
  sprintf(file_name,"%s_%u", name, worker);
  file = fopen(file_name, "w");
  cudaMemcpy(cpu_copy, vect, n*sizeof(*vect),  cudaMemcpyDeviceToHost);
  for(i = 0 ; i < n; i++)
    fprintf(file,"%u\n", cpu_copy[i]);
  free(cpu_copy);
  fclose(file);
}

void
write_uvect(unsigned *vect, unsigned n, char *name)
{
  int i;
  FILE *file;
  file = fopen(name, "w");
  fprintf(file,"size: %u\n", n);
  for(i = 0 ; i < n; i++)
    fprintf(file,"%u\n", vect[i]);
  fclose(file);
}


void
gpu_check_vect_all_same(double *vect, unsigned n, char *name)
{
  int i = 1, all_same = 1;
  double *cpu_copy = malloc(n*sizeof(*cpu_copy));
  cudaMemcpy(cpu_copy, vect, n*sizeof(*vect),  cudaMemcpyDeviceToHost);
  for( ; i < n; i++)
    if (cpu_copy[i] != cpu_copy[0] ) {
      printf("error on %s[%d] = %lf and %s[0] = %lf\n", name, i, cpu_copy[i], name, cpu_copy[0]);
      all_same = 0;
      break;
    }
  if ( all_same )
    printf("%s[*] = %lf\n", name, cpu_copy[0]);
  free(cpu_copy);
}


void
gpu_check_suite(unsigned *vect, unsigned n, char *name)
{
  int i, all_same = 1;
  unsigned *cpu_copy = malloc(n*sizeof(*cpu_copy));
  cudaMemcpy(cpu_copy, vect, n*sizeof(*vect),  cudaMemcpyDeviceToHost);
  unsigned val = cpu_copy[0];
  for( i = 0 ; i < n; i++) {
    if (cpu_copy[i] != val ) {
      printf("error on %s[%d] = %u and should be %u \n", name, i, cpu_copy[i], val);
      all_same = 0;
    }
    val = val + 1.0;
  }
  if ( all_same )
    printf("%s is a suite \n", name);
  free(cpu_copy);
}

void
check_matrix_handle_identity(starpu_data_handle_t handle,
			     struct starpu_data_filter *filter,
			     char *name, char *prefix, int unpartition)
{
  unsigned *row_ptr, *colInd;
  double *val;
  unsigned m;
  
  starpu_task_wait_for_all ();
  if (unpartition) 
    starpu_data_unpartition(handle, 0);
  starpu_data_acquire(handle, STARPU_R);  

  row_ptr = (unsigned *) starpu_csr_get_local_rowptr(handle);
  colInd  = (unsigned *) starpu_csr_get_local_colind(handle);
  val     = (double *) starpu_csr_get_local_nzval(handle);
  m       = (unsigned)  starpu_csr_get_nrow(handle);
  
  printf("%s\n", prefix);
  {
    unsigned i, j, all_same = 1;
    for(i = 0; i < m; i++) {
      for(j = row_ptr[i]; j < row_ptr[i+1]; j++)
	if (val[j] != 1.00 ) {
	  printf("error on %s[%u][%u] = %lf instead of 1.00\n", name, i, j, val[j]);
	  all_same = 0;
	  break;
	}
      if ( !all_same )
	break;
    }
    if ( all_same )
      printf("%s is a identity  matrix\n", name);
  }
  printf("\n");
  starpu_data_release(handle);
  if (unpartition) 
    starpu_data_partition(handle, filter);
}


void
check_vect_handle_all_same(starpu_data_handle_t handle, 
			   struct starpu_data_filter *filter,
			   char *name, char *prefix, int unpartition)
{
  double *vect;
  unsigned n;

  starpu_task_wait_for_all ();
  if (unpartition) 
    starpu_data_unpartition(handle, 0);
  starpu_data_acquire(handle, STARPU_R);  

  vect = (double *) starpu_vector_get_local_ptr(handle);
  n    = (unsigned) starpu_vector_get_nx(handle);

  printf( "%s\n", prefix);
  {
    int i = 1, all_same = 1;
    for( ; i < n; i++)
      if (vect[i] != vect[0] ) {
        printf("error on %s[%d] = %lf and %s[0] = %lf\n", name, i, vect[i], name, vect[0]);
        all_same = 0;
	break;
      }
    if ( all_same )
      printf("%s[*] = %lf\n", name, vect[0]);
  }
  printf("\n");
  
  starpu_data_release(handle);
  if (unpartition) 
    starpu_data_partition(handle, filter);
}


void
csr_2D_destroy(struct csr_2D_struct *self)
{
  unsigned nchunks = self[0].nchunks, chunk, chunk_j;

  for (chunk = 0; chunk < nchunks; chunk++)
    {
      for(chunk_j = 0; chunk_j < nchunks; chunk_j++)
	{
	  starpu_free(self[chunk].RowPtrs[chunk_j]);
	  if(  self[chunk].block_nnz[chunk_j] != 0 ) {
	    starpu_free(self[chunk].ColInds[chunk_j]);
	    starpu_free(self[chunk].Vals[chunk_j]);
	  }
	}
      free(self[chunk].block_nnz);
      free(self[chunk].RowPtrs);
      free(self[chunk].ColInds);
      free(self[chunk].Vals);
    }
  free(self);
}


struct csr_2D_struct *
csr_2D_index_partition(uint32_t *blockRowPtr, uint32_t nchunks, uint32_t *csrColInd, double *csrVal, uint32_t *csrRowPtr)
{
  uint32_t **block_nnz;
  uint32_t ***RowPtrS;
  uint32_t ***ColIndS;
  double ***ValS;
  unsigned i, row, row_block, column_block;
  struct csr_2D_struct *res = malloc(nchunks * sizeof (*res));

  block_nnz = malloc(nchunks * sizeof(*block_nnz));
  for( i = 0; i < nchunks; i++)
    block_nnz[i] = calloc(nchunks, sizeof(**block_nnz));

  RowPtrS = malloc( nchunks * sizeof (*RowPtrS));
  for(i = 0; i < nchunks; i++)
    RowPtrS[i] = malloc( nchunks * sizeof (**RowPtrS));

  ColIndS = malloc( nchunks * sizeof (*ColIndS));
  for(i = 0; i < nchunks; i++)
    ColIndS[i] = malloc( nchunks *sizeof (**ColIndS));

  ValS = malloc( nchunks * sizeof (*ValS));
  for(i = 0; i < nchunks; i++)
    ValS[i] = malloc( nchunks *sizeof (**ValS));

  // for every row  block
  for ( row_block = 0; row_block < nchunks; row_block++)
    {
      // for every line in the row block 
      for (row = blockRowPtr[row_block]; row < blockRowPtr[row_block+1]; row++)
        {
          unsigned column_indx = 0;
	  // for every column (two dim)  block 
          for( column_block = 0; column_block < nchunks ; column_block++)
            {
              while (csrRowPtr[row] + column_indx < csrRowPtr[row+1])
                {
		  if(csrColInd[csrRowPtr[row] + column_indx] >= blockRowPtr[column_block+1])
		    break; 
		  // count the number of nnz per block
                  block_nnz[row_block][column_block]++;
                  column_indx++;
                }
            }
        }

      //for every 2D block creates data
      unsigned nrows = blockRowPtr[row_block+1] - blockRowPtr[row_block] + 1;
      for( column_block = 0; column_block < nchunks; column_block++)
        {
          starpu_malloc((void **) &RowPtrS[row_block][column_block], nrows * sizeof(***RowPtrS));
	  if(  block_nnz[row_block][column_block] == 0 )
	    continue;
          starpu_malloc((void **) &ColIndS[row_block][column_block], block_nnz[row_block][column_block] * sizeof(***ColIndS));
          starpu_malloc((void **) &ValS[row_block][column_block], block_nnz[row_block][column_block]  * sizeof(***ValS));
        }

      unsigned *local_column_indx = calloc(nrows, sizeof(*local_column_indx));
      for (row = blockRowPtr[row_block]; row < blockRowPtr[row_block+1]; row++)
        {
          unsigned column_indx = 0;
          for(column_block = 0; column_block < nchunks; column_block++)
            {
              RowPtrS[row_block][column_block][row-blockRowPtr[row_block]] = local_column_indx[column_block];
              while ( csrRowPtr[row] + column_indx < csrRowPtr[row+1] &&
		      csrColInd[ csrRowPtr[row] + column_indx ] < blockRowPtr[column_block+1] )
                {
                  ValS[row_block][column_block][local_column_indx[column_block]] = csrVal[csrRowPtr[row] + column_indx];
                  ColIndS[row_block][column_block][local_column_indx[column_block]] =  csrColInd[ csrRowPtr[row] + column_indx] - blockRowPtr[column_block];
                  column_indx++; local_column_indx[column_block]++;
                }
            }
        }
      free(local_column_indx); 
      for(column_block= 0; column_block < nchunks; column_block++)
        RowPtrS[row_block][column_block][row-blockRowPtr[row_block]] = block_nnz[row_block][column_block];
    }

  for( row_block = 0; row_block < nchunks; row_block++)
    {
      res[row_block].nchunks   = nchunks;
      res[row_block].nrows     = blockRowPtr[row_block + 1] - blockRowPtr[row_block];
      res[row_block].block_nnz = block_nnz[row_block];
      res[row_block].RowPtrs   = RowPtrS[row_block];
      res[row_block].ColInds   = ColIndS[row_block];
      res[row_block].Vals      = ValS[row_block];
    }
  
  free(block_nnz);
  free(RowPtrS);
  free(ColIndS);
  free(ValS);

  return res;
}

unsigned **
create_distribution_vector(struct csr_2D_struct *matrix_2D, 
			   unsigned nchunks)
{
  unsigned **distribution, i, j;
  distribution = malloc(nchunks*sizeof(*distribution));
  for(i = 0; i < nchunks; i++) {
    distribution[i] = malloc(nchunks*sizeof(**distribution));
    for(j = 0; j < nchunks; j++) {
      if(matrix_2D[i].block_nnz[j] == 0) 
	distribution[i][j] = SPARSEVECT_NONE; 
      else 
	distribution[i][j] = SPARSEVECT_FULL; 
    }
  }
#if defined  SPEAKING_LIKE_GOSSIP_GIRL
  printf("DISTRIBUTION:\n");
  for(i = 0; i < nchunks; i++)
    print_need_pack_line(distribution[i], nchunks);
#endif

  return  distribution;
}

