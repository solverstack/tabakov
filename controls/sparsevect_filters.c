#include "common.h"
#include "auxiliary.h"
#include "data_helpers.h"
#include "sparsevect_interface.h"

#if defined  SPEAKING_LIKE_GOSSIP_GIRL || defined WRITE_SPARSEVECTS
static void
print_cudaID(void *arg)
{
  int cuda_device;
  cudaError_t ret;
  ret =  cudaGetDevice(&cuda_device);
  if ( ret != cudaSuccess )
    STARPU_CUDA_REPORT_ERROR(ret);
  printf("Starpu_worker %u is the CUDA device %d\n", starpu_worker_get_id(), cuda_device);
} 

static void
print_unsigned_line(uint32_t *line, uint32_t n)
{
  uint32_t i;
  for(i = 0; i < n; i++)
    fprintf(stdout,"%u\t",line[i]);
  fprintf(stdout,"\n");
}

static void
print_need_pack_line(uint32_t *line, uint32_t n)
{
  uint32_t i;
  for(i = 0; i < n; i++)
    {
      switch (line[i]) {
      case SPARSEVECT_FULL:
	fprintf(stdout,"FULL\t");
	break;
      case SPARSEVECT_PACK:
	fprintf(stdout,"PACK\t");
	break;
      case SPARSEVECT_NONE:
	fprintf(stdout,"NONE\t");
	break;
      case SPARSEVECT_CONTINUOUS:
	fprintf(stdout,"CONTI\t");
	break;
      }
    }
  fprintf(stdout,"\n");
}
#endif


struct _sparsevect_internal_data {
  // all this memory is in the main memory
  uint32_t **indices_sizes;
  uint32_t **need_pack;
  unsigned nchunks;
  size_t   elem_size;
  pthread_mutex_t **memory_mutex;
  uint32_t **memory_state;
  uint32_t **first_sparse_indice;

  // all this memory is in allocated on different memory nodes
  uint32_t ***send_indices;
  uint32_t ***recive_indices;
  uintptr_t **send_buffers;
  uintptr_t **recive_buffers;
};

struct sparsevect_filter_arg { 
  sparsevect_internal_data internal_data; 
  unsigned *blockRowPtr; 
  unsigned *node_list;
};

static void
sparsevect_allocate_device_data(sparsevect_internal_data internal_data, 
				uint32_t ***father_indices, 
				unsigned *node_list)
{
  uint32_t **indices_sizes = internal_data->indices_sizes;
  uint32_t **need_pack     = internal_data->need_pack;
  size_t   elem_size       = internal_data->elem_size;
  unsigned nchunks         = internal_data->nchunks;
  unsigned nb_nodes        = starpu_memory_nodes_get_count();
  unsigned chunk, node;

  uint32_t  ***_sparsevect_send_indices;
  uint32_t  ***_sparsevect_recive_indices;
  uintptr_t **_send_buffers;
  uintptr_t **_recive_buffers;

  _sparsevect_send_indices    = calloc(nchunks, sizeof(*_sparsevect_send_indices));
  _sparsevect_recive_indices = calloc(nchunks, sizeof(*_sparsevect_recive_indices));
  for(chunk = 0; chunk < nchunks; chunk++) { 
    _sparsevect_send_indices[chunk]  = calloc(nb_nodes, sizeof(*_sparsevect_send_indices));
    _sparsevect_recive_indices[chunk] = calloc(nb_nodes, sizeof(*_sparsevect_recive_indices));
    for( node = 0; node < nb_nodes; node++) {

      if (need_pack[chunk][node] == SPARSEVECT_PACK) { 
	_sparsevect_send_indices[chunk][node] =
	  (uint32_t *) starpu_malloc_on_node(node_list[chunk],
					     indices_sizes[chunk][node] *
					     sizeof(***_sparsevect_send_indices));
	starpu_interface_copy((uintptr_t)father_indices[chunk][node], 0, 0,
			      (uintptr_t)_sparsevect_send_indices[chunk][node], 0, node_list[chunk],
			      indices_sizes[chunk][node] * sizeof(***_sparsevect_send_indices), NULL);
	_sparsevect_recive_indices[chunk][node] =
	  (uint32_t *) starpu_malloc_on_node(node,
					     indices_sizes[chunk][node] *
					     sizeof(***_sparsevect_recive_indices));
	starpu_interface_copy((uintptr_t)father_indices[chunk][node], 0, 0,
			      (uintptr_t)_sparsevect_recive_indices[chunk][node], 0, node,
			      indices_sizes[chunk][node] * sizeof(***_sparsevect_recive_indices), NULL);
	
      }
      if (need_pack[chunk][node] == SPARSEVECT_CONTINUOUS) { 
	uint32_t *tmp = malloc(sizeof(*tmp));
	*tmp = *father_indices[chunk][node];
	_sparsevect_send_indices[chunk][node] = tmp;
      }
    }
  }
            
  _send_buffers   = calloc(nchunks, sizeof(*_send_buffers));
  _recive_buffers = calloc(nchunks, sizeof(*_recive_buffers));
  for(chunk = 0; chunk < nchunks; chunk++) {
    _send_buffers[chunk]   = calloc(nb_nodes, sizeof(**_send_buffers));
    _recive_buffers[chunk] = calloc(nb_nodes, sizeof(**_recive_buffers));
    for(node = 0;  node < nb_nodes; node++) {
      if (need_pack[chunk][node] == SPARSEVECT_PACK ) {
	_send_buffers[chunk][node]    = starpu_malloc_on_node(node_list[chunk], indices_sizes[chunk][node] * elem_size);
	
	_recive_buffers[chunk][node]  = starpu_malloc_on_node(node, indices_sizes[chunk][node] * elem_size);
	
      }
    }
  }

#ifdef SPEAKING_LIKE_GOSSIP_GIRL
  unsigned worker, nb_workers = starpu_worker_get_count();
  fprintf(stdout,"EXPLICIT TALKING ABOUT THE SEND/RECIVE BUFFERS ALLOCATIONS\n");  
  for(worker = 0; worker < nb_workers; worker++)
    if( starpu_worker_get_type(worker) == STARPU_CUDA_WORKER) 
      printf("worker %u is a GPU on memory node %u\n", worker, 
	     starpu_worker_get_memory_node(worker));
    else  if( starpu_worker_get_type(worker) == STARPU_CPU_WORKER) 
      printf("worker %u is a CPU on memory node %u\n", worker, 
	     starpu_worker_get_memory_node(worker));
  starpu_execute_on_each_worker(print_cudaID, NULL, STARPU_CUDA);
  printf("\n");
  
  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0;  node < nb_nodes; node++) {
      struct cudaPointerAttributes send_buff_attr, recive_buff_attr;
      cudaError_t cuda_ret;
      if (need_pack[chunk][node] == SPARSEVECT_PACK ) {
	printf("_send_buffers[%u][%u]    = starpu_malloc_on_node(%u, %lu);(%u,%lu)\n",
	       chunk, node, node_list[chunk],
	       (unsigned long) indices_sizes[chunk][node] * elem_size,
	       indices_sizes[chunk][node], (unsigned long) elem_size);
	cuda_ret = cudaPointerGetAttributes(&send_buff_attr,   (void*)_send_buffers[chunk][node]);
	if ( cuda_ret == cudaSuccess ) {
	  printf("starpu allocating: _send_buffers[%u][%u]\ton node %u\t(%p)\n", chunk, node, node_list[chunk], (void*)_send_buffers[chunk][node]);
	  printf("cuda: _send_buffers[%u][%u] is a ", chunk, node);
	  if(send_buff_attr.memoryType == cudaMemoryTypeDevice) 
	      printf("device ptr on the GPU %d\n\n",
		     send_buff_attr.device);
	  else
	    printf("host memory\n\n");
	}  else if ( cuda_ret == cudaErrorInvalidDevice )
	  printf("cudaPointerAttributes for send failed with cudaErrorInvalidDevice\n\n");
	else
	  printf("cudaPointerAttributes for send failed with %d\n\n", cuda_ret);
	
	
	printf("_recive_buffers[%u][%u]  = starpu_malloc_on_node(%u, %lu);(%u,%lu)\n",
	       chunk, node, node, 
	       (unsigned long) indices_sizes[chunk][node] * elem_size, 
	       indices_sizes[chunk][node], (unsigned long) elem_size);
	cuda_ret = cudaPointerGetAttributes(&recive_buff_attr, (void*)_recive_buffers[chunk][node]);
	if ( cuda_ret == cudaSuccess ) {
	  printf("starpu allocating:  _recive_buffers[%u][%u]\ton node %u\t(%p)\n", chunk, node, node, (void*)_recive_buffers[chunk][node]);
	  printf("cuda: _recive_buffers[%u][%u] is a ", chunk, node);
	  if(recive_buff_attr.memoryType == cudaMemoryTypeDevice) 
	    printf("device ptr on the GPU %d\n\n",
		   recive_buff_attr.device);
	  else
	    printf("host memory\n\n");
	}  else if ( cuda_ret == cudaErrorInvalidDevice )
	  printf("cudaPointerAttributes for recive failed with cudaErrorInvalidDevice\n\n");
	  else
	    printf("cudaPointerAttributes for recive failed with %d\n\n", cuda_ret);
      }
    }
    printf("\n");
  }
    
  fprintf(stdout,"ALLOCATING THE SEND BUFFERS\n");  
  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0;  node < nb_nodes; node++) {
      if (need_pack[chunk][node] == SPARSEVECT_PACK ) 
	fprintf(stdout, "%u %u\t", node_list[chunk], indices_sizes[chunk][node]);
      else 
	fprintf(stdout,"empty\t");
    }
    fprintf(stdout, "\n");
    for(node = 0;  node < nb_nodes; node++) {
      fprintf(stdout, "(%p)\t", (void*) _send_buffers[chunk][node]);
    }
    fprintf(stdout, "\n");
    fprintf(stdout, "\n");
  }
  fprintf(stdout, "\n");
  printf("\n");

  fprintf(stdout,"ALLOCATING THE RECIVE BUFFERS`\n");  
  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0;  node < nb_nodes; node++) {
      if (need_pack[chunk][node] == SPARSEVECT_PACK ) 
	fprintf(stdout, "%u %u\t", node, indices_sizes[chunk][node]);
      else
	fprintf(stdout,"empty\t");
    }
    fprintf(stdout, "\n");
    for(node = 0;  node < nb_nodes; node++) {
      fprintf(stdout, "(%p)\t", (void*) _recive_buffers[chunk][node]);
    }
    fprintf(stdout, "\n");
    fprintf(stdout, "\n");
  }
  fprintf(stdout, "\n");
#endif
    
  internal_data->send_indices    = _sparsevect_send_indices;
  internal_data->recive_indices  = _sparsevect_recive_indices;
  internal_data->send_buffers   = _send_buffers;
  internal_data->recive_buffers = _recive_buffers;
}


void 
sparsevect_free_internal_data(sparsevect_internal_data self, 
			      unsigned *worker_list)
{
  uint32_t **indices_sizes = self->indices_sizes;
  uint32_t **need_pack = self->need_pack;
  unsigned nchunks = self->nchunks;
  size_t   elem_size = self->elem_size;
  pthread_mutex_t **memory_mutex = self->memory_mutex;
  uint32_t **memory_state = self->memory_state;
  uint32_t **first_sparse_indice = self->first_sparse_indice;

  uint32_t ***sparsevect_send_indices = self->send_indices;
  uint32_t ***sparsevect_recive_indices = self->recive_indices;
  uintptr_t **send_buffers = self->send_buffers;
  uintptr_t **recive_buffers = self->recive_buffers;
  unsigned nb_nodes = starpu_memory_nodes_get_count();
  unsigned node_list[nchunks];
  unsigned chunk, node;

  for(chunk=0; chunk < nchunks; chunk++)
    node_list[chunk] = starpu_worker_get_memory_node(worker_list[chunk]);

  for(chunk = 0; chunk < nchunks; chunk++) {
    pthread_mutex_destroy(memory_mutex[chunk]);
    free(memory_mutex[chunk]);
  }
  free(memory_mutex);

  for(chunk = 0; chunk < nchunks; chunk++)
    free(memory_state[chunk]);
  free(memory_state);

  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0; node < nb_nodes; node++) {
      if (need_pack[chunk][node] == SPARSEVECT_PACK ) {
  	starpu_free_on_node(node_list[chunk], (uintptr_t)sparsevect_send_indices[chunk][node],
  			    indices_sizes[chunk][node]*sizeof(***sparsevect_send_indices));
  	starpu_free_on_node(node, (uintptr_t)sparsevect_recive_indices[chunk][node],
  			    indices_sizes[chunk][node]*sizeof(***sparsevect_recive_indices));
      }
    }
    free(sparsevect_send_indices[chunk]);
    free(sparsevect_recive_indices[chunk]);
    free(first_sparse_indice[chunk]);
  }
  free(sparsevect_send_indices);
  free(sparsevect_recive_indices);
  free(first_sparse_indice);

  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0;  node < nb_nodes; node++) {
      if (need_pack[chunk][node] == SPARSEVECT_PACK )  {
  	starpu_free_on_node(node_list[chunk],   send_buffers[chunk][node], indices_sizes[chunk][node] * elem_size);
  	starpu_free_on_node(node, recive_buffers[chunk][node], indices_sizes[chunk][node] * elem_size);
      }
    }
    free(send_buffers[chunk]);
    free(recive_buffers[chunk]);
  }

  free(send_buffers);
  free(recive_buffers);
  free(self);
}


void 
sparsevect_index_filter(void *father_interface,
			void *child_interface,
			struct starpu_data_filter *f,
			unsigned id,
			unsigned nchunks)
{
  struct sparsevect_interface *vector_father = father_interface;
  struct sparsevect_interface *vector_child = child_interface;

  struct sparsevect_filter_arg *arg = (struct sparsevect_filter_arg*) f->filter_arg_ptr;
  sparsevect_internal_data internal_data = arg->internal_data;
  unsigned *blockRowPtr =  arg->blockRowPtr;

  uint32_t nx = vector_father->nx;

  STARPU_ASSERT(nchunks <= nx);

  size_t elemsize        = vector_father->elemsize;

  uint32_t chunk_size    = blockRowPtr[id+1] - blockRowPtr[id];
  size_t offset = blockRowPtr[id] * elemsize;

  vector_child->nx       = chunk_size;
  vector_child->elemsize = elemsize;
  vector_child->father   = 0;
  vector_child->id       = id;

  // main memory 
  vector_child->memory_mutex              = internal_data->memory_mutex[id];
  vector_child->memory_state              = internal_data->memory_state[id];
  vector_child->need_packing              = internal_data->need_pack;
  vector_child->indices_sizes             = internal_data->indices_sizes;
  vector_child->first_sparse_indice       = internal_data->first_sparse_indice[id];

  // device  memory
  vector_child->send_indices   = internal_data->send_indices;
  vector_child->recive_indices = internal_data->recive_indices;

  vector_child->send_buffers   = internal_data->send_buffers[id];
  vector_child->recive_buffers = internal_data->recive_buffers[id];

  if (vector_father->ptr) {
    vector_child->ptr        = vector_father->ptr + offset;
    vector_child->dev_handle = vector_father->dev_handle;
    vector_child->offset     = vector_father->offset + offset;
  }
}


void 
create_sparse_vectors(struct csr_2D_struct *args,
		      unsigned *blockRowPtr,
		      uint32_t ***need_pack,
		      uint32_t ***sparse_vectors_sizes,
		      uint32_t ****sparse_vectors_indices)
{
  uint32_t i, j, k, l; 
  uint32_t **_need_pack;
  uint32_t **_sparse_vectors_sizes;
  uint32_t ***_sparse_vectors_indices;
  uint32_t nchunks  = args->nchunks;

  /* this vector will be used to check if in every  column of a 
     2D block, there is at least 1 NNZ */
  uint32_t ***mask_vectors = malloc (nchunks * sizeof(*mask_vectors)); 
  for ( i = 0; i < nchunks; i++) {
    mask_vectors[i] = malloc( nchunks * sizeof(**mask_vectors));
    for( j = 0; j < nchunks; j++) {
      uint32_t size = blockRowPtr[j+1] - blockRowPtr[j]; 
      mask_vectors[i][j] = calloc (size, sizeof(***mask_vectors)); 
    }
  }

  _sparse_vectors_sizes = malloc(nchunks * sizeof(*_sparse_vectors_sizes));  
  _sparse_vectors_indices = malloc (nchunks * sizeof(*_sparse_vectors_indices)); 
  _need_pack = malloc(nchunks * sizeof(*_need_pack)); 

  /* for each row */
  for( i = 0; i< nchunks; i++) {
    _sparse_vectors_sizes[i]   = malloc(nchunks * sizeof(**_sparse_vectors_sizes));
    _sparse_vectors_indices[i] = malloc (nchunks * sizeof(**_sparse_vectors_indices)); 
    _need_pack[i]              = malloc (nchunks* sizeof(**_need_pack)); 

    /* for each colum */
    for ( j = 0; j < nchunks; j++) {
      uint32_t block_size =  blockRowPtr[j+1] - blockRowPtr[j];
      uint32_t block_nnz = args[i].block_nnz[j];
      uint32_t *colInds = args[i].ColInds[j];
      uint32_t sparse_size = 0; 
      for(k = 0; k < block_nnz; k++) {
	mask_vectors[i][j][colInds[k]] = 1;
      }
      for(k = 0; k < block_size; k++) {
	if ( mask_vectors[i][j][k])
	  sparse_size++;
      }

      _sparse_vectors_sizes[i][j] = sparse_size; /* the size of each sparse_vector */

      if (_sparse_vectors_sizes[i][j] == 0)
	_need_pack[i][j] = SPARSEVECT_NONE;
      else if (_sparse_vectors_sizes[i][j]  < (blockRowPtr[j+1] - blockRowPtr[j]))  {
	_need_pack[i][j] = SPARSEVECT_PACK; 
	starpu_malloc((void **)&_sparse_vectors_indices[i][j], _sparse_vectors_sizes[i][j] * sizeof(***_sparse_vectors_indices));
	for(k = 0, l =0 ; k < block_size; k++)
	  if (mask_vectors[i][j][k])
	    _sparse_vectors_indices[i][j][l++] = k;
      } else {
	_need_pack[i][j] = SPARSEVECT_FULL;
      }
    }
  }

#if defined  SPEAKING_LIKE_GOSSIP_GIRL  
  fprintf(stdout,"BLOCKROWPTR:\n");
  print_unsigned_line(blockRowPtr, nchunks+1);

  fprintf(stdout,"AFTER CREATING SPARSE VECTORS\n");
  for(i = 0; i < nchunks; i++){
    print_unsigned_line( _sparse_vectors_sizes[i], nchunks);
    print_need_pack_line(_need_pack[i], nchunks);
#ifdef WRITE_SPARSEVECTS
    for(j = 0; j < nchunks; j++){
      if ( i == j) 
	continue;
      if( _need_pack[i][j] == SPARSEVECT_PACK){ 
	char file_name[100];
	sprintf(file_name,"sparsevect_ind[%u][%u]", i, j);
	write_uvect(_sparse_vectors_indices[i][j], _sparse_vectors_sizes[i][j], file_name);
      }
    }
#endif
    fprintf(stdout,"\n");
  }
#endif

  /* free local data */
  for(i = 0; i < nchunks; i++){
    for(j = 0; j < nchunks; j++)
      free(mask_vectors[i][j]); 
    free(mask_vectors[i]); 
  }
  free(mask_vectors); 

  *sparse_vectors_sizes = _sparse_vectors_sizes;
  *sparse_vectors_indices = _sparse_vectors_indices;
  *need_pack =_need_pack;
}

static int
compare_unsigned(const void *a, const void *b)
{
  uint32_t *a_ = (uint32_t *) a;
  uint32_t *b_ = (uint32_t *) b;

  if (*a_ > *b_) 
    return 1; 
  if (*a_ == *b_) 
    return 0;
  else
    return -1; 
}

void
merge_idndice_vectors(unsigned nchunks,
		      unsigned *blockRowPtr,
		      unsigned *node_list,
		      uint32_t **need_pack,
		      uint32_t **sparse_vectors_sizes,
		      uint32_t ***sparse_vectors_indices,
		      uint32_t ***need_pack_pernode,
		      uint32_t ***sparsevect_sizes_pernode,
		      uint32_t ****sparsevect_indices_pernode, 
		      uint32_t ***first_sparse_indice)
{
  unsigned node, chunk, sorted_indice, i; 
  unsigned nb_nodes = starpu_memory_nodes_get_count();
  uint32_t nb_chunks_pernode[nb_nodes];
  uint32_t sorted_chunks_pernode[nchunks];

  uint32_t **_need_pack_pernode;
  uint32_t **_sparsevect_sizes_pernode;
  uint32_t ***_sparsevect_indices_pernode;
  uint32_t **_first_sparse_indice;

  for(node = 0; node < nb_nodes; node++)
    nb_chunks_pernode[node] = 0;
  for(chunk = 0; chunk < nchunks; chunk++)
    nb_chunks_pernode[node_list[chunk]] += 1;

  /* - indiced array will be used as indices dot he sorted_chunks_pernode 
     - ptr array will be used after, in order to know where the blocks start  
     and finish for each  node */
  uint32_t sorted_chunks_pernode_indices[nb_nodes];
  uint32_t sorted_chunks_pernode_ptr[nb_nodes+1];

  for(node = 0; node < nb_nodes; node++) {
    sorted_chunks_pernode_indices[node] = 0;
    sorted_chunks_pernode_ptr[node]     = 0;
  }

  /* the nb_chunks_pernode[0] = 0 !! */
  for(node = 1; node < nb_nodes; node++) {
    sorted_chunks_pernode_indices[node] = nb_chunks_pernode[node-1] + sorted_chunks_pernode_indices[node-1];
    sorted_chunks_pernode_ptr[node]     = nb_chunks_pernode[node-1] + sorted_chunks_pernode_ptr[node-1];
  }
  sorted_chunks_pernode_ptr[nb_nodes] = nb_chunks_pernode[nb_nodes-1] + sorted_chunks_pernode_ptr[nb_nodes-1];

  /* from 0 to nb_chunks_pernode[1] are the blocks corespoding to the node 0, 
   _chunks_pernode[1] to _chunks_pernode[0] the node 1  etc etc */
  for(chunk = 0; chunk < nchunks; chunk++) {
    node = node_list[chunk];
    sorted_chunks_pernode[sorted_chunks_pernode_indices[node]] = chunk;
    sorted_chunks_pernode_indices[node] += 1;
  }

  /* calculating the need_pack_pernode that will give us the relation 
   which piece of data is neede on which node, and not the relation 
   between all chunks */

  /* WARNING: in need_pack need_pack[i][j] tells how to send the 
     data j  to the node i!!! in need_pack_pernode need_pack_pernode[i][j] 
     tells how to send the data i to the node j(inverse from need_pack)    */
  _need_pack_pernode = malloc(nchunks*sizeof(*_need_pack_pernode));
  for(chunk = 0; chunk < nchunks; chunk++) {
    _need_pack_pernode[chunk] = malloc(nb_nodes*sizeof(**_need_pack_pernode));
    for(node = 0; node < nb_nodes; node++) {
      _need_pack_pernode[chunk][node] = SPARSEVECT_NONE;
      /* sending data to the same node doesnt make any sense */
      if( node == node_list[chunk] )  
	continue;

      for(sorted_indice = sorted_chunks_pernode_ptr[node];
	  sorted_indice < sorted_chunks_pernode_ptr[node+1];
	  sorted_indice++) { 
	uint32_t chunk_on_node = sorted_chunks_pernode[sorted_indice];
	  /* this should never happen because od the previous test few lines above */
	  // TODO: if this never fails, remove it
	  assert(node == node_list[chunk_on_node]);
	/* the case where the whole vector is neede! this case will win 
	   over the packing case */
	if (need_pack[chunk_on_node][chunk] == SPARSEVECT_FULL) 
	  _need_pack_pernode[chunk][node] = SPARSEVECT_FULL;
	/* if it need to be packed and it is not the full vector that is neede
	 *  then we pack the vecotr  */
	if (need_pack[chunk_on_node][chunk] == SPARSEVECT_PACK &&
	    _need_pack_pernode[chunk][node] != SPARSEVECT_FULL) 
	  _need_pack_pernode[chunk][node] = SPARSEVECT_PACK;
      }
    }
  }
  

  _sparsevect_sizes_pernode   = calloc(nchunks, sizeof(*_sparsevect_sizes_pernode));
  _sparsevect_indices_pernode = calloc(nchunks, sizeof(*_sparsevect_indices_pernode));
  _first_sparse_indice = malloc(nchunks*sizeof(*_first_sparse_indice));
  for(chunk = 0; chunk < nchunks; chunk++) {

    _sparsevect_sizes_pernode[chunk]   = calloc(nb_nodes, sizeof(**_sparsevect_sizes_pernode));
    _sparsevect_indices_pernode[chunk] = calloc(nb_nodes, sizeof(**_sparsevect_indices_pernode));
    _first_sparse_indice[chunk] = calloc(nb_nodes, sizeof(**_first_sparse_indice));
    for(node = 0; node < nb_nodes; node++) {

      /* ALL THE UGLY THING ARE DONE HERE!!!! */
      if(_need_pack_pernode[chunk][node] != SPARSEVECT_PACK) 
	continue;
      if(node == node_list[chunk]) 
	// TODO: if this never fails, remove it
	assert(0);

     /* first find out if more then one vector need to be packed */
      uint32_t chunks_to_be_merged[nchunks], nb_chunks_to_be_merged=0;
      for(sorted_indice = sorted_chunks_pernode_ptr[node];  
	  sorted_indice < sorted_chunks_pernode_ptr[node+1];
	  sorted_indice++) {
	uint32_t chunk_on_node = sorted_chunks_pernode[sorted_indice];

	if (need_pack[chunk_on_node][chunk] == SPARSEVECT_PACK) { 
	  chunks_to_be_merged[nb_chunks_to_be_merged] = chunk_on_node;
	  nb_chunks_to_be_merged++;
	}
      }
      // TODO: if this never fails, remove it
      assert( nb_chunks_to_be_merged != 0);

      /* there is only one vect to be packed, so the data is allready  as it should be */
      if (nb_chunks_to_be_merged == 1) {
	uint32_t nb_elem =  sparse_vectors_sizes[chunks_to_be_merged[0]][chunk];
	uint32_t *merged_vect;

	starpu_malloc((void **)&merged_vect, nb_elem*sizeof(*merged_vect));
	memcpy(merged_vect, (void *)sparse_vectors_indices[chunks_to_be_merged[0]][chunk], nb_elem*sizeof(*merged_vect));
	_sparsevect_sizes_pernode[chunk][node] = nb_elem;
	_sparsevect_indices_pernode[chunk][node] = merged_vect;
	_first_sparse_indice[chunk][node] = _sparsevect_indices_pernode[chunk][node][0];
	continue;
      }

      /* in this case more then one chunks need to be packed, so they should be merged */
      uint32_t merged_vect_raw_size = 0;
      uint32_t merged_vect_size = 1;
      uint32_t merge_chunk_indice;
      /* first find the size of the raw merged vector */
      for(merge_chunk_indice = 0;
	  merge_chunk_indice < nb_chunks_to_be_merged;
	  merge_chunk_indice++) {
	uint32_t chunk_to_be_merged = chunks_to_be_merged[merge_chunk_indice];
	merged_vect_raw_size += sparse_vectors_sizes[chunk_to_be_merged][chunk];
      }

      /* second  copy all the indice vectors to the merged raw vector  */
      uint32_t merged_vect_raw[merged_vect_raw_size];
      uint32_t first_index = 0;
      for(merge_chunk_indice = 0;
	  merge_chunk_indice < nb_chunks_to_be_merged;
	  merge_chunk_indice++) {
	uint32_t chunk_to_be_merged = chunks_to_be_merged[merge_chunk_indice];
	unsigned nb_elem = sparse_vectors_sizes[chunk_to_be_merged][chunk];
	memcpy(&merged_vect_raw[first_index], sparse_vectors_indices[chunk_to_be_merged][chunk], nb_elem*sizeof(*merged_vect_raw));
	first_index += nb_elem;
      }
      qsort(merged_vect_raw, merged_vect_raw_size, sizeof(*merged_vect_raw), compare_unsigned);

      /* Calculate the number elements in the merged vector by taking out 
       * entries that are */ 
      /* it goes until merged_vect_raw_size-1 because the upper limit of i causes pbs 
       * in the while loop */
      for(i = 0; i < merged_vect_raw_size-1; i++) {
	while (merged_vect_raw[i+1] == merged_vect_raw[i]
	       && i < merged_vect_raw_size-1)
	  i++;
	merged_vect_size++;
      }
      if(merged_vect_raw[merged_vect_raw_size-2] != merged_vect_raw[merged_vect_raw_size-1]) 
	merged_vect_size++;

      /* if the merged  vectors  */
      if (merged_vect_size  ==  blockRowPtr[chunk+1] - blockRowPtr[chunk]  ){
	_need_pack_pernode[chunk][node] = SPARSEVECT_FULL;
	continue;
      }

      /* and finaly  allocate the data for the merged vector and init it */
      uint32_t *merged_vect, index=0;
      starpu_malloc((void **)&merged_vect, merged_vect_size*sizeof(*merged_vect));
      merged_vect[0] = merged_vect_raw[0];
      for(i = 1; i < merged_vect_raw_size; i++) 
	if(merged_vect_raw[i] != merged_vect[index])  {
	  index++;
	  merged_vect[index] = merged_vect_raw[i];
	}
      _sparsevect_sizes_pernode[chunk][node] = merged_vect_size;
      _sparsevect_indices_pernode[chunk][node] = merged_vect;
      _first_sparse_indice[chunk][node] = _sparsevect_indices_pernode[chunk][node][0];
    }
  }


#if defined  SPEAKING_LIKE_GOSSIP_GIRL 
  fprintf(stdout,"AFTER THE MERGING THE SPARSE VECTORS\n");
  for(chunk = 0; chunk < nchunks; chunk++){
    print_unsigned_line(_sparsevect_sizes_pernode[chunk], nb_nodes);
    print_need_pack_line(_need_pack_pernode[chunk], nb_nodes);
    fprintf(stdout,"\n");
  }
#ifdef  WRITE_SPARSEVECTS
  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0; node < nb_nodes; node++) {
      if (_need_pack_pernode[chunk][node] == SPARSEVECT_PACK) {
	char file_name[100];
	sprintf(file_name,"sparsevect_ind_pernode[%u][%u]", chunk, node);
	write_uvect(_sparsevect_indices_pernode[chunk][node], _sparsevect_sizes_pernode[chunk][node], file_name);
      }	
    }
  }
#endif 
#endif

  *need_pack_pernode = _need_pack_pernode;
  *sparsevect_sizes_pernode = _sparsevect_sizes_pernode;
  *sparsevect_indices_pernode = _sparsevect_indices_pernode;
  *first_sparse_indice = _first_sparse_indice;
}


void
find_continuous_parts(unsigned nchunks, 
		      unsigned *blockRowPtr,
		      double alpha_pack,
		      uint32_t **need_pack,
		      uint32_t **sparsevect_sizes,
		      uint32_t ***sparsevect_indices)
{
  unsigned node, chunk; 
  unsigned nb_nodes = starpu_memory_nodes_get_count();
  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0; node < nb_nodes; node++) {
      if(need_pack[chunk][node] == SPARSEVECT_PACK) { 
	unsigned packing_part = sparsevect_indices[chunk][node][sparsevect_sizes[chunk][node] - 1]
	                        - sparsevect_indices[chunk][node][0] ;
	
	if( packing_part <= (unsigned )( blockRowPtr[chunk+1] - blockRowPtr[chunk] ) / alpha_pack )  { 
	  uint32_t *tmp_indices = malloc(sizeof(*tmp_indices));

	  *tmp_indices = *sparsevect_indices[chunk][node];
	  starpu_free(sparsevect_indices[chunk][node]);
	  sparsevect_indices[chunk][node] = tmp_indices;
	  need_pack[chunk][node] = SPARSEVECT_CONTINUOUS; 
	}
      }
    }
  }
#if defined  SPEAKING_LIKE_GOSSIP_GIRL 
  fprintf(stdout,"AFTER THE CALCULTAING THE CONTINUOUS VECTORS\n");
  for(chunk = 0; chunk < nchunks; chunk++){
    print_unsigned_line(sparsevect_sizes[chunk], nb_nodes);
    print_need_pack_line(need_pack[chunk], nb_nodes);
    fprintf(stdout,"\n");
  }
#endif 

}



#ifdef STARPU_USE_CUDA
static void dumb_cuda_func(void *descr[], void *cl_arg)
{
  double *v  = (double *)SPARSEVECT_GET_PTR(descr[0]);
  (void) v;
  update_memory_state(descr[0]);
}
#endif

static void dumb_cpu_func(void *descr[], void *cl_arg)
{
  double *v  = (double *)SPARSEVECT_GET_PTR(descr[0]);
  (void) v;
  update_memory_state(descr[0]);
}

static struct starpu_codelet ddumb_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
 | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {dumb_cpu_func, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {dumb_cuda_func, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .nbuffers = 1, 
  .modes = {STARPU_RW}
};


void
sparsevect_reorganize(sparsevect_internal_data data, 
		      starpu_data_handle_t handle)
{
  uint32_t **indices_sizes       = data->indices_sizes;
  uint32_t **need_pack           = data->need_pack;
  unsigned nchunks               = data->nchunks;
  uint32_t **memory_state        = data->memory_state;
  /* uint32_t **first_sparse_indice = data->first_sparse_indice; */

  uint32_t ***send_indices   = data->send_indices;
  uint32_t ***recive_indices = data->recive_indices;
  uintptr_t **send_buffers   = data->send_buffers;
  uintptr_t **recive_buffers = data->recive_buffers;

  unsigned node, chunk;
  unsigned nb_nodes = starpu_memory_nodes_get_count();
  for(chunk = 0; chunk < nchunks; chunk++) {
    starpu_data_handle_t child = starpu_data_get_sub_data(handle, 1, chunk);
    unsigned owner = 999999;
    unsigned worker_owner;
    for(node = 0; node < nb_nodes; node++) {
      if ( memory_state[chunk][node] == SPARSEVECT_MEMORY_OWE ) {
	owner = node;

	unsigned worker, nb_workers = starpu_worker_get_count(); 
	for(worker = 0; worker < nb_workers; worker++)
	  if( starpu_worker_get_memory_node(worker) ==  node) {
	    worker_owner = worker;
	    break;
	  }
	break;
      }
    }
    STARPU_ASSERT( owner != 999999 );


    for(node = 0; node < nb_nodes; node++) {
      if (need_pack[chunk][node] != SPARSEVECT_PACK || node == owner ) 
	continue; 
      double packed_timing, continuous_timing;
      struct timeval start, end; 

      struct starpu_task *task_packed = starpu_task_create();
      task_packed->cl=&ddumb_cl;
      task_packed->handles[0] = child;
      task_packed->execute_on_a_specific_worker = 1;
      task_packed->workerid = worker_owner;
      task_packed->synchronous = 1;
      /* PACKED */
      if (starpu_task_submit(task_packed))
	fatal_error(__FUNCTION__,"taks submmition failed!");
      starpu_task_wait_for_all();
      gettimeofday(&start, NULL);
      starpu_data_prefetch_on_node(child, node, 0);
      gettimeofday(&end, NULL);
      packed_timing = (double)(((double)end.tv_sec - (double)start.tv_sec)*1e6 + ((double)end.tv_usec - (double)start.tv_usec));

      /* CONTINUOUS */
      need_pack[chunk][node] = SPARSEVECT_CONTINUOUS; 
      
      struct starpu_task *task_continuous = starpu_task_create();
      task_continuous->cl=&ddumb_cl;
      task_continuous->handles[0] = child;
      task_continuous->execute_on_a_specific_worker = 1;
      task_continuous->workerid = worker_owner;
      task_continuous->synchronous = 1;
      
      if (starpu_task_submit(task_continuous))
	fatal_error(__FUNCTION__,"taks submmition failed!");
      starpu_task_wait_for_all();
      
      gettimeofday(&start, NULL);
      starpu_data_prefetch_on_node(child, node, 0);
      gettimeofday(&end, NULL);
      continuous_timing = (double)(((double)end.tv_sec - (double)start.tv_sec)*1e6 + ((double)end.tv_usec - (double)start.tv_usec));

      /* CHOSE ONE: CONTINUOUS OR PACKED */
      if (continuous_timing < packed_timing ) {
	need_pack[chunk][node] = SPARSEVECT_CONTINUOUS; 
	/* free unused memory */
	starpu_free_on_node(owner, (uintptr_t)send_indices[chunk][node],
			    indices_sizes[chunk][node]*sizeof(***send_indices));
	starpu_free_on_node(owner, (uintptr_t)send_buffers[chunk][node],
			    indices_sizes[chunk][node]*sizeof(***send_indices));

	starpu_free_on_node(node, (uintptr_t)recive_indices[chunk][node],
			    indices_sizes[chunk][node]*sizeof(***recive_indices));
	starpu_free_on_node(node, (uintptr_t)recive_buffers[chunk][node],
			    indices_sizes[chunk][node]*sizeof(***recive_indices));
      } else {
	need_pack[chunk][node] = SPARSEVECT_PACK;
      }
    }
  }

#if defined  SPEAKING_LIKE_GOSSIP_GIRL 
  fprintf(stdout,"AFTER REORDERING THE NEED_PACK\n");
  for(chunk = 0; chunk < nchunks; chunk++){
    print_unsigned_line(indices_sizes[chunk], nb_nodes);
    print_need_pack_line(need_pack[chunk], nb_nodes);
    fprintf(stdout,"\n");
  }
#endif 
}

		      
sparsevect_internal_data
partition_sparsevect(starpu_data_handle_t handle,
		     uint32_t *csrColInd,
		     uint32_t *csrRowPtr,
		     double *csrVal, 
		     unsigned *blockRowPtr,
		     unsigned *worker_list,
		     unsigned nchunks, 
		     size_t elem_size)
{
  sparsevect_internal_data self = calloc(1, sizeof(*self));
  struct starpu_data_filter filter = { };
  struct sparsevect_filter_arg filter_arg;
  unsigned nb_nodes = starpu_memory_nodes_get_count();
  unsigned chunk, node, i, j;
  unsigned node_list[nchunks];

  /* vects  per chunk */
  uint32_t ***indices_cpu_mem;
  uint32_t **indices_sizes;
  uint32_t **need_pack;
  uint32_t **first_sparse_indice;

  /* vects per node */
  uint32_t ***sparsevect_indices_pernode;
  uint32_t **sparsevect_sizes_pernode;
  uint32_t **need_pack_pernode;

  pthread_mutex_t **packing_mutex;
  uint32_t **memory_state;
  struct csr_2D_struct *matrix_2D;

  for(chunk=0; chunk < nchunks; chunk++)
    node_list[chunk] = starpu_worker_get_memory_node(worker_list[chunk]);

  packing_mutex = calloc(nchunks, sizeof(*packing_mutex));
  for( chunk = 0; chunk < nchunks; chunk++) {
    packing_mutex[chunk] = calloc(1, sizeof(**packing_mutex));
    pthread_mutex_init(packing_mutex[chunk], NULL);
  }

  memory_state = calloc(nchunks, sizeof(*memory_state));
  for(chunk = 0; chunk < nchunks; chunk++) {
    memory_state[chunk] = calloc(nb_nodes, sizeof(*memory_state));
    for(node = 0; node < nb_nodes; node++)
      memory_state[chunk][node] = SPARSEVECT_MEMORY_VALID;
    memory_state[chunk][node_list[chunk]] = SPARSEVECT_MEMORY_OWE;
  }

  matrix_2D = csr_2D_index_partition(blockRowPtr, nchunks,
				     csrColInd, csrVal,
				     csrRowPtr);
  
  create_sparse_vectors(matrix_2D, blockRowPtr,
			&need_pack, &indices_sizes, &indices_cpu_mem);

  /* the need_pack  and the other vectors that were calculated
   * in the previous function are giving the relation chunk to chunk. 
   * But on the same node, several  worker can be used(CPU) or even one
   * worker can have several block attributed to them. So that is why 
   * we merge the needed indice vectors. */
  merge_idndice_vectors(nchunks,
  			blockRowPtr,
  			node_list,
  			need_pack,
  			indices_sizes,
  			indices_cpu_mem,
  			&need_pack_pernode,
  			&sparsevect_sizes_pernode,
  			&sparsevect_indices_pernode,
			&first_sparse_indice);

  self->indices_sizes = sparsevect_sizes_pernode;
  self->need_pack     = need_pack_pernode;
  self->nchunks       = nchunks;
  self->memory_mutex  = packing_mutex;
  self->memory_state  = memory_state;
  self->elem_size     = elem_size;
  self->first_sparse_indice = first_sparse_indice;
  
  sparsevect_allocate_device_data(self,
				  sparsevect_indices_pernode,
				  node_list);
  
  filter_arg.internal_data = self;
  filter_arg.blockRowPtr   = blockRowPtr;
  filter_arg.node_list     = node_list;

  filter.filter_func    = sparsevect_index_filter;
  filter.nchildren      = nchunks;
  filter.filter_arg_ptr = (void*) &filter_arg;

  prefetch_on_all_workers(handle,0);
  starpu_data_partition(handle, &filter);

  sparsevect_reorganize(self, handle);

  for(chunk = 0; chunk < nchunks; chunk++) {
    for(node = 0; node < nb_nodes; node++)   {
      if(need_pack_pernode[chunk][node] == SPARSEVECT_PACK)
  	starpu_free(sparsevect_indices_pernode[chunk][node]);
    }
    free(sparsevect_indices_pernode[chunk]);
  }
  free(sparsevect_indices_pernode);

  for(i = 0; i < nchunks; i++) {
    for(j = 0; j < nchunks; j++)  {
      if(need_pack[i][j] == SPARSEVECT_PACK)
  	starpu_free(indices_cpu_mem[i][j]);
    }
    free(indices_cpu_mem[i]);
    free(need_pack[i]);
    free(indices_sizes[i]);
  }
  free(indices_cpu_mem);
  free(need_pack);
  free(indices_sizes);
  csr_2D_destroy(matrix_2D);

  return self;
}
