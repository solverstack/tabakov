#include "common.h"
#include "p_dkernels.h"
#include "dkernels.h"
#include "enum.h"


void ddot_kernel(starpu_data_handle_t v1,
		 starpu_data_handle_t v2,
		 starpu_data_handle_t dot,
		 starpu_data_handle_t dots,
		 int use_pack, 
		 int use_reduction, 
		 int explicit_prefetch,
		 unsigned *workers_list, 
		 starpu_tag_t starting_tag)
{
#ifdef PRINT_WORKER_STATS
  starpu_profiling_init();
#endif
  ddot_kernel_async(v1, v2, dot, dots, use_pack, use_reduction, explicit_prefetch, workers_list, starting_tag);
  starpu_task_wait_for_all ();
#ifdef PRINT_WORKER_STATS
  print_worker_stats();
#endif
}

void ddot_kernel_async(starpu_data_handle_t v1,
		       starpu_data_handle_t v2,
		       starpu_data_handle_t dot,
		       starpu_data_handle_t dots,
		       int use_pack, 
		       int use_reduction, 
		       int explicit_prefetch, 
		       unsigned *workers_list, 
		       starpu_tag_t starting_tag)
{
  if ( use_reduction == MANUAL_REDUCTION )
    pddot_manual_reduction(v1, v2, dot, dots, use_pack, explicit_prefetch,  workers_list, starting_tag);
  else
    pddot(v1, v2, dot, use_pack, use_reduction, workers_list, starting_tag);
}

