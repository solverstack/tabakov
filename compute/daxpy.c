#include "common.h"
#include "p_dkernels.h"
#include "dkernels.h"


void daxpy_kernel(starpu_data_handle_t v1,
		  starpu_data_handle_t v2,
		  starpu_data_handle_t alpha,
		  int use_pack,
		  int explicit_prefetch,
 		  unsigned *workers_list, 
		  starpu_tag_t starting_tag,
		  starpu_tag_t starting_depend_tag)
{
#ifdef PRINT_WORKER_STATS
  starpu_profiling_init();
#endif
  daxpy_kernel_async(v1, v2, alpha, use_pack, explicit_prefetch, workers_list, starting_tag, starting_depend_tag);
  starpu_task_wait_for_all ();
#ifdef PRINT_WORKER_STATS
  print_worker_stats();
#endif
}


void daxpy_kernel_async(starpu_data_handle_t v1,
			starpu_data_handle_t v2,
			starpu_data_handle_t alpha,
			int use_pack,
			int explicit_prefetch,
 			unsigned *workers_list, 
			starpu_tag_t starting_tag,
			starpu_tag_t starting_depend_tag)
{
  if ( starting_depend_tag && !starting_tag ) {
    /* TODO write a warrning fucntion and call it here fot the tag! */
    starting_depend_tag = 0;
  }
  pdaxpy(v1, v2, alpha, use_pack, explicit_prefetch, workers_list, starting_tag, starting_depend_tag);
}
