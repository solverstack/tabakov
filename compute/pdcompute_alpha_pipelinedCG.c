#include "common.h"
#include "codelets_d.h"
#include "data_helpers.h"
#include "p_dkernels.h"

static void 
callback_prefetch(void *arg)
{
  unsigned *nchunks = (unsigned *) arg;
  starpu_data_handle_t alpha = STARPU_TASK_GET_HANDLE(starpu_task_get_current(), 0);
  starpu_data_handle_t minus_alpha = STARPU_TASK_GET_HANDLE(starpu_task_get_current(), 1);
  prefetch_on_all_needed_workers(alpha, *nchunks, 1);
  prefetch_on_all_needed_workers(minus_alpha, *nchunks, 1);
  free(nchunks);
}


void pdcompute_alpha_pipelinedCG(starpu_data_handle_t alpha,
				 starpu_data_handle_t minus_alpha,
				 starpu_data_handle_t new_gamma,
				 starpu_data_handle_t delta,
				 starpu_data_handle_t beta,
				 unsigned nchunks,
				 int explicit_prefetch,
				 unsigned *workers_list, 
				 starpu_tag_t starting_tag)
{
  struct codelet_options options = {
    .tag = starting_tag
  };
  if ( workers_list ) {
    options.execute_on_worker = 1;
    options.worker_id = workers_list[0];
  }
  
  if (explicit_prefetch) { 
    unsigned *arg = malloc(sizeof(*arg));
    *arg = nchunks;
    options.callback_func = callback_prefetch;
    options.callback_arg = (void *) arg;
  }

  task_dcompute_alpha_pipelinedCG(alpha, minus_alpha, new_gamma, delta,
				  beta, &options);
  
  /* if the starting tag is at zero, it will stay at zero */
  if (starting_tag) 
    starting_tag++;
}
