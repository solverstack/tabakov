#include <mkl.h>
#include <math.h>
#include "common.h"
#include "auxiliary.h"
#include "data_helpers.h"
#include "codelets_d.h"
#include "p_dkernels.h"
#include "dkernels.h"
#include "dCG.h"
#include "enum.h"

/*
 *      Conjugate Gradient
 *
 *      Input:
 *              - matrix A
 *              - vector b
 *              - vector x (starting value)
 *              - int i_max, error tolerance eps < 1.
 *      Ouput:
 *              - vector x
 *
 *      Pseudo code:
 *                  
 *             1: r <- b                                     
 *             2: r <- - Ax + r                              
 *             3: p <- r                                     
 *             4: delta_new <- dot(r,r)
 *             5: delta_old <- delta_new
 *             6: {
 *             7:         q <- Ap
 *             8:         alpha <- delta_new / dot(p, q)
 *             9:         x <- x + alpha p
 *            10:         r <- r - alpha q
 *            11:         delta_old <- delta_new
 *            12:         delta_new <- dot(r,r)
 *            13:         beta <- delta_new / delta_old
 *            14:         p <- r + beta p
 *            15: }
 */

void dCG(struct CG_input *input_data, 
	 int two_dim, 
	 int use_reduction, 
	 int explicit_prefetch,
	 int use_pack,
	 int permuting_instruction, 
	 int check, 
	 int debug_mode,
	 unsigned *blockRowPtr, 
	 unsigned **distribution,
	 unsigned *workers_list)
{
  starpu_data_handle_t A_handle = input_data->A;
  starpu_data_handle_t x_handle = input_data->x;
  starpu_data_handle_t b_handle = input_data->b;

  CG_internal_data internal_data;
  unsigned iter, max_iter = input_data->max_iter; 
  unsigned n = input_data->n;
  unsigned nchunks = starpu_data_get_nb_children(x_handle);

  struct timeval start, end;
  double timing = 0.0;
  long nb_flops;
 
  /* check variables */
  double *norm_vect, *norm_Ax_b, *norm_b, **cumulVect, *one, *minus_one;
  starpu_data_handle_t norm_vect_handle, norm_b_handle, norm_Ax_b_handle, cumulVect_handle=NULL, one_handle, minus_one_handle;

  struct starpu_data_filter vect_filter = {
    .filter_func    = indx_filter_func_vector,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };

  /*check*/
  if (check) {
    manage_data(0,
		MALLOC_DATA, &norm_vect,(size_t) n*sizeof(*norm_vect),
		MALLOC_DATA, &norm_Ax_b,(size_t) sizeof(*norm_Ax_b),
		MALLOC_DATA, &norm_b,   (size_t) sizeof(*norm_b),
		MALLOC_DATA, &one,      (size_t) sizeof(*one),
		MALLOC_DATA, &minus_one,(size_t) sizeof(*minus_one),
		END_OF_VAR_LIST);
    assert( norm_vect && norm_Ax_b && norm_b && one && minus_one );
    
    init_dvect(norm_vect, 0, n, 13.0);
    *one = 1.0;
    *minus_one = -1.0;
    *norm_Ax_b =  0.0;
    
    manage_data(0,
		VECTOR_REGISTER, &norm_vect_handle, 0, norm_vect, n, (size_t) sizeof(*norm_vect),
		VECTOR_REGISTER, &norm_Ax_b_handle, 0, norm_Ax_b, 1, (size_t) sizeof(*norm_Ax_b),
		VECTOR_REGISTER, &norm_b_handle,    0, norm_b,    1, (size_t) sizeof(*norm_b),
		VECTOR_REGISTER, &one_handle,       0, one,       1, (size_t) sizeof(*one),
		VECTOR_REGISTER, &minus_one_handle, 0, minus_one, 1, (size_t) sizeof(*minus_one),
		END_OF_VAR_LIST);

    manage_data(0,
		VECTOR_INDEX_PARTITION, norm_vect_handle, blockRowPtr, nchunks,

		CYCLIC_PREFETCH,         norm_vect_handle, 0,
		PREFETCH_ON_ALL_WORKERS, norm_Ax_b_handle, 0,
		PREFETCH_ON_ALL_WORKERS, norm_b_handle, 0,
		PREFETCH_ON_ALL_WORKERS, one_handle, 0,
		PREFETCH_ON_ALL_WORKERS, minus_one_handle, 0,
		END_OF_VAR_LIST);

    if ( use_reduction == STARPU_REDUCTION ) {
      starpu_data_set_reduction_methods(norm_b_handle,    &daccumvect_task_cl, &dbzero_task_cl);
      starpu_data_set_reduction_methods(norm_Ax_b_handle, &daccumvect_task_cl, &dbzero_task_cl);
    }

    if ( use_reduction == MANUAL_REDUCTION )
      setup_cumulVect_manual_reduction(&cumulVect_handle, &cumulVect);
    /* calculate the norm of b */
    if(debug_mode)
      check_vect_handle_all_same(b_handle, &vect_filter, "b", "checking b", 1);
    ddot_kernel(b_handle, b_handle, norm_b_handle, cumulVect_handle, 0, use_reduction, explicit_prefetch, workers_list, 0);
    starpu_data_acquire(norm_b_handle, STARPU_R);

    *norm_b = sqrt(*norm_b);
    printf("norm_b = %e\n",*norm_b);

    starpu_data_release(norm_b_handle);

    manage_data(0,
		UNREGISTER, norm_b_handle,
		END_OF_VAR_LIST);

  }


  pdCG_init_data(&internal_data, n, use_reduction, use_pack);

  
  pdCG_init_algo(input_data, internal_data, two_dim,
		 use_reduction,  explicit_prefetch,
		 use_pack, permuting_instruction, debug_mode,
		 blockRowPtr, distribution, workers_list);
  
  gettimeofday(&start, NULL);
#ifdef PRINT_WORKER_STATS
  starpu_profiling_init();
#endif
  for (iter = 0; iter < max_iter; iter++)
    {
      pdCG_iter_asynch(input_data, internal_data, iter, 
		       two_dim, use_reduction, explicit_prefetch,
		       use_pack,  permuting_instruction, debug_mode,
		       blockRowPtr, distribution, workers_list);
      if (check) {

	starpu_task_wait_for_all ();
	/* norm_vect <- b */
	dcopyvect_kernel_async(norm_vect_handle, b_handle, 0, workers_list, 0);
	if(debug_mode)
	  check_vect_handle_all_same(norm_vect_handle, &vect_filter, "norm_vect", "norm_vect <- b", 1);
	
	/* norm_vect  <-  A x - norm_vect */
	dspmv_kernel_async(A_handle, x_handle, norm_vect_handle, one_handle, minus_one_handle, 0, two_dim,
			   use_reduction, 0, workers_list, distribution, 0);
	if(debug_mode) {
	  check_vect_handle_all_same(x_handle, &vect_filter, "x", "checking x", 1);
	  check_vect_handle_all_same(norm_vect_handle, &vect_filter, "norm_vect", "norm_vect <- A x - norm_vect", 1);
	}
	ddot_kernel_async(norm_vect_handle, norm_vect_handle, norm_Ax_b_handle,
			  cumulVect_handle, 0, use_reduction, explicit_prefetch,
			  workers_list, 0);

	starpu_data_acquire(norm_Ax_b_handle, STARPU_R);

	*norm_Ax_b = sqrt(*norm_Ax_b);
	printf("%u \t %e\n",iter, *norm_Ax_b / *norm_b);

	starpu_data_release(norm_Ax_b_handle);
	starpu_task_wait_for_all ();
      }
    }  
  
  starpu_task_wait_for_all ();
#ifdef PRINT_WORKER_STATS
  print_worker_stats();
#endif

  gettimeofday(&end, NULL);
  timing = (double)(((double)end.tv_sec - (double)start.tv_sec)*1e6 + ((double)end.tv_usec - (double)start.tv_usec));

  if ( check ) {
    fprintf(stdout,"kernel CG: Checking the relative error\n");

    if (*norm_b > EPS_MACHINE)
      fprintf(stdout,"kernel CG: Warrning the solution is  suspicious ! \n");
    else 
      fprintf(stdout,"kernel CG: The solution is  correct ! \n");
  } else { 
    nb_flops = (long) max_iter * (2*input_data->nnz + 9*n) - 2*n;
    printf("kernel CG: nchunks %u timing %f nb_flops %lu Gflops %lf\n", 
	   nchunks, 
	   timing, 
	   nb_flops, 
	   (double) (nb_flops / 1000) / timing);
  }  

  pdCG_cleanUp(internal_data, use_pack, use_reduction, workers_list);

  if (check) {
    manage_data(0,
  		UNPARTITION, norm_vect_handle, 0,
  		UNREGISTER,  norm_Ax_b_handle,
  		UNREGISTER,  norm_vect_handle,
  		UNREGISTER,  one_handle,
  		UNREGISTER,  minus_one_handle,

		FREE_DATA, norm_vect,
		FREE_DATA, norm_Ax_b,
		FREE_DATA, norm_b,
		FREE_DATA, one,
		FREE_DATA, minus_one,
		END_OF_VAR_LIST);

    if ( use_reduction == MANUAL_REDUCTION )
      cleanup_cumulVect_manual_reduction(cumulVect_handle, cumulVect);
  }
}
