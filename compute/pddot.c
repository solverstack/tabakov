#include "common.h"
#include "codelets_d.h"
#include "enum.h"
#include "data_helpers.h"
#include "p_dkernels.h"

void pddot(starpu_data_handle_t v1,
	   starpu_data_handle_t v2,
	   starpu_data_handle_t dot,
	   int use_pack,
	   int use_reduction, 
	   unsigned *workers_list, 
	   starpu_tag_t starting_tag)
{
  unsigned i;
  unsigned nchunks = starpu_data_get_nb_children(v1);  

  struct codelet_options dbzero_options = {
    .tag = 0
  };
  
  if ( workers_list ){ 
    dbzero_options.execute_on_worker = 1;
    dbzero_options.worker_id = workers_list[0];
  }  
  task_dbzero(dot, &dbzero_options);

  for (i = 0; i < nchunks; i++)
    {
      struct codelet_options options = {
	.tag = starting_tag
      };
      if ( workers_list ) {
	options.execute_on_worker = 1; 
	options.worker_id = workers_list[i];
      }
      task_ddot(starpu_data_get_sub_data(v1, 1, i),
		starpu_data_get_sub_data(v2, 1, i),
		dot, use_pack, use_reduction, &options);

      /* if the starting tag is at zero, it will stay at zero */
      if (starting_tag)
	starting_tag++;
    }      

  return;
}
