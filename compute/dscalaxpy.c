#include "common.h"
#include "p_dkernels.h"
#include "dkernels.h"



void dscalaxpy_kernel(starpu_data_handle_t v1,
		      starpu_data_handle_t v2,
		      starpu_data_handle_t alpha,
		      starpu_data_handle_t beta,
		      int use_pack,
		      int explicit_prefetch,
		      unsigned *workers_list, 
		      starpu_tag_t starting_tag)
{
#ifdef PRINT_WORKER_STATS
  starpu_profiling_init();
#endif
  dscalaxpy_kernel_async(v1, v2, alpha, beta, use_pack, explicit_prefetch, workers_list, starting_tag);
  starpu_task_wait_for_all ();
#ifdef PRINT_WORKER_STATS
  print_worker_stats();
#endif
}


void dscalaxpy_kernel_async(starpu_data_handle_t v1,
			    starpu_data_handle_t v2,
			    starpu_data_handle_t alpha,
			    starpu_data_handle_t beta,
			    int use_pack,
			    int explicit_prefetch,
			    unsigned *workers_list, 
			    starpu_tag_t starting_tag)
{
  pdscalaxpy(v1, v2, alpha, beta, use_pack, explicit_prefetch, workers_list, starting_tag);
}
