#include "common.h"
#include "p_dkernels.h"
#include "dkernels.h"


void dspmv_kernel(starpu_data_handle_t A,
		  starpu_data_handle_t x,
		  starpu_data_handle_t y,
		  starpu_data_handle_t alpha,
		  starpu_data_handle_t beta,
		  int no_alpha_beta,
		  int two_dim,
		  int use_reduction,
		  int use_pack,
		  unsigned *workers_list, 
		  unsigned **distribution,
		  starpu_tag_t starting_tag)
{
#ifdef PRINT_WORKER_STATS
  starpu_profiling_init();
#endif
  dspmv_kernel_async(A, x, y, alpha, beta, no_alpha_beta, two_dim,
		     use_reduction, use_pack, workers_list, distribution, starting_tag);
  starpu_task_wait_for_all ();
#ifdef PRINT_WORKER_STATS
  print_worker_stats();
#endif
}


void dspmv_kernel_async(starpu_data_handle_t A, 
			starpu_data_handle_t x,
			starpu_data_handle_t y,
			starpu_data_handle_t alpha,
			starpu_data_handle_t beta,
			int no_alpha_beta,
			int two_dim,
			int use_reduction,
			int use_pack,
			unsigned *workers_list, 
			unsigned **distribution,
			starpu_tag_t starting_tag)
{
  pdspmv(A, x, y, alpha, beta, no_alpha_beta, two_dim, 
	 use_reduction, use_pack, workers_list, distribution, starting_tag);
}
