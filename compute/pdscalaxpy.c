#include "common.h"
#include "codelets_d.h"
#include "data_helpers.h"
#include "p_dkernels.h"

static void 
callback_prefetch(void *arg)
{
  unsigned *nchunks = (unsigned *) arg;
  starpu_data_handle_t alpha = STARPU_TASK_GET_HANDLE(starpu_task_get_current(), 1);
  prefetch_on_all_needed_workers(alpha, *nchunks, 1);
  free(nchunks);
}


void pdscalaxpy(starpu_data_handle_t v1,
		starpu_data_handle_t v2,
		starpu_data_handle_t alpha,
		starpu_data_handle_t beta,
		int use_pack,
		int explicit_prefetch, 
		unsigned *workers_list, 
		starpu_tag_t starting_tag)
{
  unsigned i;
  int nchunks = starpu_data_get_nb_children(v1);  
  
  /* this means that the data is not partitioned  */
  if (nchunks == 0) { 
    struct codelet_options options = {
      .tag = starting_tag
    };
    if ( workers_list ) {
      options.execute_on_worker = 1;
      options.worker_id = workers_list[0];
    }
    if (explicit_prefetch) { 
      unsigned *arg = malloc(sizeof(*arg));
      // uglym very ugly
      *arg = 1;
      options.callback_func = callback_prefetch;
      options.callback_arg = (void *) arg;
    }
    
    task_dscalaxpy(v1,
		   v2,
		   alpha, beta, use_pack, &options);
    
    /* if the starting tag is at zero, it will stay at zero */
    if (starting_tag) 
      starting_tag++;
  } else {  
    for (i = 0; i < nchunks; i++)
      {
	struct codelet_options options = {
	  .tag = starting_tag
	};
	
	if ( workers_list ) {
	  options.execute_on_worker = 1;
	  options.worker_id = workers_list[i];
	}
      if (explicit_prefetch) { 
	unsigned *arg = malloc(sizeof(*arg));
	*arg = nchunks;
	options.callback_func = callback_prefetch;
	options.callback_arg = (void *) arg;
      }
	
	task_dscalaxpy(starpu_data_get_sub_data(v1, 1, i),
		       starpu_data_get_sub_data(v2, 1, i),
		       alpha, beta, use_pack, &options);
	
	
	if (starting_tag) 
	  starting_tag++;
      }
  }
}
