#include "common.h"
#include "codelets_d.h"
#include "p_dkernels.h"
#include "enum.h"

void pdspmv(starpu_data_handle_t A, starpu_data_handle_t x,
	    starpu_data_handle_t y, starpu_data_handle_t alpha,
	    starpu_data_handle_t beta,
	    int no_alpha_beta, 
	    int two_dim,
	    int use_reduction, 
	    int use_pack,
	    unsigned *workers_list,
	    unsigned **distribution, 
	    starpu_tag_t starting_tag)
{
  unsigned i, j;
  unsigned nchunks = (unsigned)starpu_data_get_nb_children(y);  

  for (i = 0; i < nchunks; i++)
    {
      struct codelet_options options = {
	.tag = starting_tag
      };
      if ( workers_list ) {
	options.execute_on_worker = 1;
	options.worker_id = workers_list[i];
      }
      if (two_dim) { 
	task_dspmv_2D(starpu_data_get_sub_data(A, 2, i, i),
		      starpu_data_get_sub_data(x, 1, i),
		      starpu_data_get_sub_data(y, 1, i),
		      alpha, beta, no_alpha_beta,
		      use_reduction, use_pack, 1, &options );
	for(j = 0; j < nchunks; j++) { 
	  if ( distribution[i][j] != SPARSEVECT_NONE && i != j) { 
	    task_dspmv_2D(starpu_data_get_sub_data(A, 2, i, j),
			  starpu_data_get_sub_data(x, 1, j),
			  starpu_data_get_sub_data(y, 1, i),
			  alpha, beta, no_alpha_beta,
 			  use_reduction, use_pack, 0, &options);
	    /* first = 0; */
	  }
	}
      }
      else {  
	task_dspmv_1D(starpu_data_get_sub_data(A, 1, i), 
		      x, 
		      starpu_data_get_sub_data(y, 1, i),
		      alpha, beta, no_alpha_beta, nchunks, 
		      use_pack, &options);
      }

      if (starting_tag) 
	starting_tag++;
    }
}
