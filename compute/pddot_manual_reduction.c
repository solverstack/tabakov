#include "common.h"
#include "data_helpers.h"
#include "codelets_d.h"
#include "data_helpers.h"
#include "p_dkernels.h"
#include "auxiliary.h"
#include "enum.h"


static void 
callback_ddot_prefetch(void *arg)
{
  unsigned *worker = (unsigned *) arg;
  starpu_data_handle_t handle = STARPU_TASK_GET_HANDLE(starpu_task_get_current(), 2);
  starpu_data_prefetch_on_node(handle, starpu_worker_get_memory_node(*worker), 1);
  free(worker);
}

static void 
callback_dsum_prefetch(void *arg)
{
  unsigned *nchunks = (unsigned *) arg;
  starpu_data_handle_t handle = STARPU_TASK_GET_HANDLE(starpu_task_get_current(), 0);
  prefetch_on_all_needed_workers(handle, *nchunks, 1);
  free(nchunks);
}

void pddot_manual_reduction(starpu_data_handle_t v1,
			    starpu_data_handle_t v2,
			    starpu_data_handle_t dot,
			    starpu_data_handle_t dots,
			    int use_pack,
			    int explicit_prefetch, 
			    unsigned *worker_list,
			    starpu_tag_t starting_tag)
{
  if ( !worker_list )
    fatal_error(__FUNCTION__, "manual_reduction works only with static scheduling");
  unsigned i;
  unsigned n_workers = starpu_worker_get_count();
  unsigned nchunks = starpu_data_get_nb_children(v1);
  unsigned last_task_per_worker[n_workers]; 

  /* This array will be used to know when to do the 
     explicit prefetch */
  if (explicit_prefetch && worker_list){
    for(i = 0; i < n_workers; i++) 
      last_task_per_worker[i] = 0;
    for(i = 0; i < nchunks; i++)
      last_task_per_worker[worker_list[i]] = i;
  }
  if( nchunks < n_workers) 
    n_workers = nchunks;
  for ( i = 0; i < n_workers; i++) {
    struct codelet_options dbzero_options = {
      .tag = 0
    };
    if(worker_list) {
      dbzero_options.execute_on_worker = 1;
      dbzero_options.worker_id = i;
    };
    task_dbzero( starpu_data_get_sub_data(dots, 1, i),
		 &dbzero_options);
  }

  for (i = 0; i < nchunks; i++)
    {
      struct codelet_options ddot_options = {
	.tag = starting_tag,
	.execute_on_worker = 1,
	.worker_id = worker_list[i]
      }; 
      
      if (explicit_prefetch  && worker_list &&
	  last_task_per_worker[worker_list[i]] == i)  { 
	unsigned *worker = malloc(sizeof(*worker));
	*worker = worker_list[0];
	ddot_options.callback_func = callback_ddot_prefetch;
	ddot_options.callback_arg  = (void *) worker;
      }

      task_ddot(starpu_data_get_sub_data(v1, 1, i),
		starpu_data_get_sub_data(v2, 1, i),
		starpu_data_get_sub_data(dots, 1, worker_list[i]),
		use_pack, MANUAL_REDUCTION, &ddot_options);


      /* if the starting tag is at zero, it will stay at zero */
      if (starting_tag)
	starting_tag++;
    }

  struct codelet_options dsum_options = {
    .tag = 0
  };

  if (worker_list) {
    dsum_options.execute_on_worker = 1;
    dsum_options.worker_id = worker_list[0];
  }
  if (explicit_prefetch) {
    unsigned *arg = malloc(sizeof(*arg));
    *arg = nchunks;
    dsum_options.callback_func = callback_dsum_prefetch;
    dsum_options.callback_arg = (void *) arg;
  }
  task_dsum_vect(dot, dots, n_workers, &dsum_options);
}


void 
setup_cumulVect_manual_reduction(starpu_data_handle_t *handle,
				 double ***vects)
{
  unsigned n_workers = starpu_worker_get_count();
  manage_data(0,
	      VECTOR_REGISTER_ON_ALL, handle, vects, n_workers, sizeof(***vects),
  	      END_OF_VAR_LIST);

  manage_data(0,
	      PREFETCH_ON_ALL_WORKERS, *handle, 0,
	      VECTOR_BLOCK_PARTITION,  *handle, n_workers, 
  	      END_OF_VAR_LIST);
}

void 
cleanup_cumulVect_manual_reduction(starpu_data_handle_t handle,
				   double **vects)
{
  manage_data(0,
	      UNPARTITION, handle, 0,
	      UNREGISTER, handle, 
	      FREE_DATA_ON_ALL, vects, starpu_worker_get_count() * sizeof(**vects),
  	      END_OF_VAR_LIST);
}
