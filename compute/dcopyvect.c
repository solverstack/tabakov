#include "common.h"
#include "p_dkernels.h"
#include "dkernels.h"



void dcopyvect_kernel(starpu_data_handle_t v1,
		      starpu_data_handle_t v2,
		      int use_pack, 
		      unsigned *workers_list, 
		      starpu_tag_t starting_tag)
{
#ifdef PRINT_WORKER_STATS
  starpu_profiling_init();
#endif
  dcopyvect_kernel_async(v1, v2, use_pack, workers_list, starting_tag);
  starpu_task_wait_for_all ();
#ifdef PRINT_WORKER_STATS
  print_worker_stats();
#endif
}


void dcopyvect_kernel_async(starpu_data_handle_t v1,
			    starpu_data_handle_t v2,
			    int use_pack, 
			    unsigned *workers_list, 
			    starpu_tag_t starting_tag)
{
  pdcopyvect(v1, v2, use_pack, workers_list, starting_tag);
}
