#include "common.h"
#include "auxiliary.h"
#include "data_helpers.h"
#include "dkernels.h"
#include "codelets_d.h"
#include "p_dkernels.h"
#include "sparsevect_interface.h"
#include "enum.h"
#include "dpipelinedCG.h"


struct _pipelinedCG_internal_data {
  double **w;
  sparsevect_internal_data w_sparsevect_data;
  double **u;
  double *m;
  double *z;
  double *s;
  double *p;
  double *r;

  double *alpha; 
  double *beta;
  double *minus_alpha;
  double *delta;
  double *new_gamma;
  double *old_gamma;

  double *one;
  double *minus_one;
  double *zero;
  double **delta_cumul;
  double **new_gamma_cumul;

  unsigned n;

  starpu_data_handle_t w_handle;
  starpu_data_handle_t u_handle;
  starpu_data_handle_t m_handle;
  starpu_data_handle_t z_handle;
  starpu_data_handle_t s_handle;
  starpu_data_handle_t p_handle;
  starpu_data_handle_t r_handle;

  starpu_data_handle_t alpha_handle;
  starpu_data_handle_t beta_handle;
  starpu_data_handle_t minus_alpha_handle;
  starpu_data_handle_t delta_handle;
  starpu_data_handle_t new_gamma_handle;
  starpu_data_handle_t old_gamma_handle;
  starpu_data_handle_t one_handle;
  starpu_data_handle_t minus_one_handle;
  starpu_data_handle_t zero_handle;
  starpu_data_handle_t new_gamma_cumul_handle;
  starpu_data_handle_t delta_cumul_handle;
};
/* static starpu_tag_t base_tag = 1; */


void 
pdpipelinedCG_init_data(pipelinedCG_internal_data *internal_data,
			unsigned n,
			int use_reduction, 
			int use_pack)
{
  pipelinedCG_internal_data self = malloc(sizeof(**internal_data));

  double **w, **u, *m, *z, *s, *p, *r;
  double *alpha, *beta, *minus_alpha, *delta, *new_gamma, *old_gamma;
  double *one, *minus_one, *zero;
  double **delta_cumul, **new_gamma_cumul;

  starpu_data_handle_t w_handle, u_handle, m_handle, z_handle, s_handle, p_handle, r_handle; 
  starpu_data_handle_t alpha_handle, beta_handle, minus_alpha_handle, delta_handle;
  starpu_data_handle_t new_gamma_handle, old_gamma_handle;
  starpu_data_handle_t one_handle, minus_one_handle, zero_handle;
  starpu_data_handle_t delta_cumul_handle = NULL, new_gamma_cumul_handle=NULL;

  manage_data(0,
	      MALLOC_DATA, &m,           (size_t) n*sizeof(*m),
	      MALLOC_DATA, &z,           (size_t) n*sizeof(*z),
	      MALLOC_DATA, &s,           (size_t) n*sizeof(*s),
	      MALLOC_DATA, &p,           (size_t) n*sizeof(*p),
	      MALLOC_DATA, &r,           (size_t) n*sizeof(*r),

	      MALLOC_DATA, &alpha,       (size_t) sizeof(*alpha), 
	      MALLOC_DATA, &beta,        (size_t) sizeof(*beta),
	      MALLOC_DATA, &minus_alpha, (size_t) sizeof(*minus_alpha),
	      MALLOC_DATA, &delta,       (size_t) sizeof(*delta),
	      MALLOC_DATA, &new_gamma,   (size_t) sizeof(*new_gamma),
	      MALLOC_DATA, &old_gamma,   (size_t) sizeof(*old_gamma),

	      MALLOC_DATA, &one,         (size_t) sizeof(*one),
	      MALLOC_DATA, &minus_one,   (size_t) sizeof(*minus_one),
	      MALLOC_DATA, &zero,        (size_t) sizeof(*zero),
	      END_OF_VAR_LIST);

  assert( m && z && s && p && r );
  assert( alpha && beta && minus_alpha && delta && new_gamma && old_gamma);
  assert( one && minus_one && zero);
  
  init_dvect(m, 0, n, 0.0);
  init_dvect(z, 0, n, 0.0);
  init_dvect(s, 0, n, 0.0);
  init_dvect(p, 0, n, 0.0);
  init_dvect(r, 0, n, 0.0);
  
  *alpha = *beta = *minus_alpha = *delta = *new_gamma = *old_gamma = *zero = 0.0;
  *one  = 1.0; *minus_one = -1.0;

  if(use_pack)
    manage_data(0,
		SPARSEVECT_REGISTER_ON_ALL, &w_handle, &w, n, (size_t) sizeof(**w),
		END_OF_VAR_LIST);
  else 
    manage_data(0,
		VECTOR_REGISTER_ON_ALL, &w_handle, &w, n, (size_t) sizeof(**w),
		END_OF_VAR_LIST);
    
  manage_data(0,
	      VECTOR_REGISTER_ON_ALL, &u_handle, &u, n, (size_t) sizeof(**u),
	      END_OF_VAR_LIST);

  manage_data(0,
	      VECTOR_REGISTER, &m_handle,           0, m,           n, (size_t) sizeof(*m),
	      VECTOR_REGISTER, &z_handle,           0, z,           n, (size_t) sizeof(*z),
	      VECTOR_REGISTER, &s_handle,           0, s,           n, (size_t) sizeof(*s),
	      VECTOR_REGISTER, &p_handle,           0, p,           n, (size_t) sizeof(*p),
	      VECTOR_REGISTER, &r_handle,           0, r,           n, (size_t) sizeof(*r),

	      VECTOR_REGISTER, &alpha_handle,       0, alpha,       1, (size_t) sizeof(*alpha),
	      VECTOR_REGISTER, &beta_handle,        0, beta,        1, (size_t) sizeof(*beta),
	      VECTOR_REGISTER, &minus_alpha_handle, 0, minus_alpha, 1, (size_t) sizeof(*minus_alpha),
	      VECTOR_REGISTER, &delta_handle,       0, delta,       1, (size_t) sizeof(*delta),
	      VECTOR_REGISTER, &new_gamma_handle,   0, new_gamma,   1, (size_t) sizeof(*new_gamma),
	      VECTOR_REGISTER, &old_gamma_handle,   0, old_gamma,   1, (size_t) sizeof(*old_gamma),

	      VECTOR_REGISTER, &one_handle,         0, one,         1, (size_t) sizeof(*one),
	      VECTOR_REGISTER, &minus_one_handle,   0, minus_one,   1, (size_t) sizeof(*minus_one),
	      VECTOR_REGISTER, &zero_handle,        0, zero,        1, (size_t) sizeof(*zero),
	      END_OF_VAR_LIST);

  if ( use_reduction == STARPU_REDUCTION ) {
    starpu_data_set_reduction_methods(delta_handle, &daccumvect_task_cl, &dbzero_task_cl);
    starpu_data_set_reduction_methods(new_gamma_handle, &daccumvect_task_cl, &dbzero_task_cl);
  }
  if ( use_reduction == MANUAL_REDUCTION ) {
      setup_cumulVect_manual_reduction(&delta_cumul_handle, &delta_cumul);
      setup_cumulVect_manual_reduction(&new_gamma_cumul_handle, &new_gamma_cumul);
  }
  /* vectors */
  self->w = w;
  self->u = u;
  self->m = m;
  self->z = z;
  self->s = s;
  self->p = p;
  self->r = r;
  /* scalars */
  self->alpha = alpha; 
  self->beta = beta;
  self->minus_alpha = minus_alpha;
  self->delta = delta;
  self->new_gamma = new_gamma;
  self->old_gamma = old_gamma;
  /* helpers */
  self->one = one;
  self->minus_one = minus_one;
  self->zero = zero;
  self->delta_cumul = delta_cumul;
  self->new_gamma_cumul = new_gamma_cumul;

  self->n = n;

  self->w_handle = w_handle;
  self->u_handle = u_handle;
  self->m_handle = m_handle;
  self->z_handle = z_handle;
  self->s_handle = s_handle;
  self->p_handle = p_handle;
  self->r_handle = r_handle;

  self->alpha_handle = alpha_handle;
  self->beta_handle = beta_handle;
  self->minus_alpha_handle = minus_alpha_handle;
  self->delta_handle = delta_handle;
  self->new_gamma_handle = new_gamma_handle;
  self->old_gamma_handle = old_gamma_handle;
  self->one_handle = one_handle;
  self->minus_one_handle = minus_one_handle;
  self->zero_handle = zero_handle;
  self->delta_cumul_handle = delta_cumul_handle;
  self->new_gamma_cumul_handle = new_gamma_cumul_handle;

  *internal_data = self;
}


void pdpipelinedCG_init_algo(struct pipelinedCG_input *input_data,
			     pipelinedCG_internal_data internal_data,
			     int two_dim,
			     int use_reduction,
			     int explicit_prefetch,
			     int use_pack,
			     int permuting_instruction,
			     int debug_mode,
			     unsigned *blockRowPtr,
			     unsigned **distribution,
			     unsigned *workers_list)
{
  starpu_data_handle_t A_handle = input_data->A;
  starpu_data_handle_t x_handle = input_data->x;
  starpu_data_handle_t b_handle = input_data->b;

  starpu_data_handle_t w_handle               = internal_data->w_handle;
  starpu_data_handle_t u_handle               = internal_data->u_handle;
  starpu_data_handle_t m_handle               = internal_data->m_handle;
  starpu_data_handle_t z_handle               = internal_data->z_handle;
  starpu_data_handle_t s_handle               = internal_data->s_handle;
  starpu_data_handle_t p_handle               = internal_data->p_handle;
  starpu_data_handle_t r_handle               = internal_data->r_handle;

  starpu_data_handle_t alpha_handle           = internal_data->alpha_handle;
  starpu_data_handle_t beta_handle            = internal_data->beta_handle;
  starpu_data_handle_t minus_alpha_handle     = internal_data->minus_alpha_handle;
  starpu_data_handle_t delta_handle           = internal_data->delta_handle;
  starpu_data_handle_t new_gamma_handle       = internal_data->new_gamma_handle;
  starpu_data_handle_t old_gamma_handle       = internal_data->old_gamma_handle;
  starpu_data_handle_t one_handle             = internal_data->one_handle;
  starpu_data_handle_t minus_one_handle       = internal_data->minus_one_handle;
  starpu_data_handle_t zero_handle            = internal_data->zero_handle;
  starpu_data_handle_t delta_cumul_handle     = internal_data->delta_cumul_handle;
  starpu_data_handle_t new_gamma_cumul_handle = internal_data->new_gamma_cumul_handle;

  unsigned nchunks = starpu_data_get_nb_children(b_handle);
  
  struct codelet_options codelet_options = {
    .tag = 0
  };
  if (workers_list) {
    codelet_options.execute_on_worker = 1;
    codelet_options.worker_id = workers_list[0];
  }

  struct starpu_data_filter vect_filter = {
    .filter_func    = indx_filter_func_vector,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };
  struct starpu_data_filter matrix_filter = {
    .filter_func    = csr_index_filter,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };

  /* prefetch scalars */
  manage_data(0,
	      PREFETCH_ON_ALL_WORKERS, alpha_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, beta_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, minus_alpha_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, delta_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, new_gamma_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, old_gamma_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, one_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, minus_one_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, zero_handle, 0,
	      END_OF_VAR_LIST);  

  /* partition and prefetch data */
  if (use_pack)
    internal_data->w_sparsevect_data = partition_sparsevect(w_handle,
							    starpu_csr_get_local_colind(A_handle), 
							    starpu_csr_get_local_rowptr(A_handle),
							    (double*) starpu_csr_get_local_nzval(A_handle),
							    blockRowPtr,
    							    workers_list,
							    nchunks,
    							    starpu_csr_get_elemsize(A_handle));
  else
    manage_data(0,
		VECTOR_INDEX_PARTITION, w_handle, blockRowPtr, nchunks,
		END_OF_VAR_LIST);  
  manage_data(0,
	      VECTOR_INDEX_PARTITION, u_handle, blockRowPtr, nchunks,
	      VECTOR_INDEX_PARTITION, m_handle, blockRowPtr, nchunks,
	      VECTOR_INDEX_PARTITION, z_handle, blockRowPtr, nchunks,
	      VECTOR_INDEX_PARTITION, s_handle, blockRowPtr, nchunks,
	      VECTOR_INDEX_PARTITION, p_handle, blockRowPtr, nchunks,
	      VECTOR_INDEX_PARTITION, r_handle, blockRowPtr, nchunks,
	      CYCLIC_PREFETCH, u_handle, 0,
	      CYCLIC_PREFETCH, m_handle, 0,
	      CYCLIC_PREFETCH, z_handle, 0,
	      CYCLIC_PREFETCH, s_handle, 0,
	      CYCLIC_PREFETCH, p_handle, 0,
	      CYCLIC_PREFETCH, r_handle, 0,
	      END_OF_VAR_LIST);  
	      
  if(debug_mode) {
    if (!two_dim) 
      check_matrix_handle_identity(A_handle, &matrix_filter, "A", "checking identity of A:", 1);
    check_vect_handle_all_same(x_handle, &vect_filter, "x", "checking x", 1);
  }

  /* r <- b */
  dcopyvect_kernel_async(r_handle, b_handle, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r <-b", 1);

  /* r  <- - A x + r */
  dspmv_kernel_async(A_handle, x_handle, r_handle, minus_one_handle, one_handle, 0, two_dim,
		     use_reduction, 0, workers_list, distribution, 0);  
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r  <- - A x + r", 1);

  /* u <- r */
  dcopyvect_kernel_async(u_handle, r_handle, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(u_handle, &vect_filter, "u", "u <- r", 1);

  /* w <- A u */
  dspmv_kernel_async(A_handle, u_handle, w_handle, NULL, NULL, 1, two_dim,
		     use_reduction,
		     use_pack ? THIRD_PACKED : NONE_PACKED,
		     workers_list, distribution, 0);  
  if (debug_mode && !use_pack)
    check_vect_handle_all_same(w_handle, &vect_filter, "w", "w  <- A u", 1);

  /* new_gamma = dot(r,u) */
  ddot_kernel_async(r_handle, u_handle, new_gamma_handle, new_gamma_cumul_handle,
		    0, use_reduction, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(new_gamma_handle, NULL, "new_gamma", "new_gamma = dot(r,u)", 0);

  /* delta = dot(w,u) */
  ddot_kernel_async(w_handle, u_handle, delta_handle, delta_cumul_handle,
		    use_pack ? FIRST_PACKED : NONE_PACKED,
		    use_reduction, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(delta_handle, NULL, "delta", "delta <- dot(w,u)", 0);

  /* m <- A w */
  dspmv_kernel_async(A_handle, w_handle, m_handle, NULL, NULL, 1, two_dim,
		     use_reduction,
		     use_pack ? SECOND_PACKED : NONE_PACKED,
		     workers_list, distribution, 0);  
  if (debug_mode)
    check_vect_handle_all_same(m_handle, &vect_filter, "m", "m  <- A w", 1);

  /* beta = 0 */
  task_dbzero(beta_handle, &codelet_options);
  if (debug_mode)
    check_vect_handle_all_same(beta_handle, NULL, "beta", "beta = 0", 0);

  /* alpha = new_gamma / delta */
  pddivide_var(alpha_handle, new_gamma_handle, delta_handle, nchunks,
	       explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(alpha_handle, NULL, "alpha", "alpha = new_gamma / delta", 0);

  /* minus_alpha = - alpha */
  dscalaxpy_kernel_async(alpha_handle, minus_alpha_handle, minus_one_handle, zero_handle,
			 0, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(minus_alpha_handle, NULL, "minus_alpha", "minus_alpha = - alpha", 0);

  /* z <- m + beta z */
  dscalaxpy_kernel_async(m_handle, z_handle, one_handle, beta_handle, 0, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(z_handle, &vect_filter, "z", "z <- m + beta z", 1);

  /* s <- w + beta s */
  dscalaxpy_kernel_async(w_handle, s_handle, one_handle, beta_handle,
			 use_pack ? FIRST_PACKED : NONE_PACKED,
			 explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(s_handle, &vect_filter, "s", "s <- w + beta s", 1);

  /* p <- u + beta p */
  dscalaxpy_kernel_async(u_handle, p_handle, one_handle, beta_handle,
			 0, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(p_handle, &vect_filter, "p", "p <- u + beta p", 1);

  /* x <- alpha p + x */ 
  daxpy_kernel_async(p_handle, x_handle, alpha_handle, 0, 0, workers_list, 0, 0);
  if (debug_mode)
    check_vect_handle_all_same(x_handle, &vect_filter, "x", "x <- alpha p + x", 1);

  /* r <- - alpha s + r */
  daxpy_kernel_async(s_handle, r_handle, minus_alpha_handle, 0, 0, workers_list, 0, 0);
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r <- - alpha s + r", 1);

  /* u <- - alpha s + u */ 
  daxpy_kernel_async(s_handle, u_handle, minus_alpha_handle, 
		     0, 0, workers_list, 0, 0);
  if (debug_mode)
    check_vect_handle_all_same(u_handle, &vect_filter, "u", "u <- - alpha z + u", 1);

  /* w <- - alpha z + w */ 
  daxpy_kernel_async(z_handle, w_handle, minus_alpha_handle, 
		     use_pack ? SECOND_PACKED : NONE_PACKED,
		     0, workers_list, 0, 0);
  if (debug_mode && !use_pack)
    check_vect_handle_all_same(u_handle, &vect_filter, "w", "w <- - alpha z + w", 1);

  starpu_task_wait_for_all();
  prefetch_children_on_all_workers(w_handle, 0);
#ifdef PCG_PRINT_SCALARS
  starpu_data_acquire(alpha_handle, STARPU_R);
  starpu_data_acquire(minus_alpha_handle, STARPU_R);
  starpu_data_acquire(beta_handle, STARPU_R);
  starpu_data_acquire(delta_handle, STARPU_R);
  starpu_data_acquire(new_gamma_handle, STARPU_R);
  starpu_data_acquire(old_gamma_handle, STARPU_R);
  double alpha = *internal_data->alpha;
  double beta = *internal_data->beta;
  double minus_alpha = *internal_data->minus_alpha;
  double delta = *internal_data->delta;
  double new_gamma = *internal_data->new_gamma;
  double old_gamma = *internal_data->old_gamma;
  printf("nb_iter\talpha\t\tbeta\t\tminus_alpha\tdelta\t\tnew_gamma\told_gamma\n");
  printf("1\t%e\t%e\t%e\t%e\t%e\t%e\n", alpha, beta, minus_alpha, delta, new_gamma, old_gamma);
  starpu_data_release(alpha_handle);
  starpu_data_release(minus_alpha_handle);
  starpu_data_release(beta_handle);
  starpu_data_release(delta_handle);
  starpu_data_release(new_gamma_handle);
  starpu_data_release(old_gamma_handle);
#endif
}

void
pdpipelinedCG_iter_asynch(struct pipelinedCG_input *input_data,
			  pipelinedCG_internal_data internal_data,
			  unsigned iter,
			  int two_dim,
			  int use_reduction,
			  int explicit_prefetch,
			  int use_pack,
			  int permuting_instruction,
			  int debug_mode,
			  unsigned *blockRowPtr,
			  unsigned **distribution,
			  unsigned *workers_list)
{
  starpu_data_handle_t A_handle = input_data->A;
  starpu_data_handle_t x_handle = input_data->x;
  starpu_data_handle_t b_handle = input_data->b;

  starpu_data_handle_t w_handle               = internal_data->w_handle;
  starpu_data_handle_t u_handle               = internal_data->u_handle;
  starpu_data_handle_t m_handle               = internal_data->m_handle;
  starpu_data_handle_t z_handle               = internal_data->z_handle;
  starpu_data_handle_t s_handle               = internal_data->s_handle;
  starpu_data_handle_t p_handle               = internal_data->p_handle;
  starpu_data_handle_t r_handle               = internal_data->r_handle;

  starpu_data_handle_t alpha_handle           = internal_data->alpha_handle;
  starpu_data_handle_t beta_handle            = internal_data->beta_handle;
  starpu_data_handle_t minus_alpha_handle     = internal_data->minus_alpha_handle;
  starpu_data_handle_t delta_handle           = internal_data->delta_handle;
  starpu_data_handle_t new_gamma_handle       = internal_data->new_gamma_handle;
  starpu_data_handle_t old_gamma_handle       = internal_data->old_gamma_handle;
  starpu_data_handle_t one_handle             = internal_data->one_handle;
  starpu_data_handle_t delta_cumul_handle     = internal_data->delta_cumul_handle;
  starpu_data_handle_t new_gamma_cumul_handle = internal_data->new_gamma_cumul_handle;

  unsigned nchunks = starpu_data_get_nb_children(b_handle);
  
  struct codelet_options codelet_options = {
    .tag = 0
  };
  if (workers_list) {
    codelet_options.execute_on_worker = 1;
    codelet_options.worker_id = workers_list[0];
  }

  struct starpu_data_filter vect_filter = {
    .filter_func    = indx_filter_func_vector,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };

  /* old_gamma = new_gamma */
  dcopyvect_kernel_async(old_gamma_handle, new_gamma_handle, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(old_gamma_handle, &vect_filter, "old_gamma", "old_gamma = new_gamma", 0);

  /* new_gamma = dot(r,u) */
  ddot_kernel_async(r_handle, u_handle, new_gamma_handle, new_gamma_cumul_handle,
		    0, use_reduction, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(new_gamma_handle, NULL, "new_gamma", "new_gamma = dot(r,u)", 0);

  /* delta = dot(w,u) */
  ddot_kernel_async(w_handle, u_handle, delta_handle, delta_cumul_handle,
		    use_pack ? FIRST_PACKED : NONE_PACKED, 
		    use_reduction, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(delta_handle, NULL, "delta", "delta <- dot(w,u)", 0);

  /* beta = new_gamma / old_gamma */
  pddivide_var(beta_handle, new_gamma_handle, old_gamma_handle, nchunks, explicit_prefetch,
	       workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(new_gamma_handle, NULL, "beta", "beta = new_gamma / old_gamma", 0);

  /* alpha = new_gamma / (delta - beta new_gamma / alpha) */
  pdcompute_alpha_pipelinedCG(alpha_handle, minus_alpha_handle, new_gamma_handle, delta_handle,
			      beta_handle, nchunks, explicit_prefetch, workers_list, 0);
  if (debug_mode) {
    check_vect_handle_all_same(alpha_handle, NULL, "alpha", "alpha = new_gamma / (delta - beta new_gamma / alpha)", 0);
    check_vect_handle_all_same(minus_alpha_handle, NULL, "minus_alpha", "minus_alpha = - alpha", 0);
  }

  /* m <- A w */
  dspmv_kernel_async(A_handle, w_handle, m_handle, NULL, NULL, 1, two_dim,
		     use_reduction,
		     use_pack ? SECOND_PACKED : NONE_PACKED, 
		     workers_list, distribution, 0);  
  if (debug_mode)
    check_vect_handle_all_same(m_handle, &vect_filter, "m", "m  <- A u", 1);

  /* z <- m + beta z */
  dscalaxpy_kernel_async(m_handle, z_handle, one_handle, beta_handle, 0, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(z_handle, &vect_filter, "z", "z <- m + beta z", 1);

  /* s <- w + beta s */
  dscalaxpy_kernel_async(w_handle, s_handle, one_handle, beta_handle,
			 use_pack ? FIRST_PACKED : NONE_PACKED, 
			 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(s_handle, &vect_filter, "s", "s <- w + beta s", 1);

  /* p <- u + beta p */
  dscalaxpy_kernel_async(u_handle, p_handle, one_handle, beta_handle,
			 0, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(p_handle, &vect_filter, "p", "p <- u + beta p", 1);

  /* x <- alpha p + x */ 
  daxpy_kernel_async(p_handle, x_handle, alpha_handle, 0, 0, workers_list, 0, 0);
  if (debug_mode)
    check_vect_handle_all_same(x_handle, &vect_filter, "x", "x <- alpha p + x", 1);
  
  /* r <- - alpha s + r */
  daxpy_kernel_async(s_handle, r_handle, minus_alpha_handle, 0, 0, workers_list, 0, 0);
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r <- - alpha s + r", 1);

  /* WARNING: starpu is not happy when we do explicit prefetch  and not use the data after  */
  if ( iter == ( input_data->max_iter -1 )) 
    explicit_prefetch = 0;
  
  /* u <- - alpha s + u */ 
  daxpy_kernel_async(s_handle, u_handle, minus_alpha_handle, 
		     0, 0, workers_list, 0, 0);
  if (debug_mode && !use_pack)
    check_vect_handle_all_same(u_handle, &vect_filter, "u", "u <- - alpha s + u", 1);

  /* w <- - alpha z + w */ 
  daxpy_kernel_async(z_handle, w_handle, minus_alpha_handle, 
		     use_pack ? SECOND_PACKED : NONE_PACKED, 
		     explicit_prefetch, workers_list, 0, 0);
  if (debug_mode && !use_pack)
    check_vect_handle_all_same(w_handle, &vect_filter, "w", "w <- - alpha s + w", 1);


#ifdef PCG_PRINT_SCALARS
  starpu_task_wait_for_all();
  starpu_data_acquire(alpha_handle, STARPU_R);
  starpu_data_acquire(minus_alpha_handle, STARPU_R);
  starpu_data_acquire(beta_handle, STARPU_R);
  starpu_data_acquire(delta_handle, STARPU_R);
  starpu_data_acquire(new_gamma_handle, STARPU_R);
  starpu_data_acquire(old_gamma_handle, STARPU_R);
  double alpha = *internal_data->alpha;
  double beta = *internal_data->beta;
  double minus_alpha = *internal_data->minus_alpha;
  double delta = *internal_data->delta;
  double new_gamma = *internal_data->new_gamma;
  double old_gamma = *internal_data->old_gamma;
  printf("1\t%e\t%e\t%e\t%e\t%e\t%e\n", alpha, beta, minus_alpha, delta, new_gamma, old_gamma);
  starpu_data_release(alpha_handle);
  starpu_data_release(minus_alpha_handle);
  starpu_data_release(beta_handle);
  starpu_data_release(delta_handle);
  starpu_data_release(new_gamma_handle);
  starpu_data_release(old_gamma_handle);
#endif
}



void 
pdpipelinedCG_cleanUp(pipelinedCG_internal_data internal_data,
		      int use_pack,
		      int use_reduction, 
		      unsigned *workers_list)
{
  manage_data(0,
	      UNPARTITION, internal_data->w_handle, 0,
	      UNPARTITION, internal_data->u_handle, 0,
	      UNPARTITION, internal_data->m_handle, 0,
	      UNPARTITION, internal_data->z_handle, 0,
	      UNPARTITION, internal_data->s_handle, 0,
	      UNPARTITION, internal_data->p_handle, 0,
	      UNPARTITION, internal_data->r_handle, 0,

	      UNREGISTER, internal_data->w_handle,
	      UNREGISTER, internal_data->u_handle,
	      UNREGISTER, internal_data->m_handle,
	      UNREGISTER, internal_data->z_handle,
	      UNREGISTER, internal_data->s_handle,
	      UNREGISTER, internal_data->p_handle,
	      UNREGISTER, internal_data->r_handle,

	      UNREGISTER, internal_data->alpha_handle,
	      UNREGISTER, internal_data->beta_handle,
	      UNREGISTER, internal_data->minus_alpha_handle,
	      UNREGISTER, internal_data->delta_handle,
	      UNREGISTER, internal_data->new_gamma_handle,
	      UNREGISTER, internal_data->old_gamma_handle,

	      UNREGISTER, internal_data->one_handle,
	      UNREGISTER, internal_data->minus_one_handle,
	      UNREGISTER, internal_data->zero_handle,

	      FREE_DATA_ON_ALL, internal_data->w, internal_data->n*sizeof(**internal_data->w),
	      FREE_DATA_ON_ALL, internal_data->u, internal_data->n*sizeof(**internal_data->u),
	      FREE_DATA, internal_data->m,
	      FREE_DATA, internal_data->z,
	      FREE_DATA, internal_data->s,
	      FREE_DATA, internal_data->p,
	      FREE_DATA, internal_data->r,

	      FREE_DATA, internal_data->alpha,
	      FREE_DATA, internal_data->beta,
	      FREE_DATA, internal_data->minus_alpha,
	      FREE_DATA, internal_data->delta,
	      FREE_DATA, internal_data->new_gamma,
	      FREE_DATA, internal_data->old_gamma,

	      FREE_DATA, internal_data->one,
	      FREE_DATA, internal_data->minus_one,
	      FREE_DATA, internal_data->zero,
	      END_OF_VAR_LIST);

  if (use_pack) 
    sparsevect_free_internal_data(internal_data->w_sparsevect_data, 
				  workers_list);

  if ( use_reduction == MANUAL_REDUCTION ) {
    cleanup_cumulVect_manual_reduction(internal_data->delta_cumul_handle, internal_data->delta_cumul);
    cleanup_cumulVect_manual_reduction(internal_data->new_gamma_cumul_handle, internal_data->new_gamma_cumul);
  }
  free(internal_data);
}
