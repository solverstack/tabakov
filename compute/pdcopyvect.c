#include "common.h"
#include "codelets_d.h"
#include "p_dkernels.h"

void pdcopyvect(starpu_data_handle_t v1,
		starpu_data_handle_t v2,
		int use_pack,
		unsigned *workers_list, 
		starpu_tag_t starting_tag)
{
  unsigned i;
  unsigned nchunks = starpu_data_get_nb_children(v1);  
  /* this means that the data is not partitioned  */
  if (nchunks == 0) { 
      struct codelet_options options = {
	.tag = starting_tag
      };
      if ( workers_list ) {
        options.execute_on_worker = 1;
	options.worker_id = workers_list[0];
      }

      task_dcopyvect(v1, v2, use_pack, &options);
      
      /* if the starting tag is at zero, it will stay at zero */
      if (starting_tag) 
	starting_tag++;
  } else {  
    for (i = 0; i < nchunks; i++)
      {
	struct codelet_options options = {
	  .tag = starting_tag
	};
	if ( workers_list ) {
	  options.execute_on_worker = 1;
	  options.worker_id = workers_list[i];
	}
	
	task_dcopyvect(starpu_data_get_sub_data(v1, 1, i),
		       starpu_data_get_sub_data(v2, 1, i),
		       use_pack, &options);
	
	/* if the starting tag is at zero, it will stay at zero */
	if (starting_tag) 
	  starting_tag++;
      }
  }
}
