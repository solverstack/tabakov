#include "common.h"
#include "codelets_d.h"
#include "data_helpers.h"
#include "p_dkernels.h"

static void 
callback_prefetch(void *arg)
{
  unsigned *nchunks = (unsigned *) arg;
  starpu_data_handle_t handle = STARPU_TASK_GET_HANDLE(starpu_task_get_current(), 0);
  prefetch_on_all_needed_workers(handle, *nchunks, 1);
  free(nchunks);
}

void pddivide_var(starpu_data_handle_t res,
		  starpu_data_handle_t a,
		  starpu_data_handle_t b,
		  unsigned nchunks,
		  int explicit_prefetch, 
		  unsigned *workers_list, 
		  starpu_tag_t starting_tag)
{
  struct codelet_options options = {
    .tag = starting_tag
  };
  if ( workers_list ) {
    options.execute_on_worker = 1;
    options.worker_id = workers_list[0];
  }
  
  if (explicit_prefetch) {
    unsigned *arg = malloc(sizeof(*arg));
    *arg = nchunks;
    options.callback_func = callback_prefetch;
    options.callback_arg = (void *) arg;
  }

  task_ddivide_var(res, a, b, &options);
  
  /* if the starting tag is at zero, it will stay at zero */
  if (starting_tag) 
    starting_tag++;
}
