#include "common.h"
#include "auxiliary.h"
#include "data_helpers.h"
#include "dkernels.h"
#include "codelets_d.h"
#include "p_dkernels.h"
#include "sparsevect_interface.h"
#include "enum.h"
#include "dCG.h"


struct _CG_internal_data {
  double **p;
  sparsevect_internal_data p_sparsevect_data;
  double *q;
  double *r;
  double *alpha; 
  double *beta;
  double *minus_alpha;
  double *delta_new;
  double *delta_old;
  double *dot_pq;
  double *one;
  double *minus_one;
  double *zero;
  double **dot_pq_cumul;
  double **delta_new_cumul;

  unsigned n;

  starpu_data_handle_t p_handle;
  starpu_data_handle_t q_handle;
  starpu_data_handle_t r_handle;
  starpu_data_handle_t alpha_handle;
  starpu_data_handle_t beta_handle;
  starpu_data_handle_t minus_alpha_handle;
  starpu_data_handle_t delta_new_handle;
  starpu_data_handle_t delta_old_handle;
  starpu_data_handle_t dot_pq_handle;
  starpu_data_handle_t one_handle;
  starpu_data_handle_t minus_one_handle;
  starpu_data_handle_t zero_handle;
  starpu_data_handle_t dot_pq_cumul_handle;
  starpu_data_handle_t delta_new_cumul_handle;
};
static starpu_tag_t base_tag = 1;


void 
pdCG_init_data(CG_internal_data *internal_data,
	       unsigned n,
	       int use_reduction, 
	       int use_pack)
{
  CG_internal_data self = malloc(sizeof(**internal_data));

  double **p, *q, *r, *alpha, *beta;
  double *minus_alpha, *delta_new, *delta_old;
  double *dot_pq, *one, *minus_one, *zero;
  double **dot_pq_cumul, **delta_new_cumul;

  starpu_data_handle_t p_handle, q_handle, r_handle, alpha_handle;
  starpu_data_handle_t beta_handle, minus_alpha_handle, delta_new_handle;
  starpu_data_handle_t delta_old_handle, dot_pq_handle, one_handle;
  starpu_data_handle_t minus_one_handle, zero_handle;
  starpu_data_handle_t dot_pq_cumul_handle = NULL, delta_new_cumul_handle=NULL;


  manage_data(0,
	      MALLOC_DATA, &q,           (size_t) n*sizeof(*q),
	      MALLOC_DATA, &r,           (size_t) n*sizeof(*r),
	      MALLOC_DATA, &alpha,       (size_t) sizeof(*alpha), 
	      MALLOC_DATA, &beta,        (size_t) sizeof(*beta),
	      MALLOC_DATA, &minus_alpha, (size_t) sizeof(*minus_alpha),
	      MALLOC_DATA, &delta_new,   (size_t) sizeof(*delta_new),
	      MALLOC_DATA, &delta_old,   (size_t) sizeof(*delta_old),
	      MALLOC_DATA, &dot_pq,      (size_t) sizeof(*dot_pq),
	      MALLOC_DATA, &one,         (size_t) sizeof(*one),
	      MALLOC_DATA, &minus_one,   (size_t) sizeof(*minus_one),
	      MALLOC_DATA, &zero,        (size_t) sizeof(*zero),
	      END_OF_VAR_LIST);
  assert( q && r && alpha && beta );
  assert( minus_alpha && delta_new && delta_old);
  assert( dot_pq && one && minus_one && zero);
  
  init_dvect(q, 0, n, 0.0);
  init_dvect(r, 0, n, 0.0);
  
  *alpha = *beta = *minus_alpha = *delta_new = *delta_old = *dot_pq = *zero = 0.0;
  *one  = 1.0; *minus_one = -1.0;

  if(use_pack)
    manage_data(0,
		SPARSEVECT_REGISTER_ON_ALL, &p_handle, &p, n, (size_t) sizeof(**p),
		END_OF_VAR_LIST);
  else 
    manage_data(0,
		VECTOR_REGISTER_ON_ALL, &p_handle, &p, n, (size_t) sizeof(**p),
		END_OF_VAR_LIST);
    
  manage_data(0,
	      VECTOR_REGISTER, &q_handle,           0, q,           n, (size_t) sizeof(*q),
	      VECTOR_REGISTER, &r_handle,           0, r,           n, (size_t) sizeof(*r),
	      VECTOR_REGISTER, &alpha_handle,       0, alpha,       1, (size_t) sizeof(*alpha),
	      VECTOR_REGISTER, &beta_handle,        0, beta,        1, (size_t) sizeof(*beta),
	      VECTOR_REGISTER, &minus_alpha_handle, 0, minus_alpha, 1, (size_t) sizeof(*minus_alpha),
	      VECTOR_REGISTER, &delta_new_handle,   0, delta_new,   1, (size_t) sizeof(*delta_new),
	      VECTOR_REGISTER, &delta_old_handle,   0, delta_old,   1, (size_t) sizeof(*delta_old),
	      VECTOR_REGISTER, &dot_pq_handle,      0, dot_pq,      1, (size_t) sizeof(*dot_pq),
	      VECTOR_REGISTER, &one_handle,         0, one,         1, (size_t) sizeof(*one),
	      VECTOR_REGISTER, &minus_one_handle,   0, minus_one,   1, (size_t) sizeof(*minus_one),
	      VECTOR_REGISTER, &zero_handle,        0, zero,        1, (size_t) sizeof(*zero),
	      END_OF_VAR_LIST);

  if ( use_reduction == STARPU_REDUCTION ) {
    starpu_data_set_reduction_methods(dot_pq_handle, &daccumvect_task_cl, &dbzero_task_cl);
    starpu_data_set_reduction_methods(delta_new_handle, &daccumvect_task_cl, &dbzero_task_cl);
  }
  if ( use_reduction == MANUAL_REDUCTION ) {
      setup_cumulVect_manual_reduction(&dot_pq_cumul_handle, &dot_pq_cumul);
      setup_cumulVect_manual_reduction(&delta_new_cumul_handle, &delta_new_cumul);
  }

  self->p = p;
  self->q = q;
  self->r = r;
  self->alpha = alpha; 
  self->beta = beta;
  self->minus_alpha = minus_alpha;
  self->delta_new = delta_new;
  self->delta_old = delta_old;
  self->dot_pq = dot_pq;
  self->one = one;
  self->minus_one = minus_one;
  self->zero = zero;
  self->dot_pq_cumul = dot_pq_cumul;
  self->delta_new_cumul = delta_new_cumul;

  self->n = n;

  self->p_handle = p_handle;
  self->q_handle = q_handle;
  self->r_handle = r_handle;
  self->alpha_handle = alpha_handle;
  self->beta_handle = beta_handle;
  self->minus_alpha_handle = minus_alpha_handle;
  self->delta_new_handle = delta_new_handle;
  self->delta_old_handle = delta_old_handle;
  self->dot_pq_handle = dot_pq_handle;
  self->one_handle = one_handle;
  self->minus_one_handle = minus_one_handle;
  self->zero_handle = zero_handle;
  self->dot_pq_cumul_handle = dot_pq_cumul_handle;
  self->delta_new_cumul_handle = delta_new_cumul_handle;

  *internal_data = self;
}


void pdCG_init_algo(struct CG_input *input_data,
		    CG_internal_data internal_data,
		    int two_dim,
		    int use_reduction,
		    int explicit_prefetch,
		    int use_pack,
		    int permuting_instruction,
		    int debug_mode,
		    unsigned *blockRowPtr,
		    unsigned **distribution,
		    unsigned *workers_list)
{
  starpu_data_handle_t A_handle = input_data->A;
  starpu_data_handle_t x_handle = input_data->x;
  starpu_data_handle_t b_handle = input_data->b;

  starpu_data_handle_t p_handle               = internal_data->p_handle;
  starpu_data_handle_t q_handle               = internal_data->q_handle;
  starpu_data_handle_t r_handle               = internal_data->r_handle;
  starpu_data_handle_t alpha_handle           = internal_data->alpha_handle;
  starpu_data_handle_t beta_handle            = internal_data->beta_handle;
  starpu_data_handle_t minus_alpha_handle     = internal_data->minus_alpha_handle;
  starpu_data_handle_t delta_new_handle       = internal_data->delta_new_handle;
  starpu_data_handle_t delta_old_handle       = internal_data->delta_old_handle;
  starpu_data_handle_t dot_pq_handle          = internal_data->dot_pq_handle;
  starpu_data_handle_t one_handle             = internal_data->one_handle;
  starpu_data_handle_t minus_one_handle       = internal_data->minus_one_handle;
  starpu_data_handle_t zero_handle            = internal_data->zero_handle;
  starpu_data_handle_t delta_new_cumul_handle = internal_data->delta_new_cumul_handle;

  unsigned nchunks = starpu_data_get_nb_children(b_handle);

  struct codelet_options codelet_options = {
    .tag = 0
  };
  if (workers_list) {
    codelet_options.execute_on_worker = 1;
    codelet_options.worker_id = workers_list[0];
  }

  struct starpu_data_filter vect_filter = {
    .filter_func    = indx_filter_func_vector,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };
  struct starpu_data_filter matrix_filter = {
    .filter_func    = csr_index_filter,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };

  /* prefetch scalars */
  manage_data(0,
	      PREFETCH_ON_ALL_WORKERS, alpha_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, beta_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, minus_alpha_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, delta_old_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, delta_new_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, dot_pq_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, one_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, minus_one_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, zero_handle, 0,
	      END_OF_VAR_LIST);  

  /* partition and prefetch data */
  if (use_pack)
    internal_data->p_sparsevect_data = partition_sparsevect(p_handle,
							    starpu_csr_get_local_colind(A_handle), 
							    starpu_csr_get_local_rowptr(A_handle),
							    (double*) starpu_csr_get_local_nzval(A_handle),
							    blockRowPtr,
    							    workers_list,
							    nchunks,
    							    starpu_csr_get_elemsize(A_handle));
  else
    manage_data(0,
		VECTOR_INDEX_PARTITION, p_handle, blockRowPtr, nchunks,
		END_OF_VAR_LIST);  
  manage_data(0,
	      VECTOR_INDEX_PARTITION, q_handle, blockRowPtr, nchunks,
	      VECTOR_INDEX_PARTITION, r_handle, blockRowPtr, nchunks,
	      CYCLIC_PREFETCH, q_handle, 0,
	      CYCLIC_PREFETCH, r_handle, 0,
	      END_OF_VAR_LIST);  
	      
  if(debug_mode) {
    if (!two_dim) 
      check_matrix_handle_identity(A_handle, &matrix_filter, "A", "checking identity of A:", 1);
    check_vect_handle_all_same(x_handle, &vect_filter, "x", "checking x", 1);
  }

  /* r <- b */
  dcopyvect_kernel_async(r_handle, b_handle, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r <-b", 1);

  /* r  <- - A x + r */
  dspmv_kernel_async(A_handle, x_handle, r_handle, minus_one_handle, one_handle, 0, two_dim,
		     use_reduction, 0, workers_list, distribution, 0);  
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r  <- - A x + r", 1);

  /* p <- r */
  dcopyvect_kernel_async(p_handle, r_handle, use_pack ? FIRST_PACKED : NONE_PACKED, workers_list, 0);
  if (debug_mode && !use_pack) 
    check_vect_handle_all_same(p_handle, &vect_filter, "p", "p <- r", 1);

  /*   delata_new = dot(r,r) */
  ddot_kernel_async(r_handle, r_handle, delta_new_handle, delta_new_cumul_handle, 0, use_reduction, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(delta_new_handle, &vect_filter, "delta_new", "delta_new <- dot(r,r)", 0);

  /* delta_old = delta_new */
  dcopyvect_kernel_async(delta_old_handle, delta_new_handle, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(delta_old_handle, &vect_filter, "delta_old", "delta_old = delta_new", 0);
  starpu_task_wait_for_all();
  prefetch_children_on_all_workers(p_handle, 0);
}

void pdCG_iter_asynch(struct CG_input *input_data,
		      CG_internal_data internal_data,
		      unsigned iter,
		      int two_dim,
		      int use_reduction,
		      int explicit_prefetch,
		      int use_pack,
		      int permuting_instruction,
		      int debug_mode,
		      unsigned *blockRowPtr,
		      unsigned **distribution,
		      unsigned *workers_list)
{
  starpu_data_handle_t A_handle = input_data->A;
  starpu_data_handle_t x_handle = input_data->x;

  starpu_data_handle_t p_handle               = internal_data->p_handle;
  starpu_data_handle_t q_handle               = internal_data->q_handle;
  starpu_data_handle_t r_handle               = internal_data->r_handle;
  starpu_data_handle_t alpha_handle           = internal_data->alpha_handle;
  starpu_data_handle_t beta_handle            = internal_data->beta_handle;
  starpu_data_handle_t minus_alpha_handle     = internal_data->minus_alpha_handle;
  starpu_data_handle_t delta_new_handle       = internal_data->delta_new_handle;
  starpu_data_handle_t delta_old_handle       = internal_data->delta_old_handle;
  starpu_data_handle_t dot_pq_handle          = internal_data->dot_pq_handle;
  starpu_data_handle_t one_handle             = internal_data->one_handle;
  starpu_data_handle_t dot_pq_cumul_handle    = internal_data->dot_pq_cumul_handle;
  starpu_data_handle_t delta_new_cumul_handle = internal_data->delta_new_cumul_handle;

  unsigned nchunks = starpu_data_get_nb_children(x_handle);

  starpu_tag_t axpy_starting_tag = 0;
  starpu_tag_t dot_starting_tag = 0;
  if (permuting_instruction) {
    axpy_starting_tag = base_tag;
    base_tag += nchunks;
    dot_starting_tag = base_tag;
    base_tag += nchunks;
  }

  struct starpu_data_filter vect_filter = {
    .filter_func    = indx_filter_func_vector,
    .nchildren      = nchunks,
    .filter_arg_ptr = blockRowPtr
  };
  

  /* q <- A p */
  dspmv_kernel_async(A_handle, p_handle, q_handle, NULL, NULL, 1, 
		     two_dim, use_reduction, use_pack ? SECOND_PACKED : NONE_PACKED,
		     workers_list, distribution, 0);
  if (debug_mode)
    check_vect_handle_all_same(q_handle, &vect_filter, "q", "q <-A p", 1);
  
  /* dot_pq = dot(p,q) */
  ddot_kernel_async(p_handle, q_handle, dot_pq_handle, dot_pq_cumul_handle,
		    use_pack ? FIRST_PACKED : NONE_PACKED,
		    use_reduction, explicit_prefetch, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(dot_pq_handle, &vect_filter, "dot_pq", "dot_pq = dot(p,q)", 0);
  
  /* alpha = delta_new / dot_pq; minus_alpha = - alpha */
  pdcompute_alpha(dot_pq_handle, delta_new_handle, alpha_handle, minus_alpha_handle, nchunks,
		  explicit_prefetch, workers_list, 0);
  if (debug_mode) {
    check_vect_handle_all_same(alpha_handle, &vect_filter, "alpha", "alpha = delta_new / dot_pq", 0);
    check_vect_handle_all_same(minus_alpha_handle, &vect_filter, "minus_alpha", "minus_alpha = - alpha", 0);
  }

  /* r <- - alpha q + r */ 
  daxpy_kernel_async(q_handle, r_handle, minus_alpha_handle, 0, 0, workers_list, 0, 0);
  if (debug_mode)
    check_vect_handle_all_same(r_handle, &vect_filter, "r", "r <- - alpha q + r", 1);
  
  /* x <- alpha p + x */
  daxpy_kernel_async(p_handle, x_handle, alpha_handle,
		     use_pack ? FIRST_PACKED : NONE_PACKED,
		     0, workers_list, axpy_starting_tag, dot_starting_tag);
  if (debug_mode)
    check_vect_handle_all_same(x_handle, &vect_filter, "x", "x <- alpha p + x", 1);
  
  /* delta_old = delta_new */
  dcopyvect_kernel_async(delta_old_handle, delta_new_handle, 0, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(delta_old_handle, &vect_filter, "delta_old", "delta_old = delta_new", 0);
  
  /* delta_new = dot(r,r) */
  ddot_kernel_async(r_handle, r_handle, delta_new_handle, delta_new_cumul_handle,
		    0, use_reduction, explicit_prefetch, workers_list, dot_starting_tag);
  if (debug_mode)
    check_vect_handle_all_same(delta_new_handle, &vect_filter, "delta_new", "delta_new = dot(r,r)", 0);
  
  /* beta = delta_new / delta_old */
  pddivide_var(beta_handle, delta_new_handle, delta_old_handle, explicit_prefetch, nchunks, workers_list, 0);
  if (debug_mode)
    check_vect_handle_all_same(beta_handle, &vect_filter, "beta", "beta = delta_new / delta_old", 0);

  /* if this is not done starpu is not happy  */
  if ( iter == ( input_data->max_iter -1 )) 
    explicit_prefetch = 0;
  /* p = r + beta p */
  dscalaxpy_kernel_async(r_handle, p_handle, one_handle, beta_handle,
			 use_pack ? SECOND_PACKED : NONE_PACKED,
			 explicit_prefetch, workers_list, 0);
  if (debug_mode && !use_pack)
    check_vect_handle_all_same(p_handle, &vect_filter, "p", "p = r + beta p", 1);
}



void 
pdCG_cleanUp(CG_internal_data internal_data,
	     int use_pack,
	     int use_reduction, 
	     unsigned *workers_list)
{
  manage_data(0,
	      UNPARTITION, internal_data->p_handle, 0,
	      UNPARTITION, internal_data->q_handle, 0,
	      UNPARTITION, internal_data->r_handle, 0,

	      UNREGISTER, internal_data->p_handle,
	      UNREGISTER, internal_data->q_handle,
	      UNREGISTER, internal_data->r_handle,
	      UNREGISTER, internal_data->alpha_handle,
	      UNREGISTER, internal_data->beta_handle,
	      UNREGISTER, internal_data->minus_alpha_handle,
	      UNREGISTER, internal_data->delta_new_handle,
	      UNREGISTER, internal_data->delta_old_handle,
	      UNREGISTER, internal_data->dot_pq_handle,
	      UNREGISTER, internal_data->one_handle,
	      UNREGISTER, internal_data->minus_one_handle,
	      UNREGISTER, internal_data->zero_handle,

	      FREE_DATA_ON_ALL, internal_data->p, internal_data->n*sizeof(**internal_data->p),
	      FREE_DATA, internal_data->q,
	      FREE_DATA, internal_data->r,
	      FREE_DATA, internal_data->alpha,
	      FREE_DATA, internal_data->beta,
	      FREE_DATA, internal_data->minus_alpha,
	      FREE_DATA, internal_data->delta_new,
	      FREE_DATA, internal_data->delta_old,
	      FREE_DATA, internal_data->dot_pq,
	      FREE_DATA, internal_data->one,
	      FREE_DATA, internal_data->minus_one,
	      FREE_DATA, internal_data->zero,
	      END_OF_VAR_LIST);

  if (use_pack) 
    sparsevect_free_internal_data(internal_data->p_sparsevect_data, 
				  workers_list);

  if ( use_reduction == MANUAL_REDUCTION ) {
    cleanup_cumulVect_manual_reduction(internal_data->dot_pq_cumul_handle, internal_data->dot_pq_cumul);
    cleanup_cumulVect_manual_reduction(internal_data->delta_new_cumul_handle, internal_data->delta_new_cumul);
  }
  free(internal_data);
}
