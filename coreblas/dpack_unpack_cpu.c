#include "common.h"

void dpack_vect_cpu(double *dst_vect, double *src_vect, unsigned *ind, unsigned n)
{
  int i;
  for ( i = 0; i < n; i++)
    dst_vect[i] = src_vect[ ind[i] ]; 
}

void dunpack_vect_cpu(double *dst_vect, double *src_vect, unsigned *ind, unsigned n)
{
  int i;
  for ( i = 0; i < n; i++)
    dst_vect[ind[i]] = src_vect[i];
}
