#include "common.h"

#ifdef STARPU_USE_CUDA

#define MAXNBLOCKS      128
#define MAXTHREADSPERBLOCK      256

static __global__ void bzero_vector_device(double *x, unsigned nelems, unsigned nelems_per_thread)
{
  unsigned i;
  unsigned first_i = blockDim.x * blockIdx.x + threadIdx.x;

  for (i = first_i; i < nelems; i += nelems_per_thread)
    x[i] = 0.0;
}

extern "C" void bzero_vector(double *x, unsigned nelems, cudaStream_t stream)
{
  unsigned nblocks = STARPU_MIN(128, nelems);
  unsigned nthread_per_block = STARPU_MIN(MAXTHREADSPERBLOCK, (nelems / nblocks));

  unsigned nelems_per_thread = nelems / (nblocks * nthread_per_block);

  bzero_vector_device<<<nblocks, nthread_per_block, 0, stream>>>(x, nelems, nelems_per_thread);
}


#endif
