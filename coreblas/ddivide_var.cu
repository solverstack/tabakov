#include "common.h"

#ifdef STARPU_USE_CUDA

__global__ void divide_var_device(double *res, double *a, double *b)
{
  *res = *a / *b;
}

extern "C" void divide_var(double *res, double *a, double *b, cudaStream_t stream)
{
  divide_var_device<<<1, 1, 0, stream>>>(res, a, b);
}


#endif
