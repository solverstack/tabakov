#include "common.h"

#ifdef STARPU_USE_CUDA
#define MAXNBLOCKS      128
#define MAXTHREADSPERBLOCK      256

extern "C" __global__ void dpack_vect_device(double *dst_vect, double *src_vect, unsigned *ind, unsigned n, unsigned nelems_per_thread)
{
  int first = blockIdx.x * blockDim.x * nelems_per_thread + threadIdx.x;
  int last_tmp = (blockIdx.x + 1) * blockDim.x * nelems_per_thread;
  int last = last_tmp > n ? n : last_tmp;

  int i;
  for ( i = first; i < last; i+=blockDim.x)
    dst_vect[i] = src_vect[ ind[i] ];
  __syncthreads();
}

extern "C" void dpack_vect(double *dst_vect, double *src_vect, unsigned *ind, unsigned n_sparse, cudaStream_t stream)
{
  unsigned nblocks =  n_sparse > MAXNBLOCKS ? MAXNBLOCKS : n_sparse;
  unsigned nthread_per_block = MAXTHREADSPERBLOCK >  n_sparse / nblocks ?  n_sparse / nblocks : MAXTHREADSPERBLOCK;
  
  unsigned nelems_per_thread = n_sparse / (nblocks * nthread_per_block) + (n_sparse % (nblocks * nthread_per_block) == 0 ? 0 : 1);

  dpack_vect_device<<<nblocks, nthread_per_block, 0, stream>>>(dst_vect, src_vect, ind, n_sparse, nelems_per_thread);
}



extern "C" __global__ void dunpack_device(double *dst_vect, double *src_vect, unsigned *ind, unsigned n, unsigned nelems_per_thread)
{
  int first = blockIdx.x * blockDim.x * nelems_per_thread + threadIdx.x;
  int last_tmp = (blockIdx.x + 1) * blockDim.x * nelems_per_thread;
  int last = last_tmp > n ? n : last_tmp;
  int i;

  for ( i = first; i < last; i+=blockDim.x)
    dst_vect[ind[i]] = src_vect[i];
  __syncthreads();
}

extern "C" void dunpack_vect(double *dst_vect, double *src_vect, unsigned *ind, unsigned n_sparse, cudaStream_t stream)
{
  unsigned nblocks =  n_sparse > MAXNBLOCKS ? MAXNBLOCKS : n_sparse;
  unsigned nthread_per_block = MAXTHREADSPERBLOCK >  n_sparse / nblocks ?  n_sparse / nblocks : MAXTHREADSPERBLOCK;

  unsigned nelems_per_thread = n_sparse / (nblocks * nthread_per_block) + (n_sparse % (nblocks * nthread_per_block) == 0 ? 0 : 1);
  
  dunpack_device<<<nblocks, nthread_per_block, 0, stream>>>(dst_vect, src_vect, ind, n_sparse, nelems_per_thread);
}


#endif
