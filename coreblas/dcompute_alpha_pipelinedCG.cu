#include "common.h"

#ifdef STARPU_USE_CUDA

__global__ void compute_alpha_pipelinedCG_device(double *alpha, double *minus_alpha,
						 double *new_gamma, double *delta,
						 double *beta)
{
  *alpha = *new_gamma /(*delta - *beta * *new_gamma / *alpha);
  *minus_alpha = - *alpha;
}

extern "C"  void compute_alpha_pipelinedCG(double *alpha, double *minus_alpha,
					   double *new_gamma, double *delta,
					   double *beta, cudaStream_t stream)
{
  compute_alpha_pipelinedCG_device<<<1, 1, 0, stream>>>(alpha, minus_alpha, new_gamma, delta, beta);
}


#endif
