C_SOURCES +=  $(addprefix ${topsrcdir}/coreblas/, 	blas.c\
							dpack_unpack_cpu.c) 

CUDA_SOURCES += $(addprefix ${topsrcdir}/coreblas/, 	dsum_vect.cu\
						   	dcompute_alpha.cu\
						   	ddivide_var.cu\
							dpack_unpack.cu\
							dbzero_vect.cu\
							dcompute_alpha_pipelinedCG.cu)
