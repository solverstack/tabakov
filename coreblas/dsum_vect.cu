#include "common.h"

#ifdef STARPU_USE_CUDA
__global__ void sum_device(double *a, double *b, int n)
{
  int i;
  *a = 0;
  for( i = 0; i < n; i++)
    *a += b[i];
}

extern "C" void vect_sum(double *res, double *vect, int n, cudaStream_t stream)
{
  sum_device<<<1, 1, 0, stream>>>(res, vect, n);
}


#endif
