#include "common.h"

#ifdef STARPU_USE_CUDA

__global__ void compute_alpha_device(double *delta_new, double *dotpq, double *alpha, double *minus_alpha)
{
  *alpha = *delta_new / *dotpq;
  *minus_alpha = -*alpha;
}

extern "C"  void compute_alpha(double *delta_new, double *dotpq, double *alpha, double *minus_alpha, cudaStream_t stream)
{
  compute_alpha_device<<<1, 1, 0, stream>>>(delta_new, dotpq, alpha, minus_alpha);
}


#endif
