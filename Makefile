Shell := /bin/bash
topsrcdir= .
includedir=${topsrcdir}/include

C_SOURCES = 
CUDA_SOURCES = 
SOURCES = 
include ${topsrcdir}/Makefile.inc 
SOURCE_DIRS = codelets compute controls coreblas test

include ${topsrcdir}/codelets/rules.mk
include ${topsrcdir}/compute/rules.mk
include ${topsrcdir}/controls/rules.mk
include ${topsrcdir}/coreblas/rules.mk
include ${topsrcdir}/test/rules.mk

SOURCES = $(C_SOURCES) $(CUDA_SOURCES)

C_OBJECTS =  $(patsubst %.c,%.o,$(C_SOURCES))
CUDA_OBJECTS = $(patsubst %.cu,%.o,$(CUDA_SOURCES))
OBJECTS = $(C_OBJECTS) $(CUDA_OBJECTS)

PERF_C_OBJECTS =  $(patsubst %.c,%.o_perf,$(C_SOURCES))
PERF_CUDA_OBJECTS = $(patsubst %.cu,%.o_perf,$(CUDA_SOURCES))
PERF_OBJECTS = $(PERF_C_OBJECTS) $(PERF_CUDA_OBJECTS)

TAG_FILE = TAGS 

FXT_FILES = activity.data  dag.dot  distrib.data  paje.trace

all : $(TARGET)

$(TARGET) : $(OBJECTS) $(PERF_OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(ALL_LDFLAGS)
	$(CC) -o $@_perf $(PERF_OBJECTS) $(PERF_ALL_LDFLAGS)

tags: $(TAG_FILE)

$(TAG_FILE): $(SOURCES)
	etags $(SOURCES) $(includedir)/*.h

%.o: %.c 
	$(CC) $(ALL_CFLAGS) -I ${includedir} -o $@ -c $<

%.o: %.cu 
	$(NVCC) $(ALL_NVCCFLAGS) -I ${includedir} -o $@ -c $<

%.o_perf: %.c 
	$(CC) $(PERF_ALL_CFLAGS) -I ${includedir} -o $@ -c $<

%.o_perf: %.cu 
	$(NVCC) $(PERF_ALL_NVCCFLAGS) -I ${includedir} -o $@ -c $<

clean:
	$(RM) $(OBJECTS) $(PERF_OBJECTS) $(TARGET) $(TARGET)_perf  

clean-fxt:
	for f in $(FXT_FILES); do \
	$(RM)	$$(find -name $$f);\
	done

clean-tags: 
	$(RM) TAGS

cleanall: clean clean-fxt clean-tags

