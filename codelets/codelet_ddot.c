#include "common.h"
#include "auxiliary.h"
#include "cublas_helper.h"
#include "sparsevect_interface.h"
#include "mkl.h"
#include "codelets_d.h"


#ifdef STARPU_USE_CUDA
static void cuda_func_ddot(void *descr[], void *cl_arg)
{
  double *dot= (double *)STARPU_VECTOR_GET_PTR(descr[2]);
  double *tmp = cublas_get_tmp_value();
  double *one = cublas_get_one_value();

  double *v1, *v2;  int n, use_pack;

  starpu_codelet_unpack_args(cl_arg, &use_pack);

  if (use_pack & FIRST_PACKED)
    v1 = (double *)SPARSEVECT_GET_PTR(descr[0]);
  else
    v1 = (double *)STARPU_VECTOR_GET_PTR(descr[0]);

  if (use_pack & SECOND_PACKED) {
    v2 = (double *) SPARSEVECT_GET_PTR(descr[1]);
    n = (int) SPARSEVECT_GET_NX(descr[1]);
  } else {
    v2 = (double *) STARPU_VECTOR_GET_PTR(descr[1]);
    n = (int) STARPU_VECTOR_GET_NX(descr[1]);
  }

#ifndef STARPU_TRUNK_VERSION
  cudaError_t res;
#endif
  cublasStatus_t res_cublas;
  cublasHandle_t handle = cublas_get_handle();
  
  res_cublas = cublasDdot_v2(handle, n, v1, 1, v2, 1, tmp);
  if (res_cublas != CUBLAS_STATUS_SUCCESS ) {
    fatal_error(__FUNCTION__, "cublas failed in dot");
  }

#ifndef STARPU_TRUNK_VERSION
  res = cudaStreamSynchronize(starpu_cuda_get_local_stream());
  
  if(res != cudaSuccess)  {
    fatal_error(__FUNCTION__, "failed in dot");
  }
#endif

  res_cublas = cublasDaxpy_v2(handle, 1, one, tmp, 1, dot, 1);
  if (res_cublas != CUBLAS_STATUS_SUCCESS ) {
    fatal_error(__FUNCTION__, "cublas failed in dot");
  }

#ifndef STARPU_TRUNK_VERSION
  res =   cudaStreamSynchronize(starpu_cuda_get_local_stream());
  if(res != cudaSuccess)  {
    fatal_error(__FUNCTION__, "failed in dot");
  }
#endif
}
#endif

static void cpu_func_ddot(void *descr[], void *cl_arg)
{
  double *dot = (double *)STARPU_VECTOR_GET_PTR(descr[2]);
  double *v1, *v2;  int n, use_pack;

  starpu_codelet_unpack_args(cl_arg, &use_pack);

  if (use_pack & FIRST_PACKED)
    v1 = (double *)SPARSEVECT_GET_PTR(descr[0]);
  else
    v1 = (double *)STARPU_VECTOR_GET_PTR(descr[0]);

  if (use_pack & SECOND_PACKED) {
    v2 = (double *) SPARSEVECT_GET_PTR(descr[1]);
    n = (int) SPARSEVECT_GET_NX(descr[1]);
  } else {
    v2 = (double *) STARPU_VECTOR_GET_PTR(descr[1]);
    n = (int) STARPU_VECTOR_GET_NX(descr[1]);
  }

  double local_dot = 0.0;

  local_dot = cblas_ddot(n, v1, 1, v2, 1);
  *dot = *dot + local_dot;
}

struct starpu_perfmodel ddot_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "task_dot"
};


struct starpu_codelet ddot_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
  | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_ddot, NULL},
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_ddot, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .model = &ddot_task_model,
  .nbuffers = STARPU_VARIABLE_NBUFFERS
};

void
task_ddot (starpu_data_handle_t v1,
	   starpu_data_handle_t v2,
	   starpu_data_handle_t dot,
	   int use_pack,
	   int use_reduction,
	   struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();

  char *arg_buffer;
  size_t arg_buffer_size;
  starpu_codelet_pack_args((void **) &arg_buffer, &arg_buffer_size,
  			   STARPU_VALUE, &use_pack, sizeof(int),
			   0);
  
  task->nbuffers = 3;
  
  task->cl_arg = arg_buffer;
  task->cl_arg_size = arg_buffer_size;
  task->cl=&ddot_task_cl;
  task->handles[0] = v1;
  task->modes[0] = STARPU_R;
  task->handles[1] = v2;
  task->modes[1] = STARPU_R;
  task->handles[2] = dot;
  if ( use_reduction == STARPU_REDUCTION )
    task->modes[2] = STARPU_REDUX;
  else
    task->modes[2] = STARPU_RW;

  update_task(task, options);
  if (starpu_task_submit(task))
    fatal_error(__FUNCTION__,"taks submmition failed!");
}
