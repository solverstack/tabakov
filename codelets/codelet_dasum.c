#include "common.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "my_blas.h"

#ifdef STARPU_USE_CUDA
extern void vect_sum(double *res, double *vect, int n, cudaStream_t stream);

static void cuda_func_dsum_vect(void *descr[], void *cl_arg)
{
  double *res  = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  double *vect = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
  unsigned nchunks;
  starpu_codelet_unpack_args(cl_arg, &nchunks);

  vect_sum(res, vect, nchunks, (cudaStream_t) starpu_cuda_get_local_stream());

#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif
}
#endif

static void cpu_func_dsum_vect(void *descr[], void *cl_arg)
{
  double *res = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  double *vect = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
  unsigned nchunks;
  unsigned i;
  starpu_codelet_unpack_args(cl_arg, &nchunks);

  *res = 0;
  for(i = 0; i < nchunks; i++)
    *res += vect[i];
}

struct starpu_perfmodel dsum_vect_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "dsum_vect_task"
};

struct starpu_codelet dsum_vect_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
 | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_dsum_vect, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_dsum_vect, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .model = &dsum_vect_task_model,
  .nbuffers = STARPU_VARIABLE_NBUFFERS
};


void
task_dsum_vect(starpu_data_handle_t res,
	       starpu_data_handle_t vect, 
	       unsigned nchunks,
	       struct codelet_options *options)
{
  int i;
  struct starpu_task *task = starpu_task_create();
  char *arg_buffer;
  size_t arg_buffer_size;

  starpu_codelet_pack_args((void **) &arg_buffer, &arg_buffer_size,
  			   STARPU_VALUE, &nchunks, sizeof(unsigned),
			   0);
  task->cl_arg = arg_buffer;
  task->cl_arg_size = arg_buffer_size;

  task->cl=&dsum_vect_task_cl;
  task->priority = starpu_sched_get_max_priority();

  task->nbuffers = nchunks + 1;
  task->handles[0] = res;
  task->modes[0] = STARPU_W;

  for(i=0; i < nchunks; i++) {
    task->handles[i+1] = starpu_data_get_sub_data(vect, 1, i);
    task->modes[i+1] = STARPU_R;
  }

  update_task(task, options);
  if (starpu_task_submit(task))
    fatal_error("task_daxpy","taks submmition failed!");
}
