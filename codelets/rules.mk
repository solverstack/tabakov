C_SOURCES += $(addprefix  ${topsrcdir}/codelets/,\
						codelet_daxpy.c\
						codelet_dbzero.c\
						codelet_ddot.c\
						codelet_daccumvector.c\
	 					codelet_dasum.c\
						codelet_dcopyvect.c\
						codelet_dscalaxpy.c\
						codelet_dspmv.c\
						codelet_ddivide_var.c\
						codelet_dcompute_alpha.c\
						codelet_dcompute_alpha_pipelinedCG.c)