#include "common.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "blas.h"


#ifdef STARPU_USE_CUDA
extern void bzero_vector(double *x, unsigned nelems, cudaStream_t stream);

static void cuda_func_dbzero(void *descr[], void *cl_arg)
{
  double *v = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  int n = (int) STARPU_VECTOR_GET_NX(descr[0]);
  cudaStream_t stream = starpu_cuda_get_local_stream();

  cudaMemsetAsync(v, 0, n*sizeof(*v), stream);
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(stream);
#endif
}
#endif

static void cpu_func_dbzero(void *descr[], void *cl_arg)
{
  double *v = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  int n = (int) STARPU_VECTOR_GET_NX(descr[0]);
  
  memset(v, 0, n*sizeof(*v));
}

struct starpu_perfmodel dbzero_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "bzero_vector"
};

struct starpu_codelet dbzero_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
 | STARPU_CUDA
#endif
   ,
  .cpu_funcs = {cpu_func_dbzero, NULL},
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_dbzero, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .nbuffers = 1,
  .model = &dbzero_task_model,
  .modes = {STARPU_W}
};


void
task_dbzero(starpu_data_handle_t v, 	
	    struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();
  task->cl=&dbzero_task_cl;
  task->handles[0] = v;

  update_task(task, options);

  if ( starpu_task_submit(task) ) 
    fatal_error("task_dbzero","taks submmition failed!");
}
