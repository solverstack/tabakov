#include "common.h"
#include "auxiliary.h"
#include "cublas_helper.h"
#include "cusparse_helper.h"
#include "codelets_d.h"
#include "sparsevect_interface.h"
#include <mkl.h>

#ifdef STARPU_USE_CUDA
static void cuda_func_dspmv(void **descr, void *cl_arg)
{
  double *A_csrVal = (double *) STARPU_CSR_GET_NZVAL(descr[0]);
  uint32_t *A_csrRowPtr = STARPU_CSR_GET_ROWPTR(descr[0]);
  uint32_t *A_csrColInd = STARPU_CSR_GET_COLIND(descr[0]);
  int nnz = (int) STARPU_CSR_GET_NNZ(descr[0]);
  int n, m;
  double *one = cublas_get_one_value(), *zero = cublas_get_zero_value();
  double *x; 
  double *y;
  double *alpha, *beta;
  unsigned nchunks;
  int i, two_Dim, use_pack, first, no_alpha_beta;
  double *unused;

  starpu_codelet_unpack_args(cl_arg, &nchunks, &two_Dim, &use_pack, &first, &no_alpha_beta);

  if( use_pack & THIRD_PACKED)  {
    y = (double *) SPARSEVECT_GET_PTR(descr[1]);
    m =(int) SPARSEVECT_GET_NX(descr[1]);
  } else  {
    y = (double *) STARPU_VECTOR_GET_PTR(descr[1]);
    m = (int) STARPU_VECTOR_GET_NX(descr[1]);
  }

  if (use_pack & SECOND_PACKED) { 
    x =  (double *) SPARSEVECT_GET_PTR(descr[2]);
    n = 0;
    if (two_Dim) {
      n = (int) SPARSEVECT_GET_NX(descr[2]);
    } else {
      for(i=0; i < nchunks; i++) 
	n+= (int) SPARSEVECT_GET_NX(descr[i + 2]);
      for(i=1; i < nchunks; i++) 
	unused = (double*)SPARSEVECT_GET_PTR(descr[i+2]);
    }
  } else { 
    x =  (double *) STARPU_VECTOR_GET_PTR(descr[2]);
    n = 0;
    if (two_Dim) {
      n = (int) STARPU_VECTOR_GET_NX(descr[2]);
    } else {
      for(i=0; i < nchunks; i++) 
	n+= (int) STARPU_VECTOR_GET_NX(descr[i + 2]);
    }
  }
  
  if (no_alpha_beta) { 
    alpha = one; 
    if (two_Dim )
      if (first)
	beta = zero;
      else 
	beta = one;
    else
      beta = zero;
  } else  if (two_Dim) {
    alpha = (double *) STARPU_VECTOR_GET_PTR(descr[3]);
    if (first) 
      beta = (double *) STARPU_VECTOR_GET_PTR(descr[4]);
    else
      beta = one;
  } else {
    alpha = (double *) STARPU_VECTOR_GET_PTR(descr[i + 2]);
    beta = (double *) STARPU_VECTOR_GET_PTR(descr[i + 3]);
  }

  if(nnz == 0)
    {
      cublasHandle_t handle = cublas_get_handle();
      cublasDscal_v2(handle, m, beta, y ,1);
      cudaStreamSynchronize(starpu_cuda_get_local_stream());
      return;
    }
  cusparseHandle_t handle = cusparse_get_handle();
  cusparseMatDescr_t descrA = cusparse_get_mat_descr();

  /* gpu_check_vect_all_same(y, m, "y before"); */
  /* char name_[100]; */
  /* sprintf(name_,"x_%unchunks_%u",nchunks, starpu_worker_get_id()); */
  /* gpu_check_vect_all_same(x, n, name_); */
  /* gpu_check_suite(A_csrRowPtr, m+1, "A_csrRowPtr"); */
  /* gpu_check_suite(A_csrColInd, nnz, "A_csrColInd"); */
  /* gpu_check_vect_all_same(A_csrVal, nnz, "A_csrVal"); */
  /* gpu_check_vect_all_same(alpha, 1, "alpha"); */
  /* gpu_check_vect_all_same(beta, 1, "beta"); */
  /* dgpu_write_vect(A_csrVal, nnz, "A_csrVal", starpu_worker_get_id()); */
  /* ugpu_write_vect(A_csrRowPtr, m+1, "A_csrRowPtr", starpu_worker_get_id()); */
  /* ugpu_write_vect(A_csrColInd, nnz, "A_csrColInd", starpu_worker_get_id()); */
  /* char name[100]; */
  /* sprintf(name,"x_%unchunks",nchunks); */
  /* dgpu_write_vect(x, n, name, starpu_worker_get_id()); */
  /* dgpu_write_vect(alpha, 1, "alpha", starpu_worker_get_id()); */
  /* dgpu_write_vect(beta, 1, "beta", starpu_worker_get_id()); */
  /* dgpu_write_vect(y, m, "y_before", starpu_worker_get_id()); */

  cusparseStatus_t stat;
  stat = cusparseDcsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
			m, n, nnz, alpha, descrA, A_csrVal,
			(int *) A_csrRowPtr, (int *) A_csrColInd, x, beta, y);
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif  
  /* dgpu_write_vect(y, m, "y_after", starpu_worker_get_id()); */
  /* double *A1_csrVal1, *x1, *y1, alpha1, beta1; */
  /* unsigned *A1_csrColInd1, *A1_csrRowPtr1; */

  /* A1_csrVal1 = malloc(nnz*sizeof(*A1_csrVal1)); */
  /* A1_csrRowPtr1 = malloc((m+1)*sizeof(*A1_csrRowPtr1)); */
  /* A1_csrColInd1 = malloc(nnz*sizeof(*A1_csrColInd1)); */
  /* x1 = malloc(m*sizeof(*x1)); */
  /* y1 = malloc(m*sizeof(*y1)); */

  /* cudaMemcpy(A1_csrVal1, A_csrVal, nnz*sizeof(*A_csrVal),  cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(A1_csrColInd1, A_csrColInd, nnz*sizeof(*A_csrColInd),  cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(A1_csrRowPtr1, A_csrRowPtr, (m+1)*sizeof(*A_csrRowPtr),  cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(&alpha1, alpha, sizeof(*alpha),  cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(&beta1, beta, sizeof(*beta),  cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(x1, x, m*sizeof(*x),  cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(y1, y, m*sizeof(*y),  cudaMemcpyDeviceToHost); */

  /* mkl_dcsrmv("N", (int *)&m, (int *)&n, &alpha1, "G**C", A1_csrVal1,  */
  /* 	     (int *)A1_csrColInd1, (int *)A1_csrRowPtr1, */
  /* 	     (int *)&A1_csrRowPtr1[1], x1, &beta1, y1); */
  /* check_vect_all_same(y1, m, "cpu y1 after"); */

  if (stat)
    fprintf(stderr,"cuda spmv kernel failed with the error stauts: %d\n",stat);

  if(use_pack & THIRD_PACKED)
    update_memory_state(descr[1]);
}
#endif

void cpu_func_dspmv(void **descr, void *cl_arg)
{
  double *A_csrVal = (double *) STARPU_CSR_GET_NZVAL(descr[0]);
  uint32_t *A_csrRowPtr = STARPU_CSR_GET_ROWPTR(descr[0]);
  uint32_t *A_csrColInd = STARPU_CSR_GET_COLIND(descr[0]);
  int nnz = (int) STARPU_CSR_GET_NNZ(descr[0]);
  int n=0 , m;
  double *x;
  double *y = (double *) STARPU_VECTOR_GET_PTR(descr[1]);
  double *alpha, *beta, one = 1.0, zero = 0.0;
  unsigned nchunks;
  int i, two_Dim, use_pack, first, no_alpha_beta;
  double *unused;

  starpu_codelet_unpack_args(cl_arg, &nchunks, &two_Dim, &use_pack, &first, &no_alpha_beta);

  if( use_pack & THIRD_PACKED)  {
    y = (double *) SPARSEVECT_GET_PTR(descr[1]);
    m =(int) SPARSEVECT_GET_NX(descr[1]);
  } else  {
    y = (double *) STARPU_VECTOR_GET_PTR(descr[1]);
    m = (int) STARPU_VECTOR_GET_NX(descr[1]);
  }

  if (use_pack & SECOND_PACKED) { 
    x =  (double *) SPARSEVECT_GET_PTR(descr[2]);
    n = 0;
    if (two_Dim) {
      n = (int) SPARSEVECT_GET_NX(descr[2]);
    } else {
      for(i=0; i < nchunks; i++) 
	n+= (int) SPARSEVECT_GET_NX(descr[i + 2]);
      for(i=1; i < nchunks; i++) 
	unused = (double*)SPARSEVECT_GET_PTR(descr[i+2]);
    }
  } else { 
    x =  (double *) STARPU_VECTOR_GET_PTR(descr[2]);
    n = 0;
    if (two_Dim) {
      n = (int) STARPU_VECTOR_GET_NX(descr[2]);
    } else {
      for(i=0; i < nchunks; i++) 
	n+= (int) STARPU_VECTOR_GET_NX(descr[i + 2]);
      
      for(i=1; i < nchunks; i++) 
	unused = (double*)STARPU_VECTOR_GET_PTR(descr[i+2]);
    }
  }

  if (no_alpha_beta) { 
    alpha = &one; 
    if (two_Dim )
      if (first)
	beta = &zero;
      else 
	beta = &one;
    else
      beta = &zero;
  } else  if (two_Dim) {
    alpha = (double *) STARPU_VECTOR_GET_PTR(descr[3]);
    if (first) 
      beta = (double *) STARPU_VECTOR_GET_PTR(descr[4]);
    else
      beta = &one;
  } else {
    alpha = (double *) STARPU_VECTOR_GET_PTR(descr[i + 2]);
    beta = (double *) STARPU_VECTOR_GET_PTR(descr[i + 3]);
  }

  if (nnz == 0)
    cblas_dscal(m, *beta, y, 1);
  else
    mkl_dcsrmv( "N", (int *)&m, (int *)&n, alpha, "G**C", A_csrVal, 
	       (int *)A_csrColInd, (int *)A_csrRowPtr,
	       (int *)&A_csrRowPtr[1], x, beta, y);

  if(use_pack & THIRD_PACKED)
    update_memory_state(descr[1]);
}


struct starpu_perfmodel dspmv_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "task_dspmv"
};


struct starpu_codelet dspmv_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
  | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_dspmv, NULL},
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_dspmv, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC},
#endif
#endif
  .nbuffers =  STARPU_VARIABLE_NBUFFERS,
  .model = &dspmv_task_model
};


void
task_dspmv_1D(starpu_data_handle_t A, starpu_data_handle_t x,
	      starpu_data_handle_t y, starpu_data_handle_t alpha,
	      starpu_data_handle_t beta, int no_alpha_beta,
	      unsigned nchunks, int use_pack, struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();
  int two_dim = 0;
  int first = 0;
  unsigned i = 0; 
  char *arg_buffer;
  size_t arg_buffer_size;  

  if (no_alpha_beta)  
    task->nbuffers = nchunks + 2;
  else
    task->nbuffers = nchunks + 4;

  starpu_codelet_pack_args((void **) &arg_buffer, &arg_buffer_size,
			   STARPU_VALUE, &nchunks,  sizeof(unsigned),
			   STARPU_VALUE, &two_dim,  sizeof(int),
			   STARPU_VALUE, &use_pack, sizeof(int),
			   STARPU_VALUE, &first,    sizeof(int),
  			   STARPU_VALUE, &no_alpha_beta, sizeof(int),
			   0);

  task->cl=&dspmv_task_cl;
  task->cl_arg = arg_buffer;
  task->cl_arg_size = arg_buffer_size;
  task->handles[0] = A;
  task->modes[0]   = STARPU_R;
  task->handles[1] = y;
  task->modes[1]   = STARPU_RW;
  for(i = 0; i < nchunks; i++) {
    task->handles[i+2] = starpu_data_get_sub_data(x, 1, i);
    task->modes[i+2]  = STARPU_R;
  }
  if (!no_alpha_beta) {
    task->handles[i+2] = alpha;
    task->modes[i+2]   = STARPU_R;
    task->handles[i+3] = beta;
    task->modes[i+3]   = STARPU_R;
  }

  update_task(task, options);
  
  if (starpu_task_submit(task))
    fatal_error(__FUNCTION__,"taks submmition failed!");
}


void
task_dspmv_2D(starpu_data_handle_t A, starpu_data_handle_t x,
	      starpu_data_handle_t y, starpu_data_handle_t alpha, 
	      starpu_data_handle_t beta, int no_alpha_beta,
	      int use_reduction, int use_pack, int first,
	      struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();
  unsigned nchunks=0, two_dim=1;
  char *arg_buffer;
  size_t arg_buffer_size;

  starpu_codelet_pack_args((void **) &arg_buffer, &arg_buffer_size,
  			   STARPU_VALUE, &nchunks,       sizeof(unsigned),
  			   STARPU_VALUE, &two_dim,       sizeof(int),
  			   STARPU_VALUE, &use_pack,      sizeof(int),
  			   STARPU_VALUE, &first,         sizeof(int),
  			   STARPU_VALUE, &no_alpha_beta, sizeof(int),
  			   0);

  if (no_alpha_beta) 
    task->nbuffers = 3;
  else
    task->nbuffers = 5;
  task->cl_arg = arg_buffer;
  task->cl_arg_size = arg_buffer_size;
  task->cl=&dspmv_task_cl;
  task->handles[0] = A;
  task->modes[0]   = STARPU_R;
  task->handles[1] = y;
  if (first)
    task->modes[1]   = STARPU_RW;
  else
    task->modes[1]   = STARPU_RW | STARPU_COMMUTE;
  task->handles[2] = x;
  task->modes[2]   = STARPU_R;
  if (!no_alpha_beta) {
    task->handles[3] = alpha;
    task->modes[3]   = STARPU_R;
    task->handles[4] = beta;
    task->modes[4]   = STARPU_R;
  }
  update_task(task, options);
  
  if (starpu_task_submit(task))
    fatal_error(__FUNCTION__,"taks submmition failed!");
}
