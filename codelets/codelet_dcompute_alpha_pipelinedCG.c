#include "common.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "my_blas.h"

#ifdef STARPU_USE_CUDA
extern void compute_alpha_pipelinedCG(double *alpha, double *minus_alpha, double *new_gamma,
				      double *delta, double *beta, cudaStream_t stream);

static void cuda_func_dcompute_alpha_pipelinedCG(void *descr[], void *cl_arg)
{
  double *alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
  double *minus_alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[1]);
  double *new_gamma = (double *)STARPU_VARIABLE_GET_PTR(descr[2]);
  double *delta = (double *)STARPU_VARIABLE_GET_PTR(descr[3]);
  double *beta = (double *)STARPU_VARIABLE_GET_PTR(descr[4]);

  compute_alpha_pipelinedCG(alpha, minus_alpha, new_gamma, delta, beta, starpu_cuda_get_local_stream());
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif
}
#endif

static void cpu_func_dcompute_alpha_pipelinedCG(void *descr[], void *cl_arg)
{
  double *alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
  double *minus_alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[1]);
  double *new_gamma = (double *)STARPU_VARIABLE_GET_PTR(descr[2]);
  double *delta = (double *)STARPU_VARIABLE_GET_PTR(descr[3]);
  double *beta = (double *)STARPU_VARIABLE_GET_PTR(descr[4]);

  *alpha = *new_gamma /(*delta - *beta * *new_gamma / *alpha);
  *minus_alpha = -*alpha;
}

struct starpu_perfmodel dcompute_alpha_pipelinedCG_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "dcompute_alpha_pipelinedCG_task"
};

struct starpu_codelet dcompute_alpha_pipelinedCG_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
 | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_dcompute_alpha_pipelinedCG, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_dcompute_alpha_pipelinedCG, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .model = &dcompute_alpha_pipelinedCG_task_model,
  .nbuffers = 5,
  .modes = {STARPU_RW, STARPU_W, STARPU_R, STARPU_R, STARPU_R}
};


void
task_dcompute_alpha_pipelinedCG(starpu_data_handle_t alpha,
				starpu_data_handle_t minus_alpha,
				starpu_data_handle_t new_gamma,
				starpu_data_handle_t delta,
				starpu_data_handle_t beta,
				struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();
  task->cl=&dcompute_alpha_pipelinedCG_task_cl;
  task->priority = starpu_sched_get_max_priority();

  task->handles[0] = alpha;
  task->handles[1] = minus_alpha;
  task->handles[2] = new_gamma;
  task->handles[3] = delta;
  task->handles[4] = beta;

  update_task(task, options);

  if (starpu_task_submit(task))
    fatal_error("task_daxpy","taks submmition failed!");
}
