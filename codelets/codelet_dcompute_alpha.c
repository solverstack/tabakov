#include "common.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "my_blas.h"

#ifdef STARPU_USE_CUDA
extern void compute_alpha(double *delta_new, double *dotpq, double *alpha, double *minus_alpha, cudaStream_t stream);

static void cuda_func_dcompute_alpha(void *descr[], void *cl_arg)
{
  double *dotpq = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
  double *delta_new = (double *)STARPU_VARIABLE_GET_PTR(descr[1]);
  double *alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[2]);
  double *minus_alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[3]);

  compute_alpha(delta_new, dotpq, alpha, minus_alpha, starpu_cuda_get_local_stream());
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif
}
#endif

static void cpu_func_dcompute_alpha(void *descr[], void *cl_arg)
{
  double *dotpq = (double *)STARPU_VARIABLE_GET_PTR(descr[0]);
  double *delta_new = (double *)STARPU_VARIABLE_GET_PTR(descr[1]);
  double *alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[2]);
  double *minus_alpha = (double *)STARPU_VARIABLE_GET_PTR(descr[3]);

  *alpha = *delta_new / *dotpq;
  *minus_alpha = - *alpha;
}

struct starpu_perfmodel dcompute_alpha_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "dcompute_alpha_task"
};

struct starpu_codelet dcompute_alpha_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
 | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_dcompute_alpha, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_dcompute_alpha, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .model = &dcompute_alpha_task_model,
  .nbuffers = 4,
  .modes = {STARPU_R, STARPU_R, STARPU_W, STARPU_W}
};


void
task_dcompute_alpha(starpu_data_handle_t dotpq,
		    starpu_data_handle_t delta_new,
		    starpu_data_handle_t alpha,
		    starpu_data_handle_t minus_alpha,
		    struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();
  task->cl=&dcompute_alpha_task_cl;
  task->priority = starpu_sched_get_max_priority();

  task->handles[0] = dotpq;
  task->handles[1] = delta_new;
  task->handles[2] = alpha;
  task->handles[3] = minus_alpha;

  update_task(task, options);

  if (starpu_task_submit(task))
    fatal_error("task_daxpy","taks submmition failed!");
}
