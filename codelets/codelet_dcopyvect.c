#include "common.h"
#include "auxiliary.h"
#include "cublas_helper.h"
#include "sparsevect_interface.h"
#include "mkl.h"
#include "codelets_d.h"


#ifdef STARPU_USE_CUDA
static void cuda_func_dcopyvect(void *descr[], void *cl_arg)
{
  double *dst, *src;
  int nx, use_pack;
  size_t elemsize;
  starpu_codelet_unpack_args(cl_arg, &use_pack);

  if (use_pack & FIRST_PACKED)
    dst = (double *)SPARSEVECT_GET_PTR(descr[0]);
  else 
    dst = (double *)STARPU_VECTOR_GET_PTR(descr[0]);

  if (use_pack & SECOND_PACKED) {
    src = (double *)SPARSEVECT_GET_PTR(descr[1]);
    nx  = SPARSEVECT_GET_NX(descr[1]);
    elemsize = SPARSEVECT_GET_ELEMSIZE(descr[1]);
  } else {
    src = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
    nx  = STARPU_VECTOR_GET_NX(descr[1]);
    elemsize = STARPU_VECTOR_GET_ELEMSIZE(descr[1]);
  }


  cudaMemcpyAsync(dst, src, nx*elemsize, cudaMemcpyDeviceToDevice, (cudaStream_t) starpu_cuda_get_local_stream());
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize((cudaStream_t) starpu_cuda_get_local_stream());
#endif
  if(use_pack & FIRST_PACKED)
    update_memory_state(descr[0]);
}
#endif

static void cpu_func_dcopyvect(void *descr[], void *cl_arg)
{
  double *dst, *src;
  int nx, use_pack;
  size_t elemsize;
  starpu_codelet_unpack_args(cl_arg, &use_pack);

  if (use_pack & FIRST_PACKED)
    dst = (double *)SPARSEVECT_GET_PTR(descr[0]);
  else 
    dst = (double *)STARPU_VECTOR_GET_PTR(descr[0]);

  if (use_pack & SECOND_PACKED) {
    src = (double *)SPARSEVECT_GET_PTR(descr[1]);
    nx  = SPARSEVECT_GET_NX(descr[1]);
    elemsize = SPARSEVECT_GET_ELEMSIZE(descr[1]);
  } else {
    src = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
    nx  = STARPU_VECTOR_GET_NX(descr[1]);
    elemsize = STARPU_VECTOR_GET_ELEMSIZE(descr[1]);
  }

  memcpy(dst, src, nx*elemsize);
  if(use_pack & FIRST_PACKED)
    update_memory_state(descr[0]);
}


struct starpu_perfmodel dcopyvect_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "task_dcopyvect"
};


struct starpu_codelet dcopyvect_task_cl = {
  .where = STARPU_CPU 
#ifdef STARPU_USE_CUDA
  | STARPU_CUDA
#endif 
  ,
  .cpu_func = cpu_func_dcopyvect,
#ifdef STARPU_USE_CUDA
  .cuda_func = cuda_func_dcopyvect,
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .nbuffers = 2,
  .model = &dcopyvect_task_model,
  .modes = {STARPU_W, STARPU_R}
};


void
task_dcopyvect(starpu_data_handle_t v1,
	       starpu_data_handle_t v2,
	       int use_pack, 
	       struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();

  char *arg_buffer;
  size_t arg_buffer_size;
  starpu_codelet_pack_args((void **) &arg_buffer, &arg_buffer_size,
  			   STARPU_VALUE, &use_pack, sizeof(int),
			   0);

  task->cl_arg = arg_buffer;
  task->cl_arg_size = arg_buffer_size;
  task->cl=&dcopyvect_task_cl;
  task->handles[0] = v1;
  task->handles[1] = v2;

  update_task(task, options);
  
  if (starpu_task_submit(task))
    fatal_error("task_dcopyvect","taks submmition failed!");
}
