#include "common.h"
#include "auxiliary.h"
#include "cublas_helper.h"
#include "sparsevect_interface.h"
#include "mkl.h"
#include "codelets_d.h"


#ifdef STARPU_USE_CUDA
static void cuda_func_daxpy(void *descr[], void *cl_arg)
{
  double *alpha =(double *)STARPU_VECTOR_GET_PTR(descr[2]);
  double *v1, *v2;  int n, use_pack;
  starpu_codelet_unpack_args(cl_arg, &use_pack);

  if (use_pack & FIRST_PACKED)
    v1 = (double *)SPARSEVECT_GET_PTR(descr[0]);
  else 
    v1 = (double *)STARPU_VECTOR_GET_PTR(descr[0]);

  if (use_pack & SECOND_PACKED) {
    v2 = (double *)SPARSEVECT_GET_PTR(descr[1]);
    n = SPARSEVECT_GET_NX(descr[1]);
  } else {
    v2 = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
    n = (int) STARPU_VECTOR_GET_NX(descr[1]);
  }

  cublasHandle_t handle = cublas_get_handle();
  cublasDaxpy_v2(handle, n, alpha, v1, 1, v2, 1);
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif

  if(use_pack & SECOND_PACKED)
    update_memory_state(descr[1]);
}
#endif

static void cpu_func_daxpy(void *descr[], void *cl_arg)
{
  double *alpha =(double *)STARPU_VECTOR_GET_PTR(descr[2]);
  double *v1, *v2;  int use_pack, n;
  starpu_codelet_unpack_args(cl_arg, &use_pack);

  if (use_pack & FIRST_PACKED)
    v1 = (double *)SPARSEVECT_GET_PTR(descr[0]);
  else 
    v1 = (double *)STARPU_VECTOR_GET_PTR(descr[0]);

  if (use_pack & SECOND_PACKED) {
    v2 = (double *)SPARSEVECT_GET_PTR(descr[1]);
    n = SPARSEVECT_GET_NX(descr[1]);
  } else {
    v2 = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
    n = (int) STARPU_VECTOR_GET_NX(descr[1]);
  }

  cblas_daxpy(n, *alpha, v1, 1, v2, 1);

  if(use_pack & SECOND_PACKED)
    update_memory_state(descr[1]);
}

struct starpu_perfmodel daxpy_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "task_daxpy"
};

struct starpu_codelet daxpy_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
  | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_daxpy, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_daxpy, NULL}, 
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {STARPU_CUDA_ASYNC}, 
#endif
#endif
  .nbuffers = 3,
  .modes = { STARPU_R, STARPU_RW, STARPU_R },
  .model = &daxpy_task_model
};


void
task_daxpy(starpu_data_handle_t v1,
	   starpu_data_handle_t v2,
	   starpu_data_handle_t alpha,
	   int use_pack, 
	   struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();

  char *arg_buffer;
  size_t arg_buffer_size;
  starpu_codelet_pack_args((void **) &arg_buffer, &arg_buffer_size,
  			   STARPU_VALUE, &use_pack, sizeof(int),
			   0);
  task->cl_arg = arg_buffer;
  task->cl_arg_size = arg_buffer_size;

  task->cl=&daxpy_task_cl;
  task->handles[0] = v1;
  task->handles[1] = v2;
  task->handles[2] = alpha;
  
  update_task(task, options);

  if (starpu_task_submit(task))
    fatal_error("task_daxpy","taks submmition failed!");
}
