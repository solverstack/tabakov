#include "common.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "my_blas.h"

#ifdef STARPU_USE_CUDA
extern void divide_var(double *res, double *a, double *b, cudaStream_t stream);

static void cuda_func_ddivide_var(void *descr[], void *cl_arg)
{
  double *res = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  double *a = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
  double *b = (double *)STARPU_VECTOR_GET_PTR(descr[2]);

  divide_var(res, a, b, starpu_cuda_get_local_stream());
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif
}
#endif

static void cpu_func_ddivide_var(void *descr[], void *cl_arg)
{
  double *res = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  double *a = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
  double *b = (double *)STARPU_VECTOR_GET_PTR(descr[2]);

  *res = *a / *b;
}
struct starpu_perfmodel ddivide_var_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "ddivide_var_task"
};

struct starpu_codelet ddivide_var_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
 | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_ddivide_var, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_ddivide_var, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .model = &ddivide_var_task_model,
  .nbuffers = 3,
  .modes = {STARPU_W, STARPU_R, STARPU_R}
};


void
task_ddivide_var(starpu_data_handle_t res,
		 starpu_data_handle_t a,
		 starpu_data_handle_t b,
		 struct codelet_options *options)
{
  struct starpu_task *task = starpu_task_create();
  task->cl=&ddivide_var_task_cl;
  
  task->priority = starpu_sched_get_max_priority();
  task->handles[0] = res;
  task->handles[1] = a;
  task->handles[2] = b;

  update_task(task, options);

  if (starpu_task_submit(task))
    fatal_error("task_daxpy","taks submmition failed!");
}
