#include "common.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "cublas_helper.h"
#include "mkl.h"

#ifdef STARPU_USE_CUDA
static void cuda_func_daccumulate_vector(void *descr[], void *cl_arg)
{
  double *v_dst = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  double *v_src = (double *)STARPU_VECTOR_GET_PTR(descr[1]);
  int n = (int) STARPU_VECTOR_GET_NX(descr[0]);

  cublasHandle_t handle = cublas_get_handle();
  double *one = cublas_get_one_value();

  cublasDaxpy_v2(handle, n, one, v_src, 1, v_dst, 1);
#ifndef STARPU_TRUNK_VERSION
  cudaStreamSynchronize(starpu_cuda_get_local_stream());
#endif
}

#endif

static void cpu_func_daccumulate_vector(void *descr[], void *cl_arg)
{
  double *v_dst = (double *)STARPU_VECTOR_GET_PTR(descr[0]);
  double *v_src = (double *)STARPU_VECTOR_GET_PTR(descr[1]);

  int n = (int) STARPU_VECTOR_GET_NX(descr[0]);
  cblas_daxpy(n, 1.0, v_src, 1, v_dst, 1);
}

struct starpu_perfmodel daccumvect_task_model = {
  .type = STARPU_HISTORY_BASED,
  .symbol = "daccumvect_task"
};

struct starpu_codelet daccumvect_task_cl = {
  .where = STARPU_CPU
#ifdef STARPU_USE_CUDA
  | STARPU_CUDA
#endif
  ,
  .cpu_funcs = {cpu_func_daccumulate_vector, NULL}, 
#ifdef STARPU_USE_CUDA
  .cuda_funcs = {cuda_func_daccumulate_vector, NULL},
#ifdef STARPU_TRUNK_VERSION
  .cuda_flags = {(char)STARPU_CUDA_ASYNC}, 
#endif
#endif
  .nbuffers = 2,
  .model = &daccumvect_task_model,
  .modes = {STARPU_RW, STARPU_R}
};
