#include <sys/time.h>
#include <mkl.h>
#include "common.h"
#include "test.h"
#include "dpipelinedCG.h"
#include "data_helpers.h"
#include "auxiliary.h"
#include "csr_load_matrix.h"

static double **x, *b, *vals;
static starpu_data_handle_t x_handle, b_handle, A_handle;
static unsigned *rowPtr, *colInd; 
static unsigned nnz, m, n;

int dpipelinedCG_test_init_function(test_struct parameters) 
{
  csr_load_matrix(parameters->matrix_file, 
		  &vals, &colInd, &nnz, 
		  &rowPtr, &m, &n);
  parameters->n = n;
  
  manage_data(0,
	      MALLOC_DATA ,&b, (size_t) n*sizeof(*b), 
	      END_OF_VAR_LIST);

  init_dvect(b, 0, n, 1.0);  

  manage_data(0,
	      CSR_REGISTER,    &A_handle,     0, nnz,   m, (uintptr_t)vals, colInd, rowPtr, sizeof(*vals),
	      VECTOR_REGISTER_ON_ALL, &x_handle, &x,     n, sizeof(**x),
	      VECTOR_REGISTER, &b_handle,     0,  b,     n, sizeof(*b), 
	      END_OF_VAR_LIST);

  return 0;
}


int 
dpipelinedCG_test_compute_function(test_struct parameters) 
{
  struct pipelinedCG_input input_data;
  unsigned nchunks = parameters->current_nchunks, chunk;
  unsigned workers_list[nchunks];
  unsigned blockRowPtr[nchunks+1];
  int two_dim = parameters->two_dim;
  unsigned **distribution = NULL;
  struct csr_2D_struct *matrix_2D;

  create_blocRowPtr(rowPtr, m, nnz, nchunks, parameters->ratio_CPU_GPU, blockRowPtr);
  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);

  if( two_dim) {
    matrix_2D = csr_2D_index_partition(blockRowPtr, nchunks, colInd, vals, rowPtr);
    distribution = create_distribution_vector(matrix_2D, nchunks);
    manage_data(0,
		CSR_2D_INDEX_PARTITION, A_handle, matrix_2D, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH_2D, A_handle, distribution, 0,
		END_OF_VAR_LIST);
  } else { 
    manage_data(0,
		CSR_INDEX_PARTITION, A_handle, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH, A_handle, 0,
		END_OF_VAR_LIST);
  }

  manage_data(0,
	      PREFETCH_ON_ALL_WORKERS, x_handle,  0,
	      VECTOR_INDEX_PARTITION,  x_handle, blockRowPtr, nchunks, 
	      VECTOR_INDEX_PARTITION,  b_handle, blockRowPtr, nchunks, 
	      CYCLIC_PREFETCH, b_handle, 0,
	      END_OF_VAR_LIST);

  input_data.A = A_handle;
  input_data.x = x_handle;
  input_data.b = b_handle;
  input_data.n = n;
  input_data.nnz = nnz;
  input_data.max_iter = parameters->max_iter;
  
  dpipelinedCG(&input_data, two_dim, 
	       parameters->use_reduction,
	       parameters->explicit_prefetch,
	       parameters->use_pack,
	       parameters->permuting_instruction,
	       0, 0,
	       blockRowPtr, 
	       distribution, workers_list);
  
  manage_data(0,
	      UNPARTITION, x_handle, 0, 
	      UNPARTITION, b_handle, 0, 
	      END_OF_VAR_LIST);
  if( two_dim) {
    manage_data(0,
		UNPARTITION_2D, A_handle, nchunks, 0, 
		END_OF_VAR_LIST);
    csr_2D_destroy(matrix_2D);
    for(chunk = 0; chunk < nchunks; chunk++) 
      free(distribution[chunk]);
    free(distribution);
  } else {
    manage_data(0,
		UNPARTITION, A_handle, 0, 
		END_OF_VAR_LIST);
  }
  
  return 0;
}

int 
dpipelinedCG_test_check_function(test_struct parameters) 
{
  /* norm2 ( res_ref res_starpu) / norm2 ( res_ref )  it needs *
   * to be smaller than 10e-6 simple / 10e-16 double           */
  struct pipelinedCG_input input_data;
  unsigned nchunks = parameters->current_nchunks, chunk;
  unsigned workers_list[nchunks];
  unsigned blockRowPtr[nchunks+1];
  int two_dim = parameters->two_dim;
  unsigned **distribution = NULL;
  struct csr_2D_struct *matrix_2D;

  create_blocRowPtr(rowPtr, m, nnz, nchunks, parameters->ratio_CPU_GPU, blockRowPtr);
  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);

  starpu_data_acquire(x_handle, STARPU_W);
  starpu_data_acquire(b_handle, STARPU_W);

  init_dvect(*x, 0, m, 0.0);
  double tmp[m];
  init_dvect(tmp, 0, m, 1.0);
  double one = 1.0, zero = 0.0;
  mkl_dcsrmv("N", (int *)&m, (int *)&n, &one, "G**C",
             vals, (int *) colInd, (int *) rowPtr,
             (int *) &rowPtr[1], tmp, &zero, b);

  starpu_data_release(x_handle);
  starpu_data_release(b_handle);

  if( two_dim) {
    matrix_2D = csr_2D_index_partition(blockRowPtr, nchunks, colInd, vals, rowPtr);
    distribution = create_distribution_vector(matrix_2D, nchunks);
    manage_data(0,
		CSR_2D_INDEX_PARTITION, A_handle, matrix_2D, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH_2D, A_handle, distribution, 0,
		END_OF_VAR_LIST);
  } else { 
    manage_data(0,
		CSR_INDEX_PARTITION, A_handle, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH, A_handle, 0,
		END_OF_VAR_LIST);
  }
  manage_data(0,
 	      PREFETCH_ON_ALL_WORKERS, x_handle, 0,
	      VECTOR_INDEX_PARTITION,  x_handle, blockRowPtr, nchunks, 
	      VECTOR_INDEX_PARTITION,  b_handle, blockRowPtr, nchunks, 
	      CYCLIC_PREFETCH, b_handle, 0,
	      END_OF_VAR_LIST);

  input_data.A = A_handle;
  input_data.x = x_handle;
  input_data.b = b_handle;
  input_data.n = n;
  input_data.nnz = nnz;
  input_data.max_iter = parameters->max_iter;
  input_data.eps = 0.0;
  
  dpipelinedCG(&input_data, two_dim, 
	       parameters->use_reduction,
	       parameters->explicit_prefetch,
	       parameters->use_pack,
	       parameters->permuting_instruction,
	       1,
	       parameters->debug_mode,
	       blockRowPtr, 
	       distribution, workers_list);
  
  manage_data(0,
	      UNPARTITION, x_handle, 0, 
	      UNPARTITION, b_handle, 0, 
	      END_OF_VAR_LIST);
  if( two_dim) {
    manage_data(0,
		UNPARTITION_2D, A_handle, nchunks, 0, 
		END_OF_VAR_LIST);
    csr_2D_destroy(matrix_2D);
    for(chunk = 0; chunk < nchunks; chunk++) 
      free(distribution[chunk]);
    free(distribution);
  } else {
    manage_data(0,
		UNPARTITION, A_handle, 0, 
		END_OF_VAR_LIST);
  }

  return 0;
}

int dpipelinedCG_test_finalize_function(test_struct parameters) 
{
  manage_data(0,
	      UNREGISTER, A_handle,
	      UNREGISTER, x_handle,
	      UNREGISTER, b_handle,

	      FREE_DATA, vals,
	      FREE_DATA, rowPtr,
	      FREE_DATA, colInd,
	      FREE_DATA_ON_ALL, x, parameters->n*sizeof(**x),
	      FREE_DATA, b,
	      END_OF_VAR_LIST);

  return 0;
}
