#include <sys/time.h>
#include "common.h"
#include "test.h"
#include "dkernels.h"
#include "data_helpers.h"
#include "auxiliary.h"
#include "mkl.h"


static double *v1, *v2, *alpha; 
static starpu_data_handle_t v1_handle, v2_handle, alpha_handle;


int daxpy_test_init_function(test_struct parameters) 
{
  int n = parameters->n;
  manage_data(0,
	      MALLOC_DATA ,&v1, (size_t) n*sizeof(*v1), 
	      MALLOC_DATA ,&v2, (size_t) n*sizeof(*v2), 
	      MALLOC_DATA ,&alpha, (size_t) sizeof(*alpha), 
	      END_OF_VAR_LIST);
  assert( v1 && v2 && alpha);
  
  init_dvect(v1, 0, n, 1.0);  
  init_dvect(v2, 0, n, 1.0);  
  *alpha = 1.0;
  
  manage_data(0,
	      VECTOR_REGISTER, &v1_handle,    0, v1,    n, sizeof(*v1),
	      VECTOR_REGISTER, &v2_handle,    0, v2,    n, sizeof(*v2), 
	      VECTOR_REGISTER, &alpha_handle, 0, alpha, 1, sizeof(*alpha), 
	      END_OF_VAR_LIST);

  return 0;
}

int daxpy_test_compute_function(test_struct parameters) 
{
  unsigned nchunks = parameters->current_nchunks;
  struct timeval start, end; 
  double timing = 0.0;
  long nb_flops;
  int i = 0, nb_repeat = parameters->nb_repeat;
  unsigned workers_list[nchunks];

  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);

  manage_data(0,
	      VECTOR_BLOCK_PARTITION, v1_handle, nchunks, 
	      VECTOR_BLOCK_PARTITION, v2_handle, nchunks, 
	      CYCLIC_PREFETCH, v1_handle, 0,
	      CYCLIC_PREFETCH, v2_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, alpha_handle, 0,
	      END_OF_VAR_LIST);

  gettimeofday(&start, NULL);
  for( ; i < nb_repeat; i++) {
    daxpy_kernel(v1_handle, v2_handle, alpha_handle, NONE_PACKED, 0, workers_list, 0, 0);
  }
  starpu_task_wait_for_all ();
  gettimeofday(&end, NULL);
  timing = (double)(((double)end.tv_sec - (double)start.tv_sec)*1e6 + ((double)end.tv_usec - (double)start.tv_usec));
  
  timing /= nb_repeat;
  nb_flops = 2 * parameters->n;
  print_stats(parameters, timing, nb_flops);

  manage_data(0,
	      UNPARTITION, v1_handle, 0, 
	      UNPARTITION, v2_handle, 0, 
	      END_OF_VAR_LIST);
  return 0;
}

int daxpy_test_check_function(test_struct parameters) 
{
  /* norm2 ( res_ref res_starpu) / norm2 ( res_ref )  it needs *
   * to be smaller than 10e-6 simple / 10e-16 double           */
  double relative_error; 
  double *mkl_res;
  unsigned nchunks = parameters->current_nchunks;
  int n = parameters->n;
  unsigned workers_list[nchunks];

  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);

  mkl_res = malloc(n * sizeof(*mkl_res));

  starpu_data_acquire(v1_handle, STARPU_W);
  starpu_data_acquire(v2_handle, STARPU_W);
  starpu_data_acquire(alpha_handle, STARPU_W);

  init_dvect(mkl_res, 0, n, 3.0);
  init_dvect(v1, 0, n, 3.0);
  init_dvect(v2, 0, n, 3.0);
  *alpha = 5.0;

  cblas_daxpy(n, *alpha, v1, 1, mkl_res, 1);
  starpu_data_release(v1_handle);
  starpu_data_release(v2_handle);
  starpu_data_release(alpha_handle);


  manage_data(0,
	      VECTOR_BLOCK_PARTITION, v1_handle, nchunks, 
	      VECTOR_BLOCK_PARTITION, v2_handle, nchunks, 
	      CYCLIC_PREFETCH, v1_handle, 0,
	      CYCLIC_PREFETCH, v2_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, alpha_handle, 0,
	      END_OF_VAR_LIST);

  daxpy_kernel(v1_handle, v2_handle, alpha_handle, NONE_PACKED, 0, workers_list, 0, 0);
  
  manage_data(0,
	      UNPARTITION, v1_handle, 0, 
	      UNPARTITION, v2_handle, 0, 
	      END_OF_VAR_LIST);

  double norm_mkl = cblas_dnrm2 (n, mkl_res, 1);
  cblas_daxpy(n, -1.0, mkl_res, 1, v2, 1);
  double norm_mkl_starpu =  cblas_dnrm2(n, v2, 1);

  relative_error = norm_mkl_starpu / norm_mkl;

  check_relative_error_print(relative_error, parameters);
  
  return 0;
}

int daxpy_test_finalize_function(test_struct parameters) 
{
  manage_data(0,
	      UNREGISTER, v1_handle,
	      UNREGISTER, v2_handle,
	      UNREGISTER, alpha_handle,
	      FREE_DATA, v1,
	      FREE_DATA, v2,
	      FREE_DATA, alpha,
	      END_OF_VAR_LIST);
  return 0;
}
