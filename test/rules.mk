C_SOURCES += $(addprefix  ${topsrcdir}/test/, \
						test.c\
						ddot_test.c\
	                         	        daxpy_test.c\
	                                	dcopyvect_test.c\
						dscalaxpy_test.c\
						dspmv_test.c\
						dspmv_sparsevect_test.c\
						dCG_test.c\
						dpipelinedCG_test.c)
TARGET 	+=  ${topsrcdir}/test/test