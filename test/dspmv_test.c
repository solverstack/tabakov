#include <sys/time.h>
#include <mkl.h>
#include "common.h"
#include "test.h"
#include "dkernels.h"
#include "data_helpers.h"
#include "auxiliary.h"
#include "enum.h"
#include "csr_load_matrix.h"
#include "codelets_d.h"

static double *alpha, *beta, **x, *y, *vals;
static starpu_data_handle_t x_handle, y_handle, A_handle, alpha_handle, beta_handle;
static unsigned *rowPtr, *colInd; 
static unsigned nnz, m, n;

int dspmv_test_init_function(test_struct parameters) 
{
  csr_load_matrix(parameters->matrix_file, 
		  &vals, &colInd, &nnz, 
		  &rowPtr, &m, &n);
  parameters->n = n;
  
  manage_data(0,
	      MALLOC_DATA, &y, (size_t) n*sizeof(*y), 
	      MALLOC_DATA, &alpha, (size_t) sizeof(*alpha), 
	      MALLOC_DATA, &beta,  (size_t) sizeof(*beta), 
	      END_OF_VAR_LIST);

  assert( y && alpha && beta);
  init_dvect(y, 0, n, 1.0);  
  *alpha = 1.0; *beta = 1.0;

  manage_data(0,
	      VECTOR_REGISTER_ON_ALL, &x_handle, &x,    n, sizeof(**x),
	      CSR_REGISTER,    &A_handle,     0, nnz,   m, (uintptr_t)vals, colInd, rowPtr, sizeof(*vals),
	      VECTOR_REGISTER, &y_handle,     0, y,     n, sizeof(*y), 
	      VECTOR_REGISTER, &alpha_handle, 0, alpha, 1, sizeof(*alpha), 
	      VECTOR_REGISTER, &beta_handle,  0, beta,  1, sizeof(*beta), 
	      END_OF_VAR_LIST);
  return 0;
}


int 
dspmv_test_compute_function(test_struct parameters) 
{
  unsigned nchunks = parameters->current_nchunks, chunk;
  struct timeval start, end;
  double timing = 0.0;
  long nb_flops;
  int i = 0, nb_repeat = parameters->nb_repeat;
  unsigned workers_list[nchunks];
  unsigned blockRowPtr[nchunks+1];
  int two_dim = parameters->two_dim;
  int use_reduction = parameters->use_reduction;
  int no_alpha_beta = parameters->no_alpha_beta;
  unsigned **distribution = NULL;
  struct csr_2D_struct *matrix_2D;

  create_blocRowPtr(rowPtr, m, nnz, nchunks, parameters->ratio_CPU_GPU, blockRowPtr);
  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);
  
  if( two_dim) {
    matrix_2D = csr_2D_index_partition(blockRowPtr, nchunks, colInd, vals, rowPtr);
    distribution = create_distribution_vector(matrix_2D, nchunks);
    manage_data(0,
		CSR_2D_INDEX_PARTITION, A_handle, matrix_2D, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH_2D, A_handle, distribution, 0,
		END_OF_VAR_LIST);
  } else { 
    manage_data(0,
		CSR_INDEX_PARTITION, A_handle, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH, A_handle, 0,
		END_OF_VAR_LIST);
  }

  manage_data(0,
	      PREFETCH_ON_ALL_WORKERS, x_handle, 0,
	      VECTOR_INDEX_PARTITION,  x_handle, blockRowPtr, nchunks, 
	      VECTOR_INDEX_PARTITION,  y_handle, blockRowPtr, nchunks, 
	      CYCLIC_PREFETCH, y_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, alpha_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, beta_handle, 0,
	      END_OF_VAR_LIST);

  gettimeofday(&start, NULL);
  for( ; i < nb_repeat; i++) {
    dspmv_kernel(A_handle, x_handle, y_handle,
		       alpha_handle, beta_handle, no_alpha_beta,
		       two_dim, use_reduction, NONE_PACKED, workers_list,
		       distribution, 0);
  }
  starpu_task_wait_for_all ();
  gettimeofday(&end, NULL);
  
  timing = (double)(((double)end.tv_sec - (double)start.tv_sec)*1e6 + ((double)end.tv_usec - (double)start.tv_usec));

  timing /= nb_repeat;
  nb_flops = 2 * nnz;
  print_stats(parameters, timing, nb_flops);

  if( two_dim) {
    manage_data(0,
		UNPARTITION_2D, A_handle, nchunks, 0, 
		END_OF_VAR_LIST);
    csr_2D_destroy(matrix_2D);
    for(chunk = 0; chunk < nchunks; chunk++) 
      free(distribution[chunk]);
    free(distribution);
  } else {
    manage_data(0,
		UNPARTITION, A_handle, 0, 
		END_OF_VAR_LIST);
  }

  manage_data(0,
	      UNPARTITION, y_handle, 0, 
	      UNPARTITION, x_handle, 0, 
	      END_OF_VAR_LIST);

  return 0;
}

int 
dspmv_test_check_function(test_struct parameters) 
{
  /* norm2 ( res_ref res_starpu) / norm2 ( res_ref )  it needs *
   * to be smaller than 10e-6 simple / 10e-16 double           */
  double relative_error; 
  double *mkl_res;
  unsigned nchunks = parameters->current_nchunks, chunk;
  unsigned workers_list[nchunks];
  unsigned blockRowPtr[nchunks+1];
  int two_dim = parameters->two_dim;
  int use_reduction = parameters->use_reduction;
  int no_alpha_beta = parameters->no_alpha_beta;
  unsigned **distribution = NULL;
  struct csr_2D_struct *matrix_2D;

  create_blocRowPtr(rowPtr, m, nnz, nchunks, parameters->ratio_CPU_GPU, blockRowPtr);
  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);

  mkl_res = malloc(n * sizeof(*mkl_res));

  starpu_data_acquire(x_handle, STARPU_W);
  starpu_data_acquire(y_handle, STARPU_W);

  init_dvect(mkl_res, 0, m, 3.0);
  init_dvect(*x, 0, m, 3.0);
  init_dvect(y, 0, m, 3.0);
  if (no_alpha_beta) {
    double one = 1.0, zero = 0.0;
    mkl_dcsrmv("N", (int *)&m, (int *)&n, &one, "G**C",
	       vals, (int *) colInd, (int *) rowPtr,
	       (int *) &rowPtr[1], *x, &zero, mkl_res);
  } else { 
    mkl_dcsrmv("N", (int *)&m, (int *)&n, alpha, "G**C",
	       vals, (int *) colInd, (int *) rowPtr,
	       (int *) &rowPtr[1], *x, beta, mkl_res);
  }
  starpu_data_release(x_handle);
  starpu_data_release(y_handle);

  if( two_dim) {
    matrix_2D = csr_2D_index_partition(blockRowPtr, nchunks, colInd, vals, rowPtr);
    distribution = create_distribution_vector(matrix_2D, nchunks);
    manage_data(0,
		CSR_2D_INDEX_PARTITION, A_handle, matrix_2D, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH_2D, A_handle, distribution, 0,
		END_OF_VAR_LIST);
  } else { 
    manage_data(0,
		CSR_INDEX_PARTITION, A_handle, blockRowPtr, nchunks, 
		CYCLIC_PREFETCH, A_handle, 0,
		END_OF_VAR_LIST);
  }
  
  manage_data(0,
	      VECTOR_INDEX_PARTITION,  y_handle, blockRowPtr, nchunks, 
	      PREFETCH_ON_ALL_WORKERS, x_handle, 0,
	      VECTOR_INDEX_PARTITION,  x_handle, blockRowPtr, nchunks, 
	      CYCLIC_PREFETCH, y_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, alpha_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, beta_handle, 0,
	      END_OF_VAR_LIST);
  
  dspmv_kernel(A_handle, x_handle, y_handle,
	       alpha_handle, beta_handle, no_alpha_beta,
	       two_dim, use_reduction, NONE_PACKED,
	       workers_list, distribution, 0);
  
  if( two_dim) {
    manage_data(0,
		UNPARTITION_2D, A_handle, nchunks, 0, 
		END_OF_VAR_LIST);
    csr_2D_destroy(matrix_2D);
    for(chunk = 0; chunk < nchunks; chunk++) 
      free(distribution[chunk]);
    free(distribution);
  } else {
    manage_data(0,
		UNPARTITION, A_handle, 0, 
		END_OF_VAR_LIST);
  }
  manage_data(0,
	      UNPARTITION, x_handle, 0, 
	      UNPARTITION, y_handle, 0, 
	      END_OF_VAR_LIST);

  starpu_data_acquire(y_handle, STARPU_R);
  double norm_mkl = cblas_dnrm2 (n, mkl_res, 1);
  cblas_daxpy(n, -1.0, mkl_res, 1, y, 1);
  double norm_mkl_starpu =  cblas_dnrm2(n, y, 1);
  starpu_data_release(y_handle);

  relative_error = norm_mkl_starpu / norm_mkl;
  /* fprintf(stdout,"kernel %s: Checking the relative error\n", parameters->kernel); */
  check_relative_error_print(relative_error, parameters);
  
  free(mkl_res);
  return 0;
}


int dspmv_test_finalize_function(test_struct parameters) 
{
  manage_data(0,
	      UNREGISTER, A_handle,
	      UNREGISTER, x_handle,
	      UNREGISTER, y_handle,
	      UNREGISTER, alpha_handle,
	      UNREGISTER, beta_handle,

	      FREE_DATA_ON_ALL, x, parameters->n*sizeof(**x),
	      FREE_DATA, vals,
	      FREE_DATA, rowPtr,
	      FREE_DATA, colInd,
	      FREE_DATA, y,
	      FREE_DATA, alpha,
	      FREE_DATA, beta,
	      END_OF_VAR_LIST);

  return 0;
}
