#include <sys/time.h>
#include "common.h"
#include "test.h"
#include "dkernels.h"
#include "data_helpers.h"
#include "auxiliary.h"
#include "codelets_d.h"
#include "p_dkernels.h"
#include "mkl.h"
#include "enum.h"

static double *v1, *v2, *dot, **dots; 
static starpu_data_handle_t v1_handle, v2_handle, dot_handle, dots_handle;

int 
ddot_test_init_function(test_struct parameters) 
{
  int n = parameters->n;
  manage_data(0,
	      MALLOC_DATA ,&v1,  (size_t) n*sizeof(*v1), 
	      MALLOC_DATA ,&v2,  (size_t) n*sizeof(*v2), 
	      MALLOC_DATA ,&dot, (size_t) sizeof(*dot), 
	      END_OF_VAR_LIST);


  init_dvect(v1, 0, n, 1.0);  
  init_dvect(v2, 0, n, 1.0);  
  *dot = 0.0;

  manage_data(0,
	      VECTOR_REGISTER, &v1_handle,    0, v1,    n, sizeof(*v1),
	      VECTOR_REGISTER, &v2_handle,    0, v2,    n, sizeof(*v2), 
	      VECTOR_REGISTER, &dot_handle,   0, dot,   1, sizeof(*dot), 
	      END_OF_VAR_LIST);

  if ( parameters->use_reduction == MANUAL_REDUCTION )  
    setup_cumulVect_manual_reduction(&dots_handle, &dots);
  
  if ( parameters->use_reduction == STARPU_REDUCTION ) 
    starpu_data_set_reduction_methods(dot_handle, &daccumvect_task_cl, &dbzero_task_cl);
  
  return 0;
}

int 
ddot_test_compute_function(test_struct parameters) 
{
  unsigned nchunks = parameters->current_nchunks;
  struct timeval start, end; 
  double timing = 0.0;
  long nb_flops;
  int i, nb_repeat = parameters->nb_repeat;
  int use_reduction = parameters->use_reduction;
  int explicit_prefetch = parameters->explicit_prefetch;
  unsigned workers_list[nchunks];
  unsigned n_workers = starpu_worker_get_count();

  init_cyclic_worker_list(workers_list, n_workers, nchunks);


  manage_data(0,
	      VECTOR_BLOCK_PARTITION, v1_handle, nchunks, 
	      VECTOR_BLOCK_PARTITION, v2_handle, nchunks, 
	      CYCLIC_PREFETCH, v1_handle, 0,
	      CYCLIC_PREFETCH, v2_handle, 0,
	      PREFETCH_ON_ALL_WORKERS, dot_handle, 0,
	      END_OF_VAR_LIST);

  gettimeofday(&start,  NULL);
  for( i = 0; i < nb_repeat; i++) {
    ddot_kernel(v1_handle, v2_handle, dot_handle, dots_handle, NONE_PACKED, use_reduction, explicit_prefetch, workers_list, 0);
  }
  starpu_task_wait_for_all ();
  gettimeofday(&end, NULL);
  timing = (double)(((double)end.tv_sec - (double)start.tv_sec)*1e6 + ((double)end.tv_usec - (double)start.tv_usec));
  starpu_data_acquire(dot_handle, STARPU_R);
  starpu_data_release(dot_handle);
  
  timing /= nb_repeat;
  nb_flops = 2 * parameters->n;
  print_stats(parameters, timing, nb_flops);

  manage_data(0,
	      UNPARTITION, v1_handle, 0, 
	      UNPARTITION, v2_handle, 0, 
	      END_OF_VAR_LIST);
  return 0;
}



int 
ddot_test_check_function(test_struct parameters) 
{
  /* norm2 ( res_ref res_starpu) / norm2 ( res_ref )  it needs *
   * to be smaller than 10e-6 simple / 10e-16 double           */
  double dot_mkl, error; 
  unsigned nchunks = parameters->current_nchunks;
  int explicit_prefetch = parameters->explicit_prefetch;
  int use_reduction = parameters->use_reduction;
  int n = parameters->n;
  unsigned workers_list[nchunks];

  init_cyclic_worker_list(workers_list, starpu_worker_get_count(), nchunks);

  starpu_data_acquire(v1_handle, STARPU_W);
  starpu_data_acquire(v2_handle, STARPU_W);
  starpu_data_acquire(dot_handle, STARPU_W);

  init_dvect(v1, 0, n, 1.0);
  init_dvect(v2, 0, n, 1.0);
  *dot = 0.0;

  dot_mkl = cblas_ddot(n, v1, 1, v2, 1);

  starpu_data_release(v1_handle);
  starpu_data_release(v2_handle);
  starpu_data_release(dot_handle);

  manage_data(0,
	      VECTOR_BLOCK_PARTITION, v1_handle, nchunks, 
	      VECTOR_BLOCK_PARTITION, v2_handle, nchunks, 
	      CYCLIC_PREFETCH, v1_handle, 0,
	      CYCLIC_PREFETCH, v2_handle, 0, 
	      PREFETCH_ON_ALL_WORKERS, dot_handle, 0,
	      END_OF_VAR_LIST);
  
  ddot_kernel(v1_handle, v2_handle, dot_handle, dots_handle, NONE_PACKED, use_reduction, explicit_prefetch, workers_list, 0);

  starpu_data_acquire(dot_handle, STARPU_R);
  printf("in test dot = %e\n", *dot);
  error = (double) sqrt( fabs(dot_mkl) - fabs(*dot) ) / (double)sqrt( fabs(dot_mkl) );

  check_relative_error_print(error, parameters);
  starpu_data_release(dot_handle);

  manage_data(0,
	      UNPARTITION, v1_handle, 0, 
	      UNPARTITION, v2_handle, 0, 
	      END_OF_VAR_LIST);

  return 0;
}

int ddot_test_finalize_function(test_struct parameters) 
{

  manage_data(0,
	      UNREGISTER, v1_handle,
	      UNREGISTER, v2_handle,
	      UNREGISTER, dot_handle,
	      FREE_DATA, v1,
	      FREE_DATA, v2,
	      FREE_DATA, dot,
	      END_OF_VAR_LIST);

  if ( parameters->use_reduction == MANUAL_REDUCTION ) 
    cleanup_cumulVect_manual_reduction(dots_handle, dots);

  return 0;
}
