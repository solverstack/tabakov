#include "common.h"
#include "test.h"
#include "auxiliary.h"
#include "cublas_helper.h"
#include "cusparse_helper.h"
#include "csr_load_matrix.h"
#include "enum.h"

static const char *usageStrign[] = {
  "usage test: [-h] [--no_reduction] [--start_nchunks #blocks] [--end_nchunks #blocks]",
  "[--n #nb_elem] [--nb_repeat #rep] [--check] [--no_explicit_prefetch] [--2D] [--no_alpha_beta]",
  "[--starpu_reduction] [--kernel (kernel)] [--matrix (matrix)] [--debug_mode]",
  "[--no_permuting_instructions] [--no_pack] [--max_iter #max_iter] [--ratio_CPU_GPU factor] [--verbosity (level)]",
  NULL,
};

static void init_test_functions(char *kernel, test_struct self)
{
  if ( !strcmp(kernel, "dot")) {
    self->kernel        = kernel;
    self->init_func     = ddot_test_init_function;
    self->compute_func  = ddot_test_compute_function;
    self->check_func    = ddot_test_check_function;
    self->finalize_func = ddot_test_finalize_function;
  } else  if ( !strcmp(kernel, "axpy")) {
    self->kernel        = kernel;
    self->init_func     = daxpy_test_init_function;
    self->compute_func  = daxpy_test_compute_function;
    self->check_func    = daxpy_test_check_function;
    self->finalize_func = daxpy_test_finalize_function;
  } else  if ( !strcmp(kernel, "copyvect")) {
    self->kernel        = kernel;
    self->init_func     = dcopyvect_test_init_function;
    self->compute_func  = dcopyvect_test_compute_function;
    self->check_func    = dcopyvect_test_check_function;
    self->finalize_func = dcopyvect_test_finalize_function;
  } else  if ( !strcmp(kernel, "scalaxpy")) {
    self->kernel        = kernel;
    self->init_func     = dscalaxpy_test_init_function;
    self->compute_func  = dscalaxpy_test_compute_function;
    self->check_func    = dscalaxpy_test_check_function;
    self->finalize_func = dscalaxpy_test_finalize_function;
  } else  if ( !strcmp(kernel, "spmv")) {
    self->kernel        = kernel;
    self->init_func     = dspmv_test_init_function;
    self->compute_func  = dspmv_test_compute_function;
    self->check_func    = dspmv_test_check_function;
    self->finalize_func = dspmv_test_finalize_function;
  } else  if ( !strcmp(kernel, "CG")) {
    self->kernel        = kernel;
    self->init_func     = dCG_test_init_function;
    self->compute_func  = dCG_test_compute_function;
    self->check_func    = dCG_test_check_function;
    self->finalize_func = dCG_test_finalize_function;
  } else  if ( !strcmp(kernel, "pipelinedCG")) {
    self->kernel        = kernel;
    self->init_func     = dpipelinedCG_test_init_function;
    self->compute_func  = dpipelinedCG_test_compute_function;
    self->check_func    = dpipelinedCG_test_check_function;
    self->finalize_func = dpipelinedCG_test_finalize_function;
  } else  if ( !strcmp(kernel, "spmv_sparsevect")) {
    self->kernel        = kernel;
    self->init_func     = dspmv_sparsevect_test_init_function;
    self->compute_func  = dspmv_sparsevect_test_compute_function;
    self->check_func    = dspmv_sparsevect_test_check_function;
    self->finalize_func = dspmv_sparsevect_test_finalize_function;
  } else 
    fatal_error(__FUNCTION__,"unknown kernel");
}

static void parse_args(int argc,
		       char **argv,
		       test_struct self)
{
  int i;
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "--no_reduction")) {
      self->use_reduction = NO_REDUCTION;
      continue;
    } else if (!strcmp(argv[i], "--starpu_reduction")) {
      self->use_reduction = STARPU_REDUCTION;
      continue;
    } else if (!strcmp(argv[i], "--start_nchunks")) {
      int tmp = atoi(argv[++i]);
      if ( tmp < 0 )
	fatal_error("parse_args","start_nchunks can not be negative");
      self->start_nchunks = (unsigned) tmp;
      continue;
    } else if (!strcmp(argv[i], "--end_nchunks")) {
      int tmp = atoi(argv[++i]);
      if ( tmp < 0 )
	fatal_error("parse_args","end_nchunks can not be negative");
      self->end_nchunks = (unsigned) tmp;
      continue;
    } else if (!strcmp(argv[i], "--n")) {
      self->n = atoi(argv[++i]);
      continue;
    } else if (!strcmp(argv[i], "--nb_repeat")) {
      int tmp = atoi(argv[++i]);
      self->nb_repeat = (unsigned) tmp;
      continue;
    } else if (!strcmp(argv[i], "--check")) {
      self->check = 1;
      continue;
    } else if (!strcmp(argv[i], "--no_explicit_prefetch")) {
      self->explicit_prefetch = 0;
      continue;
    }else if (!strcmp(argv[i], "--2D")) {
      self->two_dim = 1;
      continue;
    } else if (!strcmp(argv[i], "--no_permuting_instructions")) {
      self->permuting_instruction = 0;
      continue;
    } else if (!strcmp(argv[i], "--no_pack")) {
      self->use_pack = 0;
      continue;
    } else if (!strcmp(argv[i], "--no_alpha_beta")) {
      self->no_alpha_beta = 1;
      continue;
    }else if (!strcmp(argv[i], "--ratio_CPU_GPU")) {
      double tmp = atof(argv[++i]);
      if ( tmp < 0.0 )
	fatal_error("parse_args","ratio_CPU_GPU can not be negative");
      self->ratio_CPU_GPU = (double) tmp;
      continue;
    } else if (!strcmp(argv[i], "--verbosity")) {
      self->verbosity = atoi(argv[++i]);
      continue;
    } else if (!strcmp(argv[i], "--kernel")) {
      init_test_functions(argv[++i], self);
      continue;
    } else if (!strcmp(argv[i], "--matrix")) {
      self->matrix_file = argv[++i];
      csr_load_sizes(self->matrix_file, &self->n,
		    &self->n, &self->nnz);
      continue;
    } else if (!strcmp(argv[i], "--debug_mode")) {
      self->debug_mode = 1;
      continue;
    }else if (!strcmp(argv[i], "--max_iter")) {
      int tmp = atoi(argv[++i]);
      if ( tmp < 0 )
	fatal_error("parse_args","max_iter can not be negative");
      self->max_iter = (unsigned) tmp;
      continue;
    } else {
      int l = 0; 
      while ( usageStrign[l] != NULL)
	fprintf(stderr, "%s\n", usageStrign[l++]);
      if (strcmp(argv[i], "-h"))
	  fprintf(stderr,"%s: error on argument (%s)\n",argv[0], argv[i]);
      exit(-1);
    }
  }
  if (self->debug_mode) {
    self->matrix_file = "/lustre/nakov/test_conv_matrices/identity.mtx";
    self->max_iter=2;
    self->permuting_instruction = 0;
    self->check=1;
    csr_load_sizes(self->matrix_file, &self->n,
		   &self->n, &self->nnz);
  }
  if (!strcmp(self->kernel, "CG") ) 
    self->nb_repeat = 0;
}


static test_struct create_test_struct()
{
  test_struct self = calloc(1, sizeof(*self));
  self->start_nchunks=1;
  self->end_nchunks=1;
  self->current_nchunks=1;
  self->max_iter=1;
  self->nb_repeat=5;
  self->use_reduction=MANUAL_REDUCTION;
  self->check=0;
  self->use_pack=1;
  self->no_alpha_beta=0;
  self->verbosity=1;
  self->explicit_prefetch=1;
  self->permuting_instruction=1;
  self->debug_mode=0; 
  self->kernel = "dot";
  self->matrix_file = "/lustre/nakov/matrices_home/spd_collection/11pts-32-32-32.mtx";
  self->ratio_CPU_GPU = 1.0;

  csr_load_sizes(self->matrix_file, &self->n,
		 &self->n, &self->nnz);

  return self;
}


int
main(int argc, char **argv)
{
  test_struct parameters = create_test_struct();
  parse_args(argc, argv, parameters);
  
  if ( starpu_init(NULL) ) 
    fatal_error("main", "starpu_init failed");

  cublas_helper_init();
  starpu_helper_cusparse_init();

  parameters->init_func(parameters);
  
  if (parameters->verbosity > 0)
    print_parameters(parameters);
#ifdef PRINT_WORKER_STATS
  starpu_profiling_status_set(STARPU_PROFILING_ENABLE);
#endif 

  for (parameters->current_nchunks = parameters->start_nchunks ;
       parameters->current_nchunks <= parameters->end_nchunks  ;
       parameters->current_nchunks++ ) {
    
    parameters->compute_func(parameters);

    if ( parameters->check)
      parameters->check_func(parameters);
  }

  parameters->finalize_func(parameters); 

  cublas_helper_shutdown();
  starpu_helper_cusparse_shutdown();
  starpu_shutdown();
  free(parameters);
  return 0;
}
