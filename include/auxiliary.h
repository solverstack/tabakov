#ifndef _AUXILIARY_H_
#define _AUXILIARY_H

#include "common.h"
#include "test.h"

#define EPS_MACHINE 1e-16
void print_parameters(test_struct parameters);
void print_stats(test_struct parameters, double timing, long nb_flops);
void fatal_error(const char *func, char *msg);
void check_relative_error_print(double error, test_struct parameters);
void init_cyclic_worker_list(unsigned *worker_list, unsigned n_workers, unsigned n_tasks);
void create_blocRowPtr(unsigned *rowPtr, unsigned m, unsigned nnz, unsigned nchunks, double ratio_CPU_GPU, unsigned *blocRowPtr);

void write_uvect(unsigned *vect, unsigned n, char *name);
void print_worker_stats();


#endif // _AUXILIARY_H_
