#ifndef __ENUM_H__
#define __ENUM_H__

#define NO_REDUCTION              0
#define STARPU_REDUCTION          1
#define MANUAL_REDUCTION          2

#define END_OF_VAR_LIST          -1

#define SPARSEVECT_FULL         1
#define SPARSEVECT_PACK         2
#define SPARSEVECT_CONTINUOUS   3
#define SPARSEVECT_NONE         4

#define SPARSEVECT_MEMORY_OWE            0
#define SPARSEVECT_MEMORY_VALID          1
#define SPARSEVECT_MEMORY_INVALID        2

#define ZERO_VALUE_INDICE     0       
#define ONE_VALUE_INDICE      1
#define TMP_VALUE_INDICE      2

#define NONE_PACKED      0
#define FIRST_PACKED     (1<<1)   
#define SECOND_PACKED    (1<<2)
#define THIRD_PACKED     (1<<3)

#endif //  __ENUM_H__
