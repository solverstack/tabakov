#ifndef _CG_CUBALS_HELPER_H
#define _CG_CUBALS_HELPER_H


#include <cublas.h>

void cublas_helper_init(void);

void cublas_helper_shutdown(void);

cublasHandle_t cublas_get_handle();

double *cublas_get_zero_value(); 
double *cublas_get_one_value(); 
double *cublas_get_tmp_value(); 

#endif
