#ifndef _FILTERS_H_
#define _FILTERS_H_

#include <stdarg.h>
#include "enum.h"    
#include "test.h"

// data*, size
#define MALLOC_DATA                1

// handle*, 0, vect, n, size
#define VECTOR_REGISTER            2

// handle*, vects*, n, size
#define VECTOR_REGISTER_ON_ALL     3

// handle*, vects*, n, size
#define SPARSEVECT_REGISTER_ON_ALL 4

// handle, mode, nnz, m, vals, colInd, rowPtr, elem_size
#define CSR_REGISTER               5

// handle, nchunks
#define VECTOR_BLOCK_PARTITION    20

// handle, blockRowPtr, nchunks
#define VECTOR_INDEX_PARTITION    21

// handle, blockRowPtr, nchunks
#define CSR_INDEX_PARTITION       22

// handle, matrix_2D, blockRowPtr, nchunks
#define CSR_2D_INDEX_PARTITION    23

// handle async
#define CYCLIC_PREFETCH           50

// handle, distribution, async
#define CYCLIC_PREFETCH_2D        51

// handle, async
#define PREFETCH_ON_ALL_WORKERS   52

// handle, async
#define PREFETCH_CHILDREN_ON_ALL_WORKERS   53

// handle, node
#define UNPARTITION               70

// handle, nchunks, node
#define UNPARTITION_2D     71

// handle
#define UNREGISTER                72

// data
#define FREE_DATA                100

// vects* n 
#define FREE_DATA_ON_ALL         101


void
manage_data(int unused, ...);

void 
indx_filter_func_vector(void *father_interface,
			void *child_interface,
			struct starpu_data_filter *f,
			unsigned id,
			unsigned nchunks);

void
csr_index_filter(void *father_interface,
                 void *child_interface,
                 struct starpu_data_filter *f,
                 unsigned id,
                 unsigned nchunks);

void init_dvect(double *v, int start, int end, double val);
void check_vect_all_same(double *vect, int n, char *name);

void prefetch_on_all_workers(starpu_data_handle_t handle, int async);
void prefetch_on_all_needed_workers(starpu_data_handle_t handle, unsigned nchunks, int async);
void prefetch_children_on_all_workers(starpu_data_handle_t handle, int async);
void cyclic_prefetch(starpu_data_handle_t father, int async);
void cyclic_prefetch_2D(starpu_data_handle_t father, unsigned **distribution, int async);
void init_cyclic_worker_list(unsigned *worker_list, unsigned n_workers, unsigned n_tasks);

void dgpu_write_vect(double *vect, unsigned n, char *name, unsigned worker);
void ugpu_write_vect(unsigned *vect, unsigned n, char *name, unsigned worker);
void gpu_check_vect_all_same(double *vect, unsigned n, char *name);
void gpu_check_suite(unsigned *vect, unsigned n, char *name);

void check_matrix_handle_identity(starpu_data_handle_t handle, 
				  struct starpu_data_filter *filter,
				  char *name, char *prefix, int unpartition);

void check_vect_handle_all_same(starpu_data_handle_t handle, 
				struct starpu_data_filter *filter,
				char *name, char *prefix, int unpartition);
void  gpu_print_double(double *val, char *name);


////////////////////////////////////////////////////////////////////////////////////////

struct csr_2D_struct {
  uint32_t nchunks;
  uint32_t nrows; 
  uint32_t *block_nnz;
  uint32_t **RowPtrs;
  uint32_t **ColInds;
  double **Vals;
};


struct csr_2D_struct *csr_2D_index_partition(uint32_t *blockRowPtr,
					     uint32_t nchunks,
					     uint32_t *csrColInd,
					     double *csrVal,
					     uint32_t *csrRowPtro);
void csr_2D_destroy(struct csr_2D_struct *self);

unsigned **
create_distribution_vector(struct csr_2D_struct *matrix_2D, 
			   unsigned nchunks);

#endif // _FILTERS_H_
