#ifndef __TEST_H__
#define __TEST_H__

#include <assert.h>

typedef struct _test_struct *test_struct;

struct _test_struct {
  unsigned start_nchunks;
  unsigned end_nchunks;
  unsigned current_nchunks;
  unsigned max_iter;
  unsigned n;
  unsigned nnz;
  
  int nb_repeat; 
  int use_reduction;
  int check;
  int use_pack;
  int explicit_prefetch;
  int permuting_instruction;
  int two_dim; 
  int no_alpha_beta;
  int verbosity;
  int debug_mode; 

  char *kernel;
  char *matrix_file;

  double ratio_CPU_GPU;

  int (*init_func)(test_struct parameters); 
  int (*compute_func)(test_struct parameters); 
  int (*check_func)(test_struct parameters); 
  int (*finalize_func)(test_struct parameters); 
};


/* ********* TEST FUNCTIONS ********* */ 
//  DDOT_TEST 
int ddot_test_init_function(test_struct parameters);
int ddot_test_compute_function(test_struct parameters);
int ddot_test_check_function(test_struct parameters);
int ddot_test_finalize_function(test_struct parameters);

//  DAXPY_TEST 
int daxpy_test_init_function(test_struct parameters);
int daxpy_test_compute_function(test_struct parameters);
int daxpy_test_check_function(test_struct parameters);
int daxpy_test_finalize_function(test_struct parameters);

//  DCOPYVECT_TEST 
int dcopyvect_test_init_function(test_struct parameters);
int dcopyvect_test_compute_function(test_struct parameters);
int dcopyvect_test_check_function(test_struct parameters);
int dcopyvect_test_finalize_function(test_struct parameters);

//  DSCALAXPY_TEST 
int dscalaxpy_test_init_function(test_struct parameters);
int dscalaxpy_test_compute_function(test_struct parameters);
int dscalaxpy_test_check_function(test_struct parameters);
int dscalaxpy_test_finalize_function(test_struct parameters);

//  DSPMV_TEST 
int dspmv_test_init_function(test_struct parameters);
int dspmv_test_compute_function(test_struct parameters);
int dspmv_test_check_function(test_struct parameters);
int dspmv_test_finalize_function(test_struct parameters);

//  DCG_TEST 
int dCG_test_init_function(test_struct parameters);
int dCG_test_compute_function(test_struct parameters);
int dCG_test_check_function(test_struct parameters);
int dCG_test_finalize_function(test_struct parameters);

//  DpipelinedCG_TEST 
int dpipelinedCG_test_init_function(test_struct parameters);
int dpipelinedCG_test_compute_function(test_struct parameters);
int dpipelinedCG_test_check_function(test_struct parameters);
int dpipelinedCG_test_finalize_function(test_struct parameters);

//  Dspmv_sparsevect_TEST 
int dspmv_sparsevect_test_init_function(test_struct parameters);
int dspmv_sparsevect_test_compute_function(test_struct parameters);
int dspmv_sparsevect_test_check_function(test_struct parameters);
int dspmv_sparsevect_test_finalize_function(test_struct parameters);

#endif // __TEST_H__
