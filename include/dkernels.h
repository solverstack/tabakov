#ifndef _DKERNELS_H_
#define _DKERNELS_H_ 
#include "common.h"

void ddot_kernel(starpu_data_handle_t v1,
		 starpu_data_handle_t v2,
		 starpu_data_handle_t dot,
		 starpu_data_handle_t dots,
		 int use_pack,
		 int use_reduction,
		 int explicit_prefetch, 
		 unsigned *workers_list, 
		 starpu_tag_t starting_tag);

void ddot_kernel_async(starpu_data_handle_t v1,
		       starpu_data_handle_t v2,
		       starpu_data_handle_t dot,
		       starpu_data_handle_t dots,
		       int use_pack,
		       int use_reduction, 
		       int explicit_prefetch, 
		       unsigned *workers_list, 
		       starpu_tag_t starting_tag);

/* v2 = alpha * v1 + v2 */ 
void daxpy_kernel(starpu_data_handle_t v1,
		  starpu_data_handle_t v2,
		  starpu_data_handle_t alpha,
		  int use_pack,
		  int explicit_prefetch,
		  unsigned *workers_list, 
		  starpu_tag_t starting_tag,
		  starpu_tag_t starting_depend_tag);

/* v2 = alpha * v1 + v2 */ 
void daxpy_kernel_async(starpu_data_handle_t v1,
			starpu_data_handle_t v2,
			starpu_data_handle_t alpha,
			int use_pack,
			int explicit_prefetch,
			unsigned *workers_list, 
			starpu_tag_t starting_tag,
			starpu_tag_t starting_depend_tag);

/* v1 = v2 */
void dcopyvect_kernel(starpu_data_handle_t v1,
		      starpu_data_handle_t v2,
		      int use_pack,
		      unsigned *workers_list, 
		      starpu_tag_t starting_tag);

/* v1 = v2 */
void dcopyvect_kernel_async(starpu_data_handle_t v1,
			    starpu_data_handle_t v2,
			    int use_pack,
			    unsigned *workers_list, 
			    starpu_tag_t starting_tag);

/* v2 = alpha * v1 + beta * v2 */
void dscalaxpy_kernel(starpu_data_handle_t v1,
		      starpu_data_handle_t v2,
		      starpu_data_handle_t alpha,
		      starpu_data_handle_t beta,
		      int use_pack,
		      int explicit_prefetch,
		      unsigned *workers_list, 
		      starpu_tag_t starting_tag);

/* v2 = alpha * v1 + beta * v2 */
void dscalaxpy_kernel_async(starpu_data_handle_t v1,
			    starpu_data_handle_t v2,
			    starpu_data_handle_t alpha,
			    starpu_data_handle_t beta,
			    int use_pack,
			    int explicit_prefetch, 
			    unsigned *workers_list, 
			    starpu_tag_t starting_tag);

/* y = alpha A x  + beta y */
void dspmv_kernel(starpu_data_handle_t A,
		  starpu_data_handle_t x,
		  starpu_data_handle_t y, starpu_data_handle_t alpha,
		  starpu_data_handle_t beta,
		  int no_alpha_beta,
		  int two_dim,
		  int use_reduction, 
		  int use_pack,
		  unsigned *workers_list, 
		  unsigned **distribution,
		  starpu_tag_t starting_tag);


/* y = alpha A x  + beta y */
void dspmv_kernel_async(starpu_data_handle_t A, 
			starpu_data_handle_t x,
			starpu_data_handle_t y, starpu_data_handle_t alpha,
			starpu_data_handle_t beta,
			int no_alpha_beta,
			int two_dim,
			int use_reduction, 
			int use_pack,
			unsigned *workers_list, 
			unsigned **distribution,
			starpu_tag_t starting_tag);

#endif // _DKERNELS_H_
