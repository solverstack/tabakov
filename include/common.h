#ifndef __COMMON_H__
#define __COMMON_H__

#include <starpu.h>

#ifdef STARPU_USE_CUDA
#include <starpu_cuda.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#include "enum.h"
#endif // __COMMON_H__
