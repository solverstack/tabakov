#ifndef CODELETS_D_H
#define CODELETS_D_H
#include "common.h"

struct codelet_options {
  int execute_on_worker;
  unsigned worker_id;
  starpu_tag_t tag; 
  starpu_tag_t *depnends_on;
  unsigned n_dependencies;
  void (*callback_func)(void *);
  void *callback_arg;
};

void update_task(struct starpu_task *task,
		 struct codelet_options *options);

extern struct starpu_perfmodel daccumvect_task_model;    
extern struct starpu_perfmodel dsum_vect_task_model;    
extern struct starpu_perfmodel daxpy_task_model;    
extern struct starpu_perfmodel dbzero_task_model;    
extern struct starpu_perfmodel dcompute_alpha_task_model;    
extern struct starpu_perfmodel dcompute_alpha_pipelinedCG_task_model;    
extern struct starpu_perfmodel dcopyvect_task_model;    
extern struct starpu_perfmodel ddivide_var_task_model;    
extern struct starpu_perfmodel ddot_task_model;    
extern struct starpu_perfmodel dscalaxpy_task_model;    
extern struct starpu_perfmodel dspmv_task_model;    

extern struct starpu_codelet daccumvect_task_cl;    
extern struct starpu_codelet dsum_vect_task_cl;    
extern struct starpu_codelet daxpy_task_cl;    
extern struct starpu_codelet dbzero_task_cl;    
extern struct starpu_codelet dcompute_alpha_task_cl;    
extern struct starpu_codelet dcompute_alpha_pipelinedCG_task_cl;    
extern struct starpu_codelet dcopyvect_task_cl;    
extern struct starpu_codelet ddivide_var_task_cl;    
extern struct starpu_codelet ddot_task_cl;    
extern struct starpu_codelet dscalaxpy_task_cl;    
extern struct starpu_codelet dspmv_task_cl;    


void
task_ddivide_var(starpu_data_handle_t res,
		 starpu_data_handle_t a,
		 starpu_data_handle_t b,
		 struct codelet_options *options);

void
task_dcompute_alpha(starpu_data_handle_t dotpq,
		   starpu_data_handle_t delta_new,
		   starpu_data_handle_t alpha,
		   starpu_data_handle_t minus_alpha,
		    struct codelet_options *options);


void
task_dcompute_alpha_pipelinedCG(starpu_data_handle_t alpha,
				starpu_data_handle_t minus_alpha,
				starpu_data_handle_t new_gamma,
				starpu_data_handle_t delta,
				starpu_data_handle_t beta,
				struct codelet_options *options);

void
task_dcompute_alpha(starpu_data_handle_t dotpq,
		   starpu_data_handle_t delta_new,
		   starpu_data_handle_t alpha,
		   starpu_data_handle_t minus_alpha,
		    struct codelet_options *options);

void 
task_ddot(starpu_data_handle_t v1,
	  starpu_data_handle_t v2,
	  starpu_data_handle_t dot,
	  int use_pack,
	  int use_reduction, 
	  struct codelet_options *options); 

void 
task_dbzero(starpu_data_handle_t v1,
	    struct codelet_options *options);

void 
task_daxpy(starpu_data_handle_t v1,
	   starpu_data_handle_t v2,
	   starpu_data_handle_t alpha,
	   int use_pack,
	   struct codelet_options *options);

void
task_dsum_vect(starpu_data_handle_t res,
	       starpu_data_handle_t vect, 
	       unsigned nchunks,
	       struct codelet_options *options);

void
task_dcopyvect(starpu_data_handle_t v1,
	       starpu_data_handle_t v2,
	       int use_pack,
	       struct codelet_options *options);

void
task_dscalaxpy(starpu_data_handle_t v1,
	       starpu_data_handle_t v2,
	       starpu_data_handle_t alpha,
	       starpu_data_handle_t beta,
	       int use_pack,
	       struct codelet_options *options);
void
task_dspmv_1D(starpu_data_handle_t A, 
	      starpu_data_handle_t x,
	      starpu_data_handle_t y,
	      starpu_data_handle_t alpha, 
	      starpu_data_handle_t beta,
	      int no_alpha_beta,
	      unsigned nchunks,
	      int use_pack,
	      struct codelet_options *options);

void
task_dspmv_2D(starpu_data_handle_t A,
	      starpu_data_handle_t x,
	      starpu_data_handle_t y, 
	      starpu_data_handle_t alpha, 
	      starpu_data_handle_t beta,
	      int no_alpha_beta,
	      int use_reduction,
	      int use_pack,
	      int first,
	      struct codelet_options *options);


#endif // CODELETS_D_H
