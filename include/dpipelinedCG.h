#ifndef __dpipelinedCG_H__
#define __dpipelinedCG_H__

struct pipelinedCG_input {
  starpu_data_handle_t A;
  starpu_data_handle_t x;
  starpu_data_handle_t b;
  unsigned n;
  unsigned nnz;
  unsigned max_iter;
  double eps;
};

void dpipelinedCG(struct pipelinedCG_input *vars, 
		  int two_dim, 
		  int use_reduction, 
		  int explicit_prefetch,
		  int use_pack,
		  int permuting_instruction, 
		  int check, 
		  int debug_mode,
		  unsigned *blockRowPtr,
		  unsigned **distribution,
		  unsigned *workers_list);

#endif // __dpipelinedCG_H__
