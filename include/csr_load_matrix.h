#ifndef CSR_LOAD_MATRIX
#define CSR_LOAD_MATRIX

void csr_load_matrix(char *file,
		     double **val, unsigned **colInd, unsigned *nnz,
		     unsigned **rowPtr, unsigned *M, unsigned *N); 

void csr_load_sizes(char *file, unsigned  *m,
		    unsigned *n, unsigned *nnz);

#endif
