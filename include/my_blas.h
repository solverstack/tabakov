#ifndef __MY_BLAS_H__
#define __MY_BLAS_H__

#include "common.h"

void dpack_vect_cpu(double *dst_vect, double *src_vect, unsigned *ind, unsigned n);

void dunpack_vect_cpu(double *dst_vect, double *src_vect, unsigned *ind, unsigned n);

#endif //__MY_BLAS_H__
