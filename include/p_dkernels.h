#ifndef __P_KERNELS_H_
#define __P_KERNELS_H_

#include "dCG.h"
#include "dpipelinedCG.h"
typedef struct _CG_internal_data *CG_internal_data;

void 
pdCG_init_data(CG_internal_data *internal_data,
	       unsigned n,
	       int use_reduction, 
	       int use_pack);

void 
pdCG_init_algo(struct CG_input *input_data,
	       CG_internal_data internal_data,
	       int two_dim,
	       int use_reduction,
	       int explicit_prefetch,
	       int use_pack,
	       int permuting_instruction,
	       int debug_mode,
	       unsigned *blockRowPtr,
	       unsigned **distribution,
	       unsigned *workers_list);

void
pdCG_iter_asynch(struct CG_input *input_data,
		 CG_internal_data internal_data,
		 unsigned iter, 
		 int two_dim,
		 int use_reduction,
		 int explicit_prefetch,
		 int use_pack,
		 int permuting_instruction,
		 int debug_mode,
		 unsigned *blockRowPtr,
		 unsigned **distribution,
		 unsigned *workers_list);

void 
pdCG_cleanUp(CG_internal_data internal_data, 
	     int use_reduction, 
	     int use_pack, 
	     unsigned *worker_list);

typedef struct _pipelinedCG_internal_data *pipelinedCG_internal_data;

void 
pdpipelinedCG_init_data(pipelinedCG_internal_data *internal_data,
			unsigned n,
			int use_reduction, 
			int use_pack);

void 
pdpipelinedCG_init_algo(struct pipelinedCG_input *input_data,
			pipelinedCG_internal_data internal_data,
			int two_dim,
			int use_reduction,
			int explicit_prefetch,
			int use_pack,
			int permuting_instruction,
			int debug_mode,
			unsigned *blockRowPtr,
			unsigned **distribution,
			unsigned *workers_list);

void
pdpipelinedCG_iter_asynch(struct pipelinedCG_input *input_data,
			  pipelinedCG_internal_data internal_data,
			  unsigned iter, 
			  int two_dim,
			  int use_reduction,
			  int explicit_prefetch,
			  int use_pack,
			  int permuting_instruction,
			  int debug_mode,
			  unsigned *blockRowPtr,
			  unsigned **distribution,
			  unsigned *workers_list);

void 
pdpipelinedCG_cleanUp(pipelinedCG_internal_data internal_data, 
		      int use_reduction, 
		      int use_pack, 
		      unsigned *worker_list);



void pddot(starpu_data_handle_t v1,
	   starpu_data_handle_t v2,
	   starpu_data_handle_t dot,
	   int use_pack,
	   int use_reduction, 
	   unsigned *workers_list, 
	   starpu_tag_t starting_tag);

void pddot_manual_reduction(starpu_data_handle_t v1,
			    starpu_data_handle_t v2,
			    starpu_data_handle_t dot,
			    starpu_data_handle_t dots,
			    int use_pack,
			    int explicit_prefetch,
			    unsigned *workers_list, 
			    starpu_tag_t starting_tag);
void 
setup_cumulVect_manual_reduction(starpu_data_handle_t *handle,
				 double ***vects);

void 
cleanup_cumulVect_manual_reduction(starpu_data_handle_t handle,
				   double **vects);


void pdaxpy(starpu_data_handle_t v1,
	    starpu_data_handle_t v2,
	    starpu_data_handle_t alpha,
	    int use_pack,
	    int explicit_prefetch, 
	    unsigned *workers_list, 
	    starpu_tag_t starting_tag,
	    starpu_tag_t starting_depend_tag);

void pdcopyvect(starpu_data_handle_t v1,
		starpu_data_handle_t v2,
		int use_pack,
		unsigned *workers_list, 
		starpu_tag_t starting_tag);


void pdscalaxpy(starpu_data_handle_t v1,
		starpu_data_handle_t v2,
		starpu_data_handle_t alpha,
		starpu_data_handle_t beta,
		int use_pack,
		int explicit_prefetch, 
		unsigned *workers_list, 
		starpu_tag_t starting_tag);

void pdspmv(starpu_data_handle_t A,
	    starpu_data_handle_t x,
	    starpu_data_handle_t y, 
	    starpu_data_handle_t alpha,
	    starpu_data_handle_t beta,
	    int no_alpha_beta,
	    int two_dim,
	    int use_reduction, 
	    int use_pack,
	    unsigned *workers_list,
	    unsigned **distribution, 
	    starpu_tag_t starting_tag);

void pddivide_var(starpu_data_handle_t res,
		  starpu_data_handle_t a,
		  starpu_data_handle_t b,
		  unsigned nchunks,
		  int explicit_prefetch, 
		  unsigned *workers_list, 
		  starpu_tag_t starting_tag);

void pdcompute_alpha(starpu_data_handle_t dotpq,
		     starpu_data_handle_t delta_new,
		     starpu_data_handle_t alpha,
		     starpu_data_handle_t minus_alpha,
		     unsigned nchunks,
		     int explicit_prefetch,
		     unsigned *workers_list, 
		     starpu_tag_t starting_tag);

void pdcompute_alpha_pipelinedCG(starpu_data_handle_t alpha,
                                 starpu_data_handle_t minus_alpha,
                                 starpu_data_handle_t new_gamma,
                                 starpu_data_handle_t delta,
                                 starpu_data_handle_t beta,
				 unsigned nchunks,
                                 int explicit_prefetch,
                                 unsigned *workers_list,
                                 starpu_tag_t starting_tag);

#endif //__P_KERNELS_H_
