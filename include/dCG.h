#ifndef __dCG_H__
#define __dCG_H__

struct CG_input {
  starpu_data_handle_t A;
  starpu_data_handle_t x;
  starpu_data_handle_t b;
  unsigned n;
  unsigned nnz;
  unsigned max_iter;
  double eps;
};

void dCG(struct CG_input *vars, 
	 int two_dim, 
	 int use_reduction, 
	 int explicit_prefetch,
	 int use_pack,
	 int permuting_instruction, 
	 int check, 
	 int debug_mode,
	 unsigned *blockRowPtr,
	 unsigned **distribution,
	 unsigned *workers_list);

#endif // __dCG_H__
