#ifndef __SPARSEVECT_INTERFACE__
#define __SPARSEVECT_INTERFACE__ 

#include "common.h"
#include "data_helpers.h"
#include "enum.h"

#define SPARSEVECT_FULL         1
#define SPARSEVECT_PACK         2
#define SPARSEVECT_CONTINUOUS   3
#define SPARSEVECT_NONE         4

#define SPARSEVECT_MEMORY_OWE            0
#define SPARSEVECT_MEMORY_VALID          1
#define SPARSEVECT_MEMORY_INVALID        2

typedef struct _sparsevect_internal_data *sparsevect_internal_data;

struct sparsevect_interface
{
  /* basic vector interface */
  uintptr_t ptr;
  uintptr_t dev_handle;
  size_t    offset;
  uint32_t  nx;
  size_t    elemsize;

  /* this is allocated and used from the main memory */
  uint32_t  father;
  uint32_t  id;

  uint32_t        *memory_state;
  pthread_mutex_t *memory_mutex;
  uint32_t        **need_packing;
  uint32_t        **indices_sizes; 
  uint32_t        *first_sparse_indice;

  /* this is allocated and used  in the device  memory */
  uint32_t  ***send_indices;
  uint32_t  ***recive_indices;
  uintptr_t *send_buffers;
  uintptr_t *recive_buffers;
};




void sparsevect_data_register(starpu_data_handle_t *handle, unsigned home_node,
				     uintptr_t ptr, uint32_t nx, size_t elemsize);

void sparsevect_ptr_register(starpu_data_handle_t handle, unsigned node, uintptr_t ptr,
				    uintptr_t dev_handle, size_t offset);


uint32_t sparsevect_get_nx(starpu_data_handle_t handle);
size_t sparsevect_get_elemsize(starpu_data_handle_t handle);
uintptr_t sparsevect_get_local_ptr(starpu_data_handle_t handle);

sparsevect_internal_data
partition_sparsevect(starpu_data_handle_t handle,
		     uint32_t *csrColInd,
		     uint32_t *csrRowPtr,
		     double *csrVal, 
		     unsigned *blockRowPtr,
		     unsigned *worker_list,
		     unsigned nchunks, 
		     size_t elem_size);

#define SPARSEVECT_GET_DEV_HANDLE(interface) (((struct sparsevect_interface *)(interface))->dev_handle)
#define SPARSEVECT_GET_OFFSET(interface)     (((struct sparsevect_interface *)(interface))->offset)
#define SPARSEVECT_GET_NX(interface)         (((struct sparsevect_interface *)(interface))->nx)
#define SPARSEVECT_GET_ELEMSIZE(interface)   (((struct sparsevect_interface *)(interface))->elemsize)

uintptr_t SPARSEVECT_GET_PTR(void *interface);
void     update_memory_state(void *interface);

void 
sparsevect_free_internal_data(sparsevect_internal_data self, 
 			      unsigned *worker_list);

void
create_sparse_vectors(struct csr_2D_struct *args,
                      unsigned *blockRowPtr,
                      uint32_t ***need_pack,
                      uint32_t ***sparse_vectors_sizes,
                      uint32_t ****sparse_vectors_indices);

#endif //__SPARSEVECT_INTERFACE__
