#ifndef SPMV_CUDA_H
#define SPMV_CUDA_H

#include <cusparse.h>



void starpu_helper_cusparse_init(void);

void starpu_helper_cusparse_shutdown(void);

cusparseHandle_t cusparse_get_handle(void);

cusparseMatDescr_t cusparse_get_mat_descr(void);

#endif
